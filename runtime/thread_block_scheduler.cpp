#include <cassert>
#include "thread_block_scheduler.h"

unsigned HilbertScheduler::getSMID(unsigned x, unsigned y) {

    assert(x < config->block_x && "thread block position is out of range");
    assert(y < config->block_y && "thread block position is out of range");

    unsigned n = config->block_x;
    unsigned m = config->block_y;

    if ( m%2 == 1 ) {
      if ( y == m - 1 ) {
        if ( y/2 % 2 == 0 ) {
          return (y*n + x) / config->SM;
        }
        else {
          return (m*n - 1 - x) / config->SM;
        }
      }
    }
    
    unsigned row = y/2 * 2;
    if ( y/2 % 2 == 0 ) {
      // from left to right
      if ( x % 2 == 0 ) {
        // from top to bottom
        return (row*n + x*2 + y%2) / config->SM;
      }
      else {
        // from bottom to top
        return (row*n + x*2 + 1 - (y%2)) / config->SM;
      }
    }
    else {
      row += 2;
      // from right to left
      if ( x % 2 == 0 ) {
        // from bottom to top
        return (row*n - x*2 + y%2 - 2) / config->SM;
      }
      else {
        // from top to bottom
        return (row*n - x*2 - y%2 - 1) / config->SM;
      }
    }
}

void HilbertScheduler::getThreadBlockID(unsigned counter, 
                                unsigned& x, unsigned& y) {
    unsigned multirow = counter / (2 * config->block_x);
    unsigned column   = ( counter % (2 * config->block_x) ) / 2;
    unsigned index    = counter % 2;
    if ( multirow % 2 == 0) {
      // even multirow, from left to right
      x = column;
      if ( column  % 2 == 1 )
        // odd column, from bottom to top
        y = multirow * 2 + 1 - index;
      else
        // even column, from top to bottom
        y = multirow * 2 + index; // at bottom
    }
    else {
      // odd multirow, from right to left
      x = config->block_x - 1 - column;
      if ( x % 2 == 0 ) 
        // from top to bottom
        y = multirow * 2 + index;
      else 
        y = multirow * 2 + 1 - index;
    }
    return;
}
