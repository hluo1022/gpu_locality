
// the launch configuration of a kernel
struct KernelLaunchConfig {
  unsigned grid_x;     // grid x dimension
  unsigned grid_y;     // grid y dimension
  unsigned block_x;    // block x dimension
  unsigned block_y;    // block y dimension
  unsigned SM;         // number of SMs
  unsigned residency;  // the active blocks in one SM
};

class ThreadBlockScheduler {

protected:

  KernelLaunchConfig* config;

public:

  ThreadBlockScheduler(KernelLaunchConfig* c) : config(c) 
  {}

  virtual unsigned getSMID(unsigned x) = 0;
  virtual unsigned getSMID(unsigned x, unsigned y) = 0;
 
  virtual void getThreadBlockID(unsigned counter, unsigned& x) = 0;
  virtual void getThreadBlockID(unsigned counter, unsigned& x, unsigned& y) = 0;

  virtual ~ThreadBlockScheduler() {}

};

//================------------------===================
class HilbertScheduler : public ThreadBlockScheduler {

public:

  HilbertScheduler(KernelLaunchConfig* c) : ThreadBlockScheduler(c)
  {}

  unsigned getSMID(unsigned x) {}
  unsigned getSMID(unsigned x, unsigned y);
  void getThreadBlockID(unsigned counter, unsigned& x) {}
  void getThreadBlockID(unsigned counter, unsigned& x, unsigned& y);
};
