#include <iostream>
#include <stdlib.h>
using namespace std;
#include "thread_block_scheduler.h"


int main(int argc, char* argv[]) {
  KernelLaunchConfig klc;
  unsigned xx, yy;
  xx = atoi(argv[1]);
  yy = atoi(argv[2]);
  klc.block_x = xx;
  klc.block_y = yy;
  klc.SM = 1;
  
  HilbertScheduler hs(&klc);
  for(unsigned y = 0; y < yy; y++) {
    for(unsigned x = 0; x < xx; x++)
      cout << hs.getSMID(x, y) << " ";
    cout << endl;
  }
  for(unsigned i = 0; i < 16; i++) {
    unsigned x, y;
    hs.getThreadBlockID(i, x, y);
    cout << x << " " << y << endl;
  }
}
