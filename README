GPU Locality Research

This is a tool set analyzing the memory locality on GPU benchmarks.
The tools include:

1. sim:

[build command]:
  make sim

[run command]:
  ./sim

[description]:
  a simple memory trace-based cache simulator

  Note: the input trace is now hand coded in the source code
        
2. coalesce:

[build command]:
  make coalesce

[run command]:
  ./coalesce 

[description]:
  a tool that coalesces the coalescible memory references in the 
  raw input trace

  Note: the input trace is now hand coded in the source code,
        and the output trace is printed out

3. thread_locality:
4. warp_locality:

[build command]:
  make thread_locality
  make warp_locality

[run command]:
  ./thread_locality
  ./warp_locality

[description]:
  these two tools take a trace as input, and print out the locality
  metrics i.e. (shared) footprint and reuse distances. To switch 
  between footprint and reuse distances, please replace "XXX" in the 
  line "using namespace XXX" with corresponding metrics in the source
  code.
 
  Note: In addition to the output metric, these tools also dump the
        reuse time histogram in a text file whose name is specified
        in the source code (in default, XXX.in). This file is the 
        input of the tools array_compose and smith.

5. array_compose
6. thread_compose

[build command]
  make array_compose
  make thread_compose

[run command]:
  ./array_compose array_1.in array_2.in ... array_n.in
  ./thread_compose warp_count shard_footprint_profile

[description]:
  these two tools are used for composition. Array_compose compose
  for different arrays while thread_compose compose for different
  threads. 

  array_compose takes a list of files decribing each array's
  reuse time histogram as input, and outputs a file containing the
  composed reuse time histogram for the array group. The output file
  can then be fed into the tool smith to predict the cache miss ratio

  thread_compose composes the footprint for a set of warps. Since
  warps are symmetric, the footprint is assumed to be identical accross
  different warp sets with the same cardinality. This tool takes two 
  parameters. The first parameter is the targeted warp count. The second
  the shared_footprint_profile file, which is generated from running
  the tool thread_locality or warp_locality on shared_footprint namespace.

7. smith

[build command]
  make smith

[run command]:
  ./smith

[description]:
  this tool applies the Smith Equation (an equation used to approximate
  the miss ratio for set-associative cache) on the reuse time histogram.
  Its input is a reuse time histogram, generated from array_compose
  (if composition is needed) or warp/thread_locality (if no composition
  is not needed). The input file name is hand coded in the source file.


=================================
Example Run:

Case 1:

To simulate the cache for the trace md_large on all arrays and all warps, 
modify the source code of coalesce.cpp to specify input file name as 
"md_large.txt", then

  ./coalesce > coalesced_trace.txt

modify the source code of cache_sim_driver.cpp to specify input file name as 
"coalesced_trace.txt"

  ./sim

Case 2:  

To simulate the cache for the trace md_large only on Array 2, 
modify the Makefile, turn on "TARGET_ON_INDIVIDUAL_ARRAY" to 
"yes" and set "CXXFLAGS += -DTARGET_ARRAY=2"

The rest is identical to Case 1.

Case 3:

To compose the arrays 1 and 2 by using footprint at warp granularity, 

First coalesce the memory trace as in Case 1. Then modify Makefile, turn on 
"TARGET_ON_INDIVIDUAL_ARRAY" to "yes" and set "CXXFLAGS += -DTARGET_ARRAY=1". 

Then modify warp_locality.cpp to specify measuring footprint metric. 
(using namespace footprint;)

Then specify input and output file. Say the output file is "array_1"
(the footprint profile) and "array_1.in" (the reuse time histogram)

Repeat above steps for array 2. We get "array_2" and "array_2.in"
respectively.

To compose, run

./array_compose array_1.in array_2.in > smith.in

The composed reuse time histogram is stored in file smith.in

At last, modify smith.cpp to specify the input file as "smith.in",
then run

./smith

You will get the composed result.

Case 4:

To compose for warp 1 and 2 by using shared footprint at warp granularity,

First coalesce the memory trace.

Then modify warp_locality.cpp to specify measuring shared footprint metric. 
(using namespace shared_footprint;)

Then specify input and output file. Say the output file is "sfp"
(the shared footprint profile) and "sfp.in" (the reuse time histogram)

Then modify smith.cpp to specify the input file as "sfp.in"
then run

./smith

You will get the composed cache miss ratio prediction.

To obtain the footprint of the warp set ({1, 2}), run

./thread_compose 2 ./sfp
