#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <set>
#include <map>
#include <vector>
#include "common.h"
#include "types.h"
#include "gpu_def.h"

using namespace std;

//===----------------------------------------------------------------------===//
Record refs_inorder[32];
bool   refs_valid[32];

//===----------------------------------------------------------------------===//
//
// Count of total references
int refs = 0;

//===----------------------------------------------------------------------===//
//
static void output(ostream& out, const Record& rec) {
  out << dec 
      << rec.array      << " "  
      << rec.type       << " " 
      << rec.occurs     << " " 
      << rec.loop       << " " 
      << rec.thread     << " " 
      << hex << "0x" << rec.address << " "
      << dec << rec.block << endl;
}

int main(int argc, char* argv[]) {

  AddressInt access_Address;
  int Array_ID, type, occurs_ID, loop_ID, thread_ID, block_ID;
  int last_block;
  std::string size_string;
  ifstream inFile;
  int warp, last_warp = -1;

  if ( argc != 2 ) {
    std::cerr << "[Usage] : reorder [raw_trace_file]" << std::endl;
    return -1;
  }

  /* open data/trace file */
  inFile.open(argv[1], std::ios_base::in);
  if ( !inFile ) {
    std::cerr << "unable to open file " << argv[1] << " for reading" << std::endl;
    return 1;
  }

  for ( int i = 0; i < 32; ++i )
    refs_valid[i] = false;
//  std::getline(inFile, size_string);

  /* read the trace file line by line */
  while ( !inFile.eof() ) {

    inFile >> dec >> Array_ID 
                  >> type 
                  >> occurs_ID 
                  >> loop_ID 
                  >> thread_ID 
           >> hex >> access_Address
           >> dec >> block_ID;

    warp = thread_ID / 32;
    refs++;

    if ( refs != 1 && (warp != last_warp || block_ID != last_block) ) {
      for ( int i = 0; i < 32; ++i ) {
        if ( refs_valid[i] ) {
          output(cout, refs_inorder[i]);
        }
        refs_valid[i] = false;
      }
    }

    refs_inorder[thread_ID % 32] = Record(Array_ID, type, occurs_ID, loop_ID,
                                          thread_ID, access_Address, block_ID);
    refs_valid[thread_ID % 32] = true;
    last_warp = warp;
    last_block = block_ID;
  }
  
  // close the file
  inFile.close();

}
