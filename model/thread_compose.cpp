#include <iomanip>   // setprecision
#include <stdlib.h>  // atoi
#include <iostream>
#include <fstream>
#include <string>
#include "common.h"
#include "gpu_def.h"
#include "locality.h"

using namespace std;

static inline double n_choose_m( int n, int m) {
  if ( n < m ) return 0;
  double r = 1.0;
  for(int i = 1; i <= m; ++i) {
    r *= (n-i+1) * 1.0 / i;
  }
  return r;
}

int main(int argc, char* argv[]) {

  double fp = 0;
  int ws;
  ifstream inFile;
  double sfp[MAX_WARPS_PER_SM];

  /* check input arguments */
  if ( argc != 3 ) {
    cerr << "Usage : compose_footprint [warp_count] [input_file]" << endl;
    return 1;
  }

  int degree = atoi(argv[1]);
  string filename(argv[2]);
  int active_warps = 0;

  /* open data/trace file */
  inFile.open(filename.c_str(), std::ios_base::in);
  if ( !inFile ) {
    cerr << "unable to open file for reading" << endl;
    return 1;
  }
  
  // assume the first line is the count of the active warps 
  inFile >> active_warps;
  ofstream smith_in(GPU_LOCALITY_SMITHIN_FILE);

  /* read the trace file line by line */
  while ( !inFile.eof() ) {

    inFile >> ws;

    if ( ws == 0 ) break;

    for(int i = 0; i < MAX_WARPS_PER_SM; i++ ) {
      inFile >> sfp[i];
    }
    for(int i = 0; i < active_warps-1; i++ ) {
      fp += (sfp[i] - sfp[i+1]) * ( 1 - n_choose_m(active_warps-degree, i+1) / n_choose_m(active_warps, i+1) );
    }
    fp += sfp[active_warps-1];

    cout << ws << "\t" << setprecision(7) << fp << endl;
    //smith_in << ws << "\t" << fp << "\t" << delta << "\n";
    
    ws = fp = 0;
   
  }
  
  // close the file
  inFile.close();
  smith_in.close();

}
