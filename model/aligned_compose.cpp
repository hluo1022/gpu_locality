#include <iomanip>   // setprecision
#include <stdlib.h>  // atoi
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "gpu_def.h"
#include "locality.h"
#include "histo.H"

using namespace std;
using namespace histo;
using namespace footprint;

int main(int argc, char* argv[]) {

  vector<char*> inFiles;
  vector<int> fps;
  vector<double> rds;
  ifstream inFile;
  int i, e, ref, line = 0;
  uint64_t refs;

  // check input arguments
  if ( argc < 2 ) {
    cerr << "Usage : array_compose [array_footprint_list]" << endl;
    return 1;
  }

  for( i = 1; i < argc; ++i) {
    inFiles.push_back(argv[i]);
  }

  for ( i = 0, e = inFiles.size(); i < e; ++i ) {
 
    inFile.open(inFiles[i], std::ios_base::in);
    if ( !inFile ) {
      cerr << "unable to open file for reading" << endl;
      return 1;
    }
  
    inFile >> ref;
    refs += ref;
    inFile.close();
  }

  fps.resize(refs);
  rds.resize(refs);

  // open data/trace file
  for ( i = 0, e = inFiles.size(); i < e; ++i ) {
 
    inFile.open(inFiles[i], std::ios_base::in);
    if ( !inFile ) {
      cerr << "unable to open file for reading" << endl;
      return 1;
    }
  
    inFile >> ref;

    line = 0;
    // read the trace file line by line
    while ( !inFile.eof() ) {
      double fp;
      int    rd;
      inFile >> fp >> rd;
      if ( i == 0 ) {
        fps.push_back(0);
        rds.push_back(0);
      }
      for(int k = line * refs / ref; k < (line+1) * refs / ref; ++k) {
        fps[k] += (int)fp;
      }
      rds[line * refs / ref] += rd;
  
      line++;
    }

    // close the file
    inFile.close();
  }

  std::cout << refs << std::endl;
  for( i = 0; i < line; ++i ) {
    std::cout << fps[i] << "\t" << rds[i] << std::endl;
  }
  
}
