#ifndef _CACHE_SIM_H_
#define _CACHE_SIM_H_

#include <string.h>
#include "gpu_def.h"
#include "types.h"

extern unsigned line_addr_to_set(unsigned long line_addr,
                          unsigned long addr,
                          unsigned num_sets);

class cache_sim {

protected:
  unsigned misses;
  unsigned hits;
  unsigned total;

public:
  virtual void on_reference(AddressInt addr) = 0;
  virtual void print_stat() = 0;

  cache_sim() : misses(0), 
                hits(0),
                total(0)
  {}

  virtual ~cache_sim() {}
};

//================------------------===================
// instance of cache sim policies
//

// L1 cache management policy
class l1_cache_sim : public cache_sim {

private: 
  inline AddressInt Index1(const AddressInt& x) const {
    return (x/L1_CACHELINE) % L1_CACHE_SETS;
  }

  inline AddressInt Index2(const AddressInt& x) const {
//    return (x>>13) % L1_CACHE_SETS;
    return ((x >> 13) % 8) | (((x>>17 % 2)) << 4) | (((x>>19) % 2) << 6);
  }

  inline AddressInt SetIndex(const AddressInt& x) const {

    return line_addr_to_set(x/L1_CACHELINE, x, L1_CACHE_SETS);
  }

  inline AddressInt CachelineBase(const AddressInt& x) const {
    return L1CachelineBase(x);
  }

private:

  AddressInt cache[L1_CACHE_SETS][L1_CACHE_ASSOCIATIVITY];

public:

  l1_cache_sim()
  {
    memset(cache, 0, sizeof(AddressInt)*L1_CACHE_SETS*L1_CACHE_ASSOCIATIVITY);
  }

  void on_reference(AddressInt addr);
  void print_stat();
};

// texture cache management policy
class tex_cache_sim : public cache_sim {

private:

  inline AddressInt SetIndex(const AddressInt& x) const {
    return (x / TEX_CACHELINE) % TEX_CACHE_SETS;
  }

  inline AddressInt CachelineBase(const AddressInt& x) const {
    return TexCachelineBase(x);
  }

  inline AddressInt SectorBase(const AddressInt& x) const {
    return TexSectorBase(x);
  }

private:

  AddressInt cache[TEX_CACHE_SETS][TEX_CACHE_ASSOCIATIVITY];
  // a bitmap
  unsigned   valid[TEX_CACHE_SETS][TEX_CACHE_ASSOCIATIVITY];

public:

  tex_cache_sim() {
    for(int i = 0; i < TEX_CACHE_SETS; i++) {
      for(int j = 0; j < TEX_CACHE_ASSOCIATIVITY; j++) {
        cache[i][j] = 0;
        valid[i][j] = 0;
      }
    }
  }

  void on_reference(AddressInt addr);
  void print_stat();

};
#endif
