#ifndef _GPU_COMMON_DEFS_H_
#define _GPU_COMMON_DEFS_H_

#include <ctime>
#include <sys/time.h>

// (shared) footprint profile
#define GPU_LOCALITY_SFP_FILE      "sfp"

// input file for smith
#define GPU_LOCALITY_SMITHIN_FILE  "smith.in"

// method of testing whether the given is a target array
#define isTargetArray(array, target) ((1<<(array))&(target))

// the max volume of data referred
// it will be used in shared_footprint.cpp and locality.cpp
#define MAP_SIZE 0x1000000

// timing facilities
typedef unsigned long long timestamp_t;
inline static timestamp_t
get_timestamp () {
  struct timeval now;
  gettimeofday (&now, NULL);
  return  now.tv_usec + (timestamp_t)now.tv_sec * 1000000;
}

#endif
