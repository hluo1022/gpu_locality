#include "coalescer.h"

using namespace std;

bool L1CacheCoalescer::onCoalesce(AddressInt address, unsigned bytes, unsigned tid, int warp, int block, unsigned line_size) {

  // Compute the max schedule length (full-warps/half-warps/quarter-warps - see programming guide section "G.4.2. Global Memory")
  unsigned schedule_length;
  const unsigned warp_size = 32;

  if      (bytes == 8)  { schedule_length = warp_size/2; }
  else if (bytes == 16) { schedule_length = warp_size/4; }
  else                  { schedule_length = warp_size; }

  int chunk = (tid % warp_size) / schedule_length;
  AddressInt line = address / line_size;
					
  if ( warp != last_warp || block != last_block || tid <= last_tid ) { 
    last_warp = warp;
    last_block = block;
    last_tid = tid;
    for(int i = 0; i < 4; ++i) {
      request_counter[i] = 0;
    }
  }

  bool coalesced = false;  

  for(int k = 0; k < request_counter[chunk]; ++k) {
    if ( request_buffer[chunk][k] == line ) {
      // this address is already requested by prior 
      // references within the same warp
      return false;
    }
  }

  request_buffer[chunk][request_counter[chunk]] = line;
  request_counter[chunk]++;

  // this is an address that should form a new transaction
  return true;
}

bool ConstantCacheCoalescer::onCoalesce(AddressInt address, unsigned bytes, unsigned tid, int warp, int block, unsigned line_size) {

  const unsigned warp_size = 32;

  if ( warp != last_warp || block != last_block || tid <= last_tid) {
    last_warp = warp;
    last_block = block;
    last_tid = tid;
    request_counter = 0;
  }

  bool coalesced = false;  

  for(int k = 0; k <request_counter; ++k) {
    if ( request_buffer[k] == address ) {
      // this address is already requested by prior 
      // references within the same warp
      return false;
    }
  }

  request_buffer[request_counter] = address;
  request_counter++;

  // this is an address that should form a new transaction
  return true;
}

bool TextureCacheCoalescer::onCoalesce(AddressInt address, unsigned bytes, unsigned tid, int warp, int block, unsigned line_size) {

   int warp_size = 32;

  int chunk = (tid % warp_size) / 4;
  AddressInt line = address / line_size;

  if ( warp != last_warp || block != last_block || last_tid >= tid) {
    last_warp = warp;
    last_block = block;
    last_tid = tid;
    for(int i = 0; i < 8; ++i) {
      request_counter[i] = 0;
    }
  }

  bool coalesced = false;  

  for(int k = 0; k < request_counter[chunk]; ++k) {
    if ( request_buffer[chunk][k] == line ) {
      // this address is already requested by prior 
      // references within the same warp
      return false;
    }
  }
  
  request_buffer[chunk][request_counter[chunk]] = line;
  request_counter[chunk]++;

  // this is an address that should form a new transaction
  return true;
}
