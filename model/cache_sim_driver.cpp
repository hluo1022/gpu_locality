#include <iostream>
#include <fstream>
#include <string>
#include <set>
#include <sys/time.h>

#include "common.h"
#include "cache_sim.h"
#include "coalescer.h"


using namespace std;

set<int> arrays;
set<int> blocks;

int main(int argc, char* argv[]) {

  cache_sim *sim;
  
  unsigned long long access_Address;
  int Array_ID, type, occurs_ID, loop_ID, thread_ID, block_ID;
  int line = 0;
  ifstream inFile;

  L1CacheCoalescer l1d_mc;
  TextureCacheCoalescer tex_mc;

  if ( argc != 3 ) {
    std::cerr << "[Usage] : sim [tex|l1] [trace_file]" << std::endl;
    return -1;
  }

  // process cache mode (texture cache or l1 data cache)
  string mode(argv[1]);
  if ( mode == "l1" )
    sim = new l1_cache_sim;
  else if ( mode == "tex" )
    sim = new tex_cache_sim;
  else {
    std::cerr << "unknown cache simulation model" << std::endl;
    return -2;
  }

  /* open data/trace file */
  inFile.open(argv[2], std::ios_base::in);
  if ( !inFile ) {
    std::cerr << "unable to open file " << argv[1] << " for reading" << std::endl;
    return 1;
  }
  
  timestamp_t t0 = get_timestamp();
  /* read the trace file line by line */
  while ( !inFile.eof() ) {
    inFile >> dec >> Array_ID 
                  >> type 
                  >> occurs_ID 
                  >> loop_ID 
                  >> thread_ID 
           >> hex >> access_Address
           >> dec >> block_ID;

#if defined(ARRAY_FILTER)
    if ( isTargetArray(Array_ID, TARGET_ARRAY) ) {
#endif
      arrays.insert(Array_ID);
      blocks.insert(block_ID);
    if( l1d_mc.onCoalesce(access_Address, 4, thread_ID, thread_ID/32, block_ID, 128) )
      sim->on_reference(access_Address);
#if defined(ARRAY_FILTER)
    }
#endif

    line++;
  }

  timestamp_t t1 = get_timestamp();
  std::cout << "time : " << (t1 - t0) / 1000000.0L << " sec.\n"; 

  /* compute and print out footprint and miss ratio */
  sim->print_stat();

  std::cout << "Thread Blocks  : ";
  for(set<int>::iterator i = blocks.begin(); i != blocks.end() ; i++) {
    cout << *i << " ";
  }
  cout << endl;

  cout << "Arrays         : ";
  for(set<int>::iterator i = arrays.begin(); i != arrays.end() ; i++) {
    cout << *i << " ";
  }
  cout << endl;

  // close the file
  inFile.close();

}
