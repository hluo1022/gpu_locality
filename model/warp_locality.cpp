#include <iostream>
#include <fstream>
#include <string>
#include <set>
#include <map>
#include <vector>
#include <stdlib.h>
#include "common.h"
#include "types.h"
#include "locality.h"
#include "gpu_def.h"

//#define WARP_FILTER
//#define TARGET_WARP 48

using namespace std;
using namespace shared_footprint;

unsigned data_granularity;

int main(int argc, char* argv[]) {

  AddressInt access_Address;
  int Array_ID, type, occurs_ID, loop_ID, thread_ID, block_ID;
  int tick = 0;
  int ref = 0; // total references
  ifstream inFile;
  vector<Record> trace;
  set<int> active_warps;

  if ( argc != 3 ) {
    std::cerr << "[Usage] : warp_locality data_granularity trace_file" << std::endl;
    return -1;
  }

  // data granularity used in locality computation
  data_granularity = atoi(argv[1]);
  // sanity check 
  if (data_granularity != L1_CACHELINE &&
      data_granularity != TEX_CACHELINE &&
      data_granularity != TEX_SECTOR) {
    std::cerr << "[Error] : not a valid data granularity" << std::endl;
    return -2;
  } 

  /* open data/trace file */
  inFile.open(argv[2], std::ios_base::in);
  if ( !inFile ) {
    std::cerr << "unable to open file " << argv[1] << " for reading" << std::endl;
    return 1;
  }
  
  /* read the trace file line by line */
  while ( !inFile.eof() ) {
    inFile >> dec >> Array_ID 
                  >> type 
                  >> occurs_ID 
                  >> loop_ID 
                  >> thread_ID 
           >> hex >> access_Address
           >> dec >> block_ID;
/*
    trace.push_back(Record(Array_ID, type, occurs_ID, loop_ID, thread_ID, access_Address, block_ID));
  }

  timestamp_t start, end;

  start = get_timestamp();

  int i, e;
  for(i = 0, e = trace.size(); i < e; ++i) {

    Record rec = trace[i];
    Array_ID       = rec.array;
    type           = rec.type;
    occurs_ID      = rec.occurs;
    loop_ID        = rec.loop;
    thread_ID      = rec.thread;
    access_Address = rec.address;
    block_ID       = rec.block;
*/
    tick++;

    int warp_ID = thread_ID / 32;

    active_warps.insert(warp_ID);
#if defined(WARP_FILTER)
    if( warp_ID == TARGET_WARP ) { 
#endif
#if defined(ARRAY_FILTER)
      //if( i->array == TARGET_ARRAY ) {
    if( isTargetArray(Array_ID, TARGET_ARRAY) ) {
#endif

    ref++;
    //on_reference(access_Address, warp_ID, tick);
    on_reference(access_Address, warp_ID, tick);
#if defined(ARRAY_FILTER)
    }
#endif
#if defined(WARP_FILTER)
    }
#endif

  }

  /* post-processing the trace */
  on_exit(tick);

  /* compute and print out footprint and miss ratio */
  ofstream sfp_out(GPU_LOCALITY_SFP_FILE);
  ofstream smith_in(GPU_LOCALITY_SMITHIN_FILE);

  print_stat(sfp_out, smith_in);
//  end = get_timestamp();

//  cerr << "time : " << (end - start) / 1000000.0L << endl;

  sfp_out.close();
  smith_in.close();

  // close the file
  inFile.close();

}
