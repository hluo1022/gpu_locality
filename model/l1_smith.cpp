#include <algorithm>
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <string>
#include "common.h"
#include "gpu_def.h"
#include "probability.h"

using namespace std;

vector<double> rds;
vector<int>    fps;

unsigned fill_time;

double smith_conv() {

  double hit = 0;

  vector<int>::iterator capacity, f;
  vector<double>::iterator r;

  // sum up to CACHE_LINES
  capacity = upper_bound(fps.begin(), fps.end(), CACHE_LINES);

  for(f = fps.begin(), r = rds.begin(), fill_time = 0; 
      f != capacity ; f++, r++, fill_time++) {
    for(int w = 0; w < CACHE_ASSOCIATIVITY; w++) {
      if ( *r != 0 && *f >= w)
        hit += *r * binomial(1.0/CACHE_SETS, *f, w);
    }
  }

  return hit;

}

int main(int argc, char* argv[]) {

  int capacity;
  int miss;
  double rd, mr, hits;
  int refs; // the total references
  ifstream inFile;

  if ( argc != 2 ) {
    std::cerr << "[Usage] : l1_smith [reuse_histogram]" << std::endl;
    return -1;
  }

  /* open data file */
  inFile.open(argv[1], std::ios_base::in);
  if ( !inFile ) {
    std::cerr << "unable to open file " << argv[1] << " for reading" << std::endl;
    return 1;
  }
  
  inFile >> refs; 
  /* read the trace file line by line */
  while ( !inFile.eof() ) {
    inFile >> capacity >> rd;
    fps.push_back(capacity);
    rds.push_back(rd);
  }

  hits = smith_conv();
  miss = refs - (int)hits;
  mr   = miss * 1.0 / refs;

  // close the file
  inFile.close();

  std::cout << "Cache capacity : " << CACHE_CAPACITY << " KB" << std::endl;
  std::cout << "Cacheline size : " << CACHELINE      << " B"  << std::endl;
  std::cout << "Cache assoc.   : " << CACHE_ASSOCIATIVITY << "-way" << std::endl;
  std::cout << "Cache sets     : " << CACHE_SETS << std::endl;
  std::cout << "Total accesses : " << refs << std::endl;
  std::cout << "Hit counts     : " << (int)hits << std::endl;
  std::cout << "Miss counts    : " << miss << std::endl;
  std::cout << "Miss rate      : " << mr   << std::endl;
  std::cout << "Fill time      : " << fill_time << std::endl;

}
