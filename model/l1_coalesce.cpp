#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <set>
#include <map>
#include <vector>
#include "common.h"
#include "types.h"
#include "gpu_def.h"

using namespace std;

//===----------------------------------------------------------------------===//
typedef set<AddressInt> AddressSet;

//===----------------------------------------------------------------------===//
//
// Buffer to hold all the reference info of one warp reference
Record address_buffer[32];

// Set to hold all the address made by one warp reference
AddressSet schedule_addresses;

int last_warp   = 0;
int last_thread = 0;
int last_array  = 0;

// Count of total references
int ref = 0;

// The set of active warps in the trace
set<int> active_warps;

//unsigned cand_blocks[] = {12, 25, 38, 51};

//===----------------------------------------------------------------------===//
//
//
static void output(ostream& out, const Record& rec) {
  out << dec 
      << rec.array      << " "  
      << rec.type       << " " 
      << rec.occurs     << " " 
      << rec.loop       << " " 
      << rec.thread     << " " 
      << hex << "0x" << rec.address << " "
      << dec << rec.block << endl;
}

int main(int argc, char* argv[]) {

  int line = 0;
  AddressInt access_Address;
  int Array_ID, type, occurs_ID, loop_ID, thread_ID, block_ID;
  int last_block = 0;
  ifstream inFile;
  vector<Record> trace;

  memset(address_buffer, 0, 32 * sizeof(Record));

  if ( argc != 2 ) {
    cerr << "[Usage] : l1_coalesce [trace_file]" << endl;
    return -1;
  }

  /* open data/trace file */
  inFile.open(argv[1], std::ios_base::in);
  if ( !inFile ) {
    std::cerr << "unable to open file " << argv[1] << " for reading" << std::endl;
    return 1;
  }

  /* read the trace file line by line */
  while ( !inFile.eof() ) {

    inFile >> dec >> Array_ID 
                  >> type 
                  >> occurs_ID 
                  >> loop_ID 
                  >> thread_ID 
           >> hex >> access_Address
           >> dec >> block_ID;

    int warp_ID = thread_ID / 32;

    if ( warp_ID != last_warp || block_ID != last_block ) {

      // a new warp-scheduling moment   
     
      int schedule_length = 1; // TODO
      int chunk = 32 / schedule_length;
      for(int j = 0; j < schedule_length; j++) {
        for(int i = j * chunk; i < (j+1)*chunk; i++) {
          AddressInt addr = address_buffer[i].address;
          if ( addr != 0 ) {
            if ( schedule_addresses.find(L1CachelineBase(addr)) == schedule_addresses.end() ) {
              address_buffer[i].address = L1CachelineBase(addr);
              output(cout, address_buffer[i]);
              schedule_addresses.insert(L1CachelineBase(addr));
            }
          }
        }
        schedule_addresses.clear();
      }

      // reset the buffers
      memset(address_buffer, 0, 32 * sizeof(Record)); 
    }

    int tid_in_warp = thread_ID % 32;
    address_buffer[tid_in_warp] = Record(Array_ID, type, occurs_ID, loop_ID, 
                                         thread_ID, access_Address, block_ID);

    last_warp = warp_ID;
    last_block = block_ID;
    last_thread = thread_ID;
    line ++;
    //if (line > 64*1024) break;
  }
  
  int schedule_length = 1; // TODO
  int chunk = 32 / schedule_length;
  for(int j = 0; j < schedule_length; j++) {
    for(int i = j * chunk; i < (j+1)*chunk; i++) {
      AddressInt addr = address_buffer[i].address;
      if ( addr != 0 ) {
        if ( schedule_addresses.find(L1CachelineBase(addr)) == schedule_addresses.end() ) {
          address_buffer[i].address = L1CachelineBase(addr);
          output(cout, address_buffer[i]);
          schedule_addresses.insert(L1CachelineBase(addr));
        }
      }
    }
    schedule_addresses.clear();
  }

  // close the file
  inFile.close();
}
