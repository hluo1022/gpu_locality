#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <set>
#include <map>
#include <vector>
#include "common.h"
#include "types.h"
#include "gpu_def.h"

using namespace std;

//===----------------------------------------------------------------------===//
set<AddressInt> requested_addresses;

Record refs_inorder[32];
bool   refs_valid[32];
Record requests[COALESCE_QUAD];

//===----------------------------------------------------------------------===//
//

//===----------------------------------------------------------------------===//
//
static void output(ostream& out, const Record& rec) {
  out << dec 
      << rec.array      << " "  
      << rec.type       << " " 
      << rec.occurs     << " " 
      << rec.loop       << " " 
      << rec.thread     << " " 
      << hex << "0x" << rec.address << " "
      << dec << rec.block << endl;
}

int main(int argc, char* argv[]) {

  AddressInt access_Address;
  int Array_ID, type, occurs_ID, loop_ID, thread_ID, block_ID;
  int last_block = 0;
  int warp, last_warp = 0;
  ifstream inFile;
  unsigned refs = 0;

  if ( argc != 2 ) {
    std::cerr << "[Usage] : tex_coalesce [trace_file]" << std::endl;
    return -1;
  }

  /* open data/trace file */
  inFile.open(argv[1], std::ios_base::in);
  if ( !inFile ) {
    std::cerr << "unable to open file " << argv[1] << " for reading" << std::endl;
    return 1;
  }

  /* read the trace file line by line */
  while ( !inFile.eof() ) {

    inFile >> dec >> Array_ID 
                  >> type 
                  >> occurs_ID 
                  >> loop_ID 
                  >> thread_ID 
           >> hex >> access_Address
           >> dec >> block_ID;

    warp = thread_ID / 32;
    refs++;

    if ( refs != 1 && (warp != last_warp || block_ID != last_block) ) {
      int form_quad = 0;
      for ( int i = 0; i < 32; ++i ) {

        if ( refs_valid[i] ) {
          form_quad++;
          
          if ( form_quad > COALESCE_QUAD ) {
            for ( int j = 0; j < COALESCE_QUAD; ++j) {
              if ( requested_addresses.count(TexSectorBase(requests[j].address)) ) {
                output(cout, requests[j]);
                requested_addresses.erase(TexSectorBase(requests[j].address));
              }
            }
            form_quad = 1; 
          }
          requests[form_quad-1] = refs_inorder[i];
          requested_addresses.insert(TexSectorBase(refs_inorder[i].address));
        }
        
        refs_valid[i] = false;
      }

      // output the requests left over in the warp's reference
      for ( int j = 0; j < COALESCE_QUAD; ++j) {
        if ( requested_addresses.count(TexSectorBase(requests[j].address)) ) {
          output(cout, requests[j]);
          requested_addresses.erase(TexSectorBase(requests[j].address));
        }
      }
    }

    refs_inorder[thread_ID % 32] = Record(Array_ID, type, occurs_ID, loop_ID,
                                          thread_ID, access_Address, block_ID);
    refs_valid[thread_ID % 32] = true;
    last_warp = warp;
    last_block = block_ID;
  }
  
  int form_quad = 0;
  for ( int i = 0; i < 32; ++i ) {

    if ( refs_valid[i] ) {
      form_quad++;
          
      if ( form_quad > COALESCE_QUAD ) {
        for ( int j = 0; j < COALESCE_QUAD; ++j) {
          if ( requested_addresses.count(TexSectorBase(requests[j].address)) ) {
            output(cout, requests[j]);
            requested_addresses.erase(TexSectorBase(requests[j].address));
          }
        }
        form_quad = 1; 
      }
      requests[form_quad-1] = refs_inorder[i];
      requested_addresses.insert(TexSectorBase(refs_inorder[i].address));
    }
        
    refs_valid[i] = false;
  }

  // output the requests left over in the warp's reference
  for ( int j = 0; j < COALESCE_QUAD; ++j) {
    if ( requested_addresses.count(TexSectorBase(requests[j].address)) ) {
      output(cout, requests[j]);
      requested_addresses.erase(TexSectorBase(requests[j].address));
    }
  }

  // close the file
  inFile.close();

}
