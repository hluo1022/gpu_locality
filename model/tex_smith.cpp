#include <algorithm>
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <string>
#include "common.h"
#include "gpu_def.h"
#include "probability.h"

using namespace std;

// coarse 
vector<double> rds1;
vector<int>    fps1;

// fine
vector<double> rds2;
vector<int>    fps2;

double smith_conv() {

  double hit = 0;

  vector<int>::iterator capacity, f1, f2;
  vector<double>::iterator r;

  // sum up to CACHE_LINES
  capacity = upper_bound(fps2.begin(), fps2.end(), TEX_CACHE_SECTORS);

  for(f1 = fps1.begin(), f2 = fps2.begin(), r = rds2.begin(); 
      f2 != capacity ; f1++, f2++, r++) {
    for(int w = 0; w < TEX_CACHE_ASSOCIATIVITY; w++) {
      if ( *r != 0 && *f1 >= w)
        hit += *r * binomial(1.0/TEX_CACHE_SETS, *f1, w);
    }
  }

  return hit;

}

int main(int argc, const char* argv[]) {

  int capacity;
  int miss;
  double rd, mr, hits;
  int refs; // the total references
  ifstream inFile1;
  ifstream inFile2;

  if ( argc != 3 ) {
    cerr << "[Usage] : tex_smith [cacheline_reuse_histo] [sector_reuse_histo]" << endl;
    return -1;
  }

  const char* cg_infile = argv[1];
  const char* fg_infile = argv[2];

  /* open data file */
  inFile1.open(cg_infile, std::ios_base::in);
  inFile2.open(fg_infile, std::ios_base::in);
  if ( !inFile1 || !inFile2 ) {
    std::cerr << "unable to open files for reading" << std::endl;
    return 1;
  }
  
  inFile1 >> refs;
  /* read the histogram file line by line */
  while ( !inFile1.eof() ) {
    inFile1 >> capacity >> rd;
    fps1.push_back(capacity);
    rds1.push_back(rd);
  }

  inFile2 >> refs;
  /* read the histogram file line by line */
  while ( !inFile2.eof() ) {
    inFile2 >> capacity >> rd;
    fps2.push_back(capacity);
    rds2.push_back(rd);
  }

  hits = smith_conv();
  miss = refs - (int)hits;
  mr   = miss * 1.0 / refs;

  // close the file
  inFile1.close();
  inFile2.close();

  std::cout << "Cache capacity : " << TEX_CACHE_CAPACITY << " KB" << std::endl;
  std::cout << "Cacheline size : " << TEX_CACHELINE      << " B"  << std::endl;
  std::cout << "Sector size    : " << TEX_SECTOR         << " B"  << std::endl;
  std::cout << "Cache assoc.   : " << TEX_CACHE_ASSOCIATIVITY << "-way" << std::endl;
  std::cout << "Cache sets     : " << TEX_CACHE_SETS << std::endl;
  std::cout << "Total accesses : " << refs << std::endl;
  std::cout << "Hit counts     : " << (int)hits << std::endl;
  std::cout << "Miss counts    : " << miss << std::endl;
  std::cout << "Miss rate      : " << mr   << std::endl;

}
