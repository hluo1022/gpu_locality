/*
 * locality.cpp
 *
 * This file contains two basic algorithms to
 * measure locality.
 *   1. use footprint to predict miss ratio
 *   2. use reuse distance to predict miss ratio
 *
 * These two algorithms are placed in two different
 * namespaces. A usage example is given in test.cpp
 *
 * More detail about the footprint method can be 
 * found in LOCA tool
 *
 *   https://github.com/dcompiler/loca
 *
 * The reuse distance is implemented using unordered_map
 * which requires a compilation flag '-std=c++11'
 * Using map is also ok. The implementation maintains
 * a combination of stack and hashmap. It also has a 
 * static marker array, whose entry points to an entry
 * in the stack of a particular depth.
 *
 * Original code was written by URCS compiler group
 * Rewritten by Hao Luo
 *
 */

#include <iostream>
#include <fstream>
#include <map>
#include <list>
#include "types.h"
#include "gpu_def.h"
#include "histo.H"
#include "locality.h"

using namespace std;
using namespace histo;

namespace footprint {

const unsigned int SUBLOG_BITS  = 8;
const unsigned int MAX_WINDOW   = (65-SUBLOG_BITS)*(1<<SUBLOG_BITS);

// N: the trace length
AddressInt N;
// R: the total reference cout
AddressInt R;
// M: the footprint volume
AddressInt M;

//  hash table, used to store the last reference time
std::map<addr_type, AddressInt> *stamp_table;

static struct stamp_table_allocator {
  stamp_table_allocator() {
    stamp_table = new std::map<addr_type, AddressInt>[MAP_SIZE];
  }
  ~stamp_table_allocator() {
    delete[] stamp_table;
  }
} __static_stamp_table_allocator;

// reuse length histograms
AddressInt reuse_histo[MAX_WINDOW];
AddressInt reuse_histo2[MAX_WINDOW];
AddressInt reuse_histo_i[MAX_WINDOW];

void on_reference(addr_type raw_addr, int tick) {

  // get the base address of addr's cacheline
  addr_type  addr = Base(raw_addr);
  AddressInt idx  = AddressMapIndex(addr);

  N = tick;

  // get last access time
  AddressInt prev_access = stamp_table[idx][addr];
  
  // store current access time
  stamp_table[idx][addr] = N;

  idx = sublog_value_to_index<MAX_WINDOW, SUBLOG_BITS>(N-prev_access-1);

  if(prev_access == 0) {
    // this is the first time accessing data
    M++;
  }
  else {
    reuse_histo2[(idx+1)%MAX_WINDOW] ++;
  }

  R++;
  reuse_histo[idx] ++;
  reuse_histo_i[idx] += N - prev_access-1;

}

void on_exit(int tick) {

  AddressInt i;
  AddressInt tmp;
  AddressInt idx;

  // collect the last reuse intervals
  
  N = tick + 1;

  // traverse the entries in stamp_table
  for(i=0; i<MAP_SIZE; i++) {

    // traverse the entry in each entry of stamp_table
    for(map<addr_type, AddressInt>::iterator iter = stamp_table[i].begin(); 
        iter != stamp_table[i].end(); iter++) {

      AddressInt s = iter->second;
      if(0 == s) {
        continue;
      }

      tmp = N - s;
      idx = sublog_value_to_index<MAX_WINDOW, SUBLOG_BITS>(tmp);
      reuse_histo[idx] ++;
      reuse_histo_i[idx] += tmp;

    }
  }

}

void print_stat(ostream& fp_out, ostream& smith_in) {

  AddressInt ws;
  AddressInt i;
  double sum = 0, sum_i = 0, fp, mr = 1;

  for(AddressInt j=1;j<=sublog_value_to_index<MAX_WINDOW, SUBLOG_BITS>(N);j++){
    sum += reuse_histo[j];
    sum_i += reuse_histo_i[j];
  }

  fp_out << "\n============== Footprint ================\n" << std::endl;
  fp_out << "idx\tws\tfp\tmr" << std::endl;
  smith_in << R << std::endl;

  AddressInt N_index = sublog_value_to_index<MAX_WINDOW, SUBLOG_BITS>(N);
  for(i=1; i<=N_index; i++) {

    ws = sublog_index_to_value<MAX_WINDOW, SUBLOG_BITS>(i);
    //fp = 1.0 * ((N-sum)*ws + sum_i) / (N-ws+1);
    //fp = 1.0 * ((N-sum)*ws + sum_i) / (N-ws+1);
    fp = (double)sum_i / (N-ws+1) - sum * (((double)(ws-1))/(N-ws+1));
    fp = M - fp;

    sum_i -= reuse_histo_i[i];
    sum   -= reuse_histo[i];
    //sum_i += reuse_histo_i[i];
    //sum   += reuse_histo[i];
    
    mr    -= reuse_histo2[i-1] * 1.0 / R;

    // output format:
    // [index]	[window_size]	[footprint]	[mr]
    //std::cout << i << "\t" << ws << "\t" << fp << "\t" << mr << std::endl;
    fp_out << i << "\t" << ws << "\t" << fp << "\t" << mr << std::endl;

    smith_in << (int)fp << "\t" << reuse_histo2[i-1] << "\n";
      
  }
}

}; // namespace footprint

namespace reuse_distance {

// the total volume of data accessed
unsigned int M;

// number of markers
#define NUM_MARKERS 2000

// Hao hack begin
// max distance
#define MAX_DIS 20000
// Hao hack end

struct stack_cell_type {
  addr_type addr;
  unsigned int dis;
};

class stack_type {

  typedef map<addr_type, list<stack_cell_type>::iterator> map_type;
  typedef list<stack_cell_type> list_type;
  map_type cache_map;
  list_type cache_list;

public:

  typedef list<stack_cell_type>::iterator iterator;

  bool find(addr_type addr, iterator* cell) {
    map_type::iterator iter = cache_map.find(addr);
    if ( iter == cache_map.end() ) {
      return false;
    }
    *cell = iter->second;
    return true;
  }

  iterator insert(stack_cell_type cell) {
    cache_list.push_front(cell);
    cache_map[cell.addr] = cache_list.begin();
    return cache_map[cell.addr];
  }

  void remove(iterator iter) {
    stack_cell_type cell = *iter;
    cache_map.erase(cell.addr);
    cache_list.erase(iter);
  }

  inline iterator back() { 
    iterator iter = cache_list.end(); 
    return --iter;
  }

// Hao hack begin
  inline void pop_back() {
    cache_map.erase(cache_list.rbegin()->addr);
    cache_list.pop_back();
  }

  inline size_t size() {
    return cache_list.size();
  }
// Hao hack end
};

struct marker_type {
  unsigned count;
  unsigned maxDis; /* maxDis is the tight upper bound of the range */
  stack_type::iterator mkrCell;
};

class markers_type {

  marker_type impl[NUM_MARKERS];

public:

  marker_type& operator[](size_t i) { return impl[i]; }

  int lastMarker;

  markers_type() {
   
    int base = 2, logSize = 11, cnt = 1, log = 2;
    int i;

    impl[0].maxDis = 0;
    while( cnt <= logSize ) {
      impl[cnt].maxDis = log;
      log *= base;
      cnt ++;
    }

    for(i=logSize+1; i<NUM_MARKERS; i++)
      impl[i].maxDis = impl[i-1].maxDis + impl[logSize].maxDis;

    for(i=0; i<NUM_MARKERS; i++) {
      impl[i].count = 0;
    }
  }

};

static unsigned total = 0;

// an array of markers, each marker points to a cell on the stack
markers_type markers;

// a stack holding the addresses
stack_type   stack;

void on_reference(addr_type raw_addr, int tick) {

  unsigned dis;
  unsigned i;
  addr_type addr = Base(raw_addr);

// Hao hack begin 
  total++;
// Hao hack end 

  // prepare cell for inserting
  stack_cell_type cell;
  cell.addr = addr;
  cell.dis  = 0;

  stack_type::iterator iter;
  if (  !stack.find(addr, &iter) ) {
    // not find addr in stack
    // first time accessing addr

    M++;
    if ( M == 1 ) {
      // very first access of the entire trace
      iter = stack.insert(cell);
      markers[0].mkrCell  = iter;
      markers.lastMarker  = 1;
      return;
    }
 
    // this is not the first access of the trace,
    // but the first access to this data
    dis = markers.lastMarker - 1;
    // Hao hack begin
    if ( M <= MAX_DIS ) {
    // Hao hack end
      if ( M == markers[markers.lastMarker].maxDis+1 ) {
        markers[markers.lastMarker].mkrCell = stack.back();
        markers.lastMarker++;
      }
    // Hao hack begin
    }
    // Hao hack end
  }
  else {
  
    // found addr in the stack
    if ( iter == markers[0].mkrCell) {
      // access the top element
      markers[0].count++;
      return;
    }

    // access element in the middle of the stack
    //
    dis = iter->dis - 1;
    
    // update markers
    markers[dis+1].count++;

    // a tricky case: the cell is a marker
    if ( markers[dis+1].mkrCell == iter ) {
      markers[dis+1].mkrCell = --iter;
      ++iter;
    }

    // delete the element
    stack.remove(iter);
  }

  /* update the markers */
  markers[0].mkrCell->dis = 1;
  for(i=1; i<=dis; i++) {
    markers[i].mkrCell->dis = i+1;
    --markers[i].mkrCell;
  }

  /* insert the cell at the head of stack */
  iter = stack.insert(cell);
  // Hao hack begin
  if ( stack.size() > MAX_DIS ) {
    stack.pop_back();
  }
  // Hao hack end
  markers[0].mkrCell = iter;
}

void on_exit(int tick) {}

void print_stat(ostream& rd_out, ostream& smith_in) {

  int i;

  rd_out << "Total data " << M << "\n";
  rd_out << "Total access " << total << "\n";
  smith_in << total << "\n";

  rd_out << "Distance 0\t" << markers[0].count << "\n";
  smith_in << "1 \t" << markers[0].count  << "\n";
  for(i=1; i<=markers.lastMarker; i++) {
    rd_out << "Distance " << markers[i-1].maxDis+1 << " to "
              << markers[i].maxDis << "\t" << markers[i].count << "\n";
    smith_in << markers[i].maxDis << "\t" << markers[i].count << "\n";
  }

}

}; // namespace reuse_distance

