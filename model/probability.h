#ifndef _PROBABILTY_H_
#define _PROBABILTY_H_

inline double n_choose_m(int n, int m) {
  double r = 1.0;
  for(int i = 1; i <= m; ++i) {
    r *= (n-i+1) * 1.0 / i;
  }
  return r;
}

inline double binomial(double p, unsigned t, unsigned h) {
  return   pow(p, (double)h) \
         * pow(1 - p, (double)(t-h)) \
         * n_choose_m(t, h);
}

#endif
