#ifndef _LOCALITY_H_
#define _LOCALITY_H_

#include <ostream>
#include "types.h"
#include "common.h"

typedef unsigned long long addr_type;

// NOTE: the "tick" variable must be positive

namespace footprint {

extern void on_reference(addr_type addr, int tick);
extern void on_exit(int tick);
extern void print_stat(std::ostream& fp_out, std::ostream& smith_in);

}

namespace reuse_distance {

extern void on_reference(addr_type addr, int tick);
extern void on_exit(int tick);
extern void print_stat(std::ostream& rd_out, std::ostream& smith_in);

}

namespace shared_footprint {

extern void on_reference(addr_type addr, int thread, int tick);
extern void on_exit(int tick);
extern void print_stat(std::ostream& sfp_out, std::ostream& smith_in);

}

extern unsigned data_granularity;

inline AddressInt AddressMapIndex(AddressInt x) {
  return (x / data_granularity) % MAP_SIZE;
}

inline AddressInt Base(AddressInt x) {
  return (x / data_granularity) * data_granularity;
}

#endif
