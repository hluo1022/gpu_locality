#include <iostream>
#include <set>
#include "gpu_def.h"
#include "cache_sim.h"

using namespace std;

std::set<unsigned> covered_cache_sets;

void l1_cache_sim::on_reference(AddressInt raw_addr) {

  total++;
  AddressInt addr = CachelineBase(raw_addr);
  int set_index = SetIndex(addr);
  covered_cache_sets.insert(set_index);
  //cout << dec << addr << endl;
  
  for(int i = 0; i < L1_CACHE_ASSOCIATIVITY; i++) {

    // hit
    if ( cache[set_index][i] == addr ) {
//      cout << hex << raw_addr << endl;
      hits++;
      for(int j = i; j >= 1; j-- ) {
        cache[set_index][j] = cache[set_index][j-1];
      }
      cache[set_index][0] = addr;
      return;
    }
  }

  // miss
  misses++;
  for(int j = L1_CACHE_ASSOCIATIVITY-1; j >= 1; j-- ) {
    cache[set_index][j] = cache[set_index][j-1];
  }
  cache[set_index][0] = addr;
  return;

}

void l1_cache_sim::print_stat() {

  std::cout << "\n============== SIM ================\n" << std::endl;
  std::cout << "Cache capacity : " << L1_CACHE_CAPACITY << " KB" << std::endl;
  std::cout << "Cacheline size : " << L1_CACHELINE      << " B"  << std::endl;
  std::cout << "Cache assoc.   : " << L1_CACHE_ASSOCIATIVITY << "-way" << std::endl;
  std::cout << "Total accesses : " << total << std::endl;
  std::cout << "Hits           : " << hits << std::endl;
  std::cout << "Misses         : " << misses << std::endl;
  std::cout << "Miss ratio     : " << misses * 1.0 / total << std::endl;
  std::cout << "Cache sets     : " << covered_cache_sets.size() << "/" << L1_CACHE_SETS << std::endl;
}
