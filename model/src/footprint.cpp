#include "histo.H"
#include "footprint.h"
#include <iostream>

using namespace std;
using namespace histo;

namespace footprint {

FootprintState* FootprintState::getFootprintState() {
  return new FootprintState;
}

void FootprintState::setupReuseTimeHisto(unsigned array_num) {
  reuses      = new ReuseTimeHisto[array_num];
  true_reuses = new ReuseTimeHisto[array_num];
  reuses_i    = new ReuseTimeHisto[array_num];
}

void FootprintState::setupFootprint(unsigned array_num) {

  footprints = new FootprintCurve[array_num];

  M = new unsigned[array_num];
  R = new unsigned[array_num];
  for(int i = 0; i < array_num; ++i)
    M[i] = R[i] = 0;
}

void FootprintState::cleanup() {
  delete[] reuses;
  delete[] reuses_i;
  delete[] true_reuses; 
  delete[] footprints;
  delete[] M;
  delete[] R;
}

bool FootprintState::initialize(unsigned array_num) {

  // if already initialized, don't do anything
  if (initialized) return false;

  setupReuseTimeHisto(array_num);
  setupFootprint(array_num);

  NArray = array_num;

  initialized = true;
  return true;
}

void FootprintState::print(std::ostream& out) {
  
  for(int a = 0; a < NArray; a++) {
    out << "array : " << a << "\n";
    out << "reuse footprint\n";
    for(int i=1; i<=footprints->length; ++i) {
      out << true_reuses[a].value[i] << " " << footprints[a].value[i] << "\n";
    }
    out << "\n";
  }

}

//============================================================//
//
void FootprintBuilder::calculateFootprint(int array) {
  
  AddressInt ws;
  double sum = 0, sum_i = 0, fp, mr = 1;

  AddressInt N_index = sublog_value_to_index<MAX_WINDOW, SUBLOG_BITS>(FS->N);

  for(AddressInt j=1; j<=N_index; ++j){
    sum += FS->reuses[array].value[j];
    sum_i += FS->reuses_i[array].value[j];
  }

  for(AddressInt i=1; i<=N_index; i++) {
    ws = sublog_index_to_value<MAX_WINDOW, SUBLOG_BITS>(i);
    fp = (double)sum_i / (FS->N-ws+1) - sum * (((double)(ws-1))/(FS->N-ws+1));
    FS->footprints[array].value[i] = FS->M[array] - fp;

    sum_i -= FS->reuses_i[array].value[i];
    sum   -= FS->reuses[array].value[i];
  }
}

void FootprintBuilder::onReference(int array, AddressInt address, int tick, unsigned granularity) {

  // get the base address of addr's cacheline
  AddressInt addr = Base(address, granularity);

  FS->R[array]++;

  // get last access time
  AddressInt prev_access = stamp_table[array][addr];
  
  // store current access time
  stamp_table[array][addr] = tick;

  unsigned idx = sublog_value_to_index<MAX_WINDOW, SUBLOG_BITS>(tick-prev_access-1);

  if(prev_access == 0) {
    // this is the first time accessing data
    FS->M[array]++;
  }
  else {
    FS->true_reuses[array].value[(idx+1)%MAX_WINDOW] ++;
  }

  FS->reuses[array].value[idx] ++;
  FS->reuses_i[array].value[idx] += tick - prev_access - 1;
  
}

void FootprintBuilder::onExit(int tick) {

  // collect the last reuse intervals
  FS->N = tick;

  for(int array = 0; array < FS->NArray; ++array) {
    // traverse the entries in stamp_table
    for( auto& iter : stamp_table[array] ) {

      AddressInt tmp = tick - iter.second;
      AddressInt idx = sublog_value_to_index<MAX_WINDOW, SUBLOG_BITS>(tmp);
      FS->reuses[array].value[idx] ++;
      FS->reuses_i[array].value[idx] += tmp;
    }
  }

  unsigned tick_idx = sublog_value_to_index<MAX_WINDOW, SUBLOG_BITS>(FS->N);
  for(int array = 0; array < FS->NArray; ++array) {
    FS->footprints[array].length  = tick_idx;
    FS->reuses[array].length      = tick_idx;
    FS->reuses_i[array].length    = tick_idx;
    FS->true_reuses[array].length = tick_idx;
    FS->length                    = tick_idx;
  }
  for(int array = 0; array < FS->NArray; ++array) {
    unsigned long long sum = 0;
    for(int i = 1; i <= tick_idx; ++i) {
      sum += FS->true_reuses[array].value[i];
      FS->true_reuses[array].value[i] = sum;
    }
  }
}

//============================================================//
//
void calculateCacheMissImpl(GroupType& arrays, unsigned target_cache,
                            unsigned capacity, 
                            FootprintState* FS,
                            FootprintState* FS1,
                            unsigned *miss_out, unsigned *hits_out) {

  double footprint1, footprint2;
  unsigned long total_refs = 0, misses = 0;

  int total_arrays = arrays.size();

  unsigned start_index = 0;
  unsigned end_index = FS->length;

  for(int array = 0; array < total_arrays; ++array) {
    if (arrays[array] == target_cache) {
      total_refs += FS->R[array];
    }
  }
  

  // fast path: if the total footprint is smaller than
  //            the target cache size
  for(int array = 0; array < total_arrays; ++array) {
    if ( arrays[array] == target_cache ) {
      misses += FS->M[array];
    }
  }

  if ( misses <= capacity ) {
    // all data can be cached in L1 cache
    *miss_out = misses;
    *hits_out = total_refs - misses;
    return;
  }

  // binary search
  int low = start_index, high = end_index, mid = start_index;
  while( low <= high ) {
    mid = low + (high - low)/2;
    footprint1 = footprint2 = 0;
    for(int array = 0; array < total_arrays; ++array) {
      if ( arrays[array] == target_cache) {
        footprint1 += FS->footprints[array].value[mid];
        footprint2 += FS->footprints[array].value[mid+1];
      }
    }

    if ( capacity + 1 >  footprint1 && 
         capacity + 1 <= footprint2 ) {
      // found the footprint
      break;
    }
    else if ( capacity + 1 <= footprint1 ) {
      high = mid - 1;
    }
    else {
      low = mid + 1;
    }
  }

  for(int array = 0; array < total_arrays; ++array) {
    if ( arrays[array] == target_cache) {
      *hits_out += FS1->true_reuses[array].value[mid];
    }
  }
  
  *miss_out = total_refs - *hits_out;
   
}

//
std::vector<unsigned> 
calculateCacheMiss(GroupType& arrays, 
                   unsigned constant_cache_size, 
                   unsigned constant_cache_line_size,
                   unsigned L1_cache_size, 
                   unsigned L1_cache_line_size, 
                   unsigned texture_cache_size, 
                   unsigned texture_cache_line_size,
                   FootprintState* CST_FS,
                   FootprintState* L1D_FS,
                   FootprintState* TEX_FS) {
 
  unsigned l1d_hits = 0, tex_hits = 0, cst_hits = 0;
  unsigned l1d_misses = 0, tex_misses = 0, cst_misses = 0;

  // call the helper function to calculate the miss and hits
  calculateCacheMissImpl(arrays, L1D_CACHE, 
                         L1_cache_size/L1_cache_line_size, L1D_FS, L1D_FS,
                         &l1d_misses, &l1d_hits);

  // call the helper function to calculate the miss and hits
  calculateCacheMissImpl(arrays, TEX_CACHE, 
                         texture_cache_size/texture_cache_line_size, L1D_FS, TEX_FS,
                         &tex_misses, &tex_hits);

  // call the helper function to calculate the miss and hits
  calculateCacheMissImpl(arrays, CST_CACHE, 
                         constant_cache_size/constant_cache_line_size, CST_FS, CST_FS,
                         &cst_misses, &cst_hits);
  

  std::vector<unsigned> ret;
  ret.push_back(cst_hits);
  ret.push_back(cst_misses);
  ret.push_back(l1d_hits);
  ret.push_back(l1d_misses);
  ret.push_back(tex_hits);
  ret.push_back(tex_misses);

  return ret;
}

} // namespace footprint
