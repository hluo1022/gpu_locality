#ifndef _PORPLE_REUSE_DISTANCE_H_
#define _PORPLE_REUSE_DISTANCE_H_

#include<string.h>
#include<iostream>
#include<vector>
#include<list>
#include<stdint.h>
#include<unordered_map>
#include "common.h"


// number of markers
#define NUM_MARKERS 2000

// max distance
#define MAX_DIS 2000

namespace reuse_distance {

typedef uint64_t AddressInt;
typedef std::vector<int> GroupType;

const unsigned int SUBLOG_BITS  = 8;
const unsigned int MAX_WINDOW   = (65-SUBLOG_BITS)*(1<<SUBLOG_BITS);

inline AddressInt Base(AddressInt address, unsigned granularity) {
  return (address / granularity) * granularity;
}

class ReuseDistanceState {

  friend class ReuseDistanceBuilder;
  friend void
  calculateCacheMissImpl(GroupType& arrays, 
                         unsigned target_cache,
                         unsigned capacity, 
                         ReuseDistanceState* RDS,
                         unsigned *miss_out, 
                         unsigned *hits_out);

public:
  template<typename T>
  struct Histogram {
    unsigned long length;
    unsigned window[MAX_WINDOW];
    T value[MAX_WINDOW];
    Histogram() {
      memset(value, 0, sizeof(T)*MAX_WINDOW);
      memset(window, 0, sizeof(unsigned)*MAX_WINDOW);
    }
  };

  typedef Histogram<unsigned long> ReuseDistanceHisto;

private:
  bool initialized;

  ReuseDistanceHisto* reuses;

  unsigned NArray;
  unsigned* R;

  ReuseDistanceState() : initialized(false) {}
  void setupReuseDistanceHisto(unsigned array_num);

public:
  // static method to get the footprint state 
  static ReuseDistanceState* getReuseDistanceState();
  bool initialize(unsigned array_num);
  void cleanup();
  void print(std::ostream& out);
  inline bool isInitialized() const { return initialized; }
};

//============================================================//
class ReuseDistanceBuilder {

  // the total volume of data accessed
  unsigned int* M;

  struct stack_cell_type {
    AddressInt addr;
    unsigned int dis;
  };

  class stack_type {

    typedef std::unordered_map<AddressInt, std::list<stack_cell_type>::iterator> map_type;
    typedef std::list<stack_cell_type> list_type;
    map_type cache_map;
    list_type cache_list;

  public:

    typedef std::list<stack_cell_type>::iterator iterator;

    bool find(AddressInt addr, iterator* cell) {
      map_type::iterator iter = cache_map.find(addr);
      if ( iter == cache_map.end() ) {
        return false;
      }
      *cell = iter->second;
      return true;
    }

    iterator insert(stack_cell_type cell) {
      cache_list.push_front(cell);
      cache_map[cell.addr] = cache_list.begin();
      return cache_map[cell.addr];
    }

    void remove(iterator iter) {
      stack_cell_type cell = *iter;
      cache_map.erase(cell.addr);
      cache_list.erase(iter);
    }

    inline iterator back() { 
      iterator iter = cache_list.end(); 
      return --iter;
    }

// Hao hack begin
    inline void pop_back() {
      cache_map.erase(cache_list.rbegin()->addr);
      cache_list.pop_back();
    }

    inline size_t size() {
      return cache_list.size();
    }
// Hao hack end
  };

  struct marker_type {
    unsigned count;
    unsigned maxDis; /* maxDis is the tight upper bound of the range */
    stack_type::iterator mkrCell;
  };

  class markers_type {

    marker_type impl[NUM_MARKERS];

    public:

    marker_type& operator[](size_t i) { return impl[i]; }

    int lastMarker;

    markers_type() {
   
      int base = 2, logSize = 11, cnt = 1, log = 2;
      int i;

      impl[0].maxDis = 0;
      while( cnt <= logSize ) {
        impl[cnt].maxDis = log;
        log *= base;
        cnt ++;
      }

      for(i=logSize+1; i<NUM_MARKERS; i++)
        impl[i].maxDis = impl[i-1].maxDis + impl[logSize].maxDis;

      for(i=0; i<NUM_MARKERS; i++) {
        impl[i].count = 0;
      }
    }
  };


private:
  // an array of markers, each marker points to a cell on the stack
  markers_type* markers;

  // a stack holding the addresses
  stack_type*   stack;

  ReuseDistanceState* RDS;
 
private:
  ReuseDistanceBuilder() {}

public:
  ReuseDistanceBuilder(ReuseDistanceState* _rds) : RDS(_rds) {
    markers = new markers_type[RDS->NArray];
    stack   = new stack_type[RDS->NArray];
    M       = new unsigned int[RDS->NArray]();
  }

  ~ReuseDistanceBuilder() {
    delete[] markers;
    delete[] stack;
    delete[] M;
  }

  void onReference(int array, AddressInt address, int tick, unsigned granularity);
  void calculateReuseDistance(int array);

};

//============================================================//
void calculateCacheMissImpl(GroupType& arrays,        // input array placement
                            unsigned target_cache,    // the target cache
                            unsigned capacity,        // the target footprint
                            ReuseDistanceState* RDS,  // reuse distance state
                            unsigned *miss_out,       // the calculated misses
                            unsigned *hits_out);      // the calculated hits

std::vector<unsigned> 
calculateCacheMiss(GroupType& arrays, 
                   unsigned const_cache_size, 
                   unsigned const_cache_line_size, 
                   unsigned L1_cache_size, 
                   unsigned L1_cache_line_size, 
                   unsigned texture_cache_size, 
                   unsigned texture_cache_line_size,
                   ReuseDistanceState* cst_rds,
                   ReuseDistanceState* l1d_rds,
                   ReuseDistanceState* tex_rds);

} // namespace reuse_distance

#endif
