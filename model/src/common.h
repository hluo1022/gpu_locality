#ifndef _PORPLE_COMMON_TYPES_H_
#define _PORPLE_COMMON_TYPES_H_

enum CacheType {
  L1D_CACHE = 0,
  TEX_CACHE,
  CST_CACHE,
  NONE
};

extern
unsigned line_addr_to_set(unsigned long line_addr,
                          unsigned long addr,
                          unsigned num_sets);

#endif
