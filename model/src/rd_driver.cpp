#include <iostream>
#include <fstream>
#include <vector>
#include <sys/time.h>
#include "reusedistance.h"
#include "coalescer.h"

/*
 * A test case that demonstrates how to use reuse distance to calculate
 * cache miss ratios for various GPU caches
 *
 * The main data structures used to model reuse distance are:
 *   1. ReuseDistanceState
 *   2. ReuseDistanceBuilder
 *
 * ReuseDistanceState contains the necessary information for reuse distance
 * computing, i.e. reuse distance histogram. The reuse distance histogram
 * is associated to an array, each entry in the array is the count of 
 * reuses in that range of the reuse distances.
 * Each ReuseDistanceState is associated to a ReuseDistanceBuilder
 * which can read the memory trace, build the reuse distance histogram
 * and compute footprint.
 *
 * The function "calculateCacheMissImpl" is used to compute the
 * miss ratio from a given ReuseDistanceState and a cache capacity.
 * It takes a group of arrays, a cache capacity and a ReuseDistanceState
 * as arguments, and store the predicted hit/miss counts in the
 * input arguments "hit_out" and "miss_out".
 *
 * The function "calculateCacheMiss" is a wrapper over "calculateCacheMissImpl"
 * which takes a set of GPU cache configurations as input and outputs
 * the predicted miss ratios as a vector.
 *
 * NOTE:
 *
 * 1. In account for memory coalescing in GPUs, we also implemented
 * a set of classes, MemoryCoalescer. It takes a memory reference's
 * relavant information as input and returns a boolean. The returned
 * value tells the client whether this reference has been coalesced.
 * If the reference is coalesced, the reference can be ignored,
 * because some prior reference has accessed the same cacheline/sector
 * If not, we can continue profiling reference.
 *
 * 2. In account for conflict miss, which is important for GPU applications,
 * we maintain an group of ReuseDistanceStates and ReuseDistanceBuilders. 
 * Each pair of ReuseDistanceState and ReuseDistanceBuilder is used to model
 * the cache misses within a cache set. The clients can provide their
 * own code for cache set hashing. The macro CACHESETS is used to specify
 * the count of cache sets. If the cache sets are fewer than CACHESETS,
 * we can combine the footprint and reuse time histogram of some cache
 * sets to obtain the miss ratios for smaller cache sizes. If the 
 * cache sets are more than CACHESETS, we can simply assume the 
 * majority of cache misses is capacity misses. Naturally when CACHESETS 
 * is set to 1, only capacity miss is modeled.
 *
 * 3. Three types of caches are currently supported: CST_CACHE, L1D_CACHE
 * and TEX_CACHE. NONE represents no caching at all.
 */

typedef unsigned long long timestamp_t;

#define CACHESETS 1

namespace {

timestamp_t get_timestamp() {
  struct timeval now;
  gettimeofday(&now, NULL);
  return now.tv_usec + (timestamp_t)now.tv_sec * 1000000;
}

}

using namespace std;
using namespace reuse_distance;

int main(int argc, char* argv[]) {

  AddressInt access_Address;
  int Array_ID, type, occurs_ID, loop_ID, thread_ID, block_ID;
  int tick = 0;
  ifstream inFile;

  /* open data/trace file */
  inFile.open(argv[1], std::ios_base::in);
  if ( !inFile ) {
    std::cerr << "unable to open file " << argv[1] << " for reading" << std::endl;
    return 1;
  }
  
  ReuseDistanceState   *l1d_fs[CACHESETS], *tex_fs[CACHESETS], *cst_fs[CACHESETS];
  ReuseDistanceBuilder *l1d_fb[CACHESETS], *tex_fb[CACHESETS], *cst_fb[CACHESETS];

  timestamp_t t0 = get_timestamp();

  /* initialize the ReuseDistanceState data structure */
  for(int i = 0; i < CACHESETS; i++) {
    l1d_fs[i] = ReuseDistanceState::getReuseDistanceState();
    tex_fs[i] = ReuseDistanceState::getReuseDistanceState();
    cst_fs[i] = ReuseDistanceState::getReuseDistanceState();
  }

  /* allocate space for maximum of 4 arrays */
  for(int i = 0; i < CACHESETS; i++) {
    l1d_fs[i]->initialize(7);
    tex_fs[i]->initialize(7);
    cst_fs[i]->initialize(7);
  }

  /* declare ReuseDistanceBuilder data structure */
  for(int i = 0; i < CACHESETS; i++) {
    l1d_fb[i] = new ReuseDistanceBuilder(l1d_fs[i]);
    tex_fb[i] = new ReuseDistanceBuilder(tex_fs[i]);
    cst_fb[i] = new ReuseDistanceBuilder(cst_fs[i]);
  }

  /* the memory coalescer */
  L1CacheCoalescer l1d_mc;
  ConstantCacheCoalescer cst_mc;
  TextureCacheCoalescer tex_mc;

  timestamp_t t1 = get_timestamp();

  std::vector<int> Arrays;
  std::vector<int> Blocks;
  std::vector<int> Threads;
  std::vector<AddressInt> Addresses;
  int e = 0;

  /* read the trace file line by line */
  while ( !inFile.eof() ) {
    inFile >> dec >> Array_ID 
                  >> type 
                  >> occurs_ID 
                  >> loop_ID 
                  >> thread_ID 
           >> hex >> access_Address
           >> dec >> block_ID;
    Arrays.push_back(Array_ID);
    Addresses.push_back(access_Address);
    Threads.push_back(thread_ID);
    Blocks.push_back(block_ID);
    e++;
  }

  timestamp_t t2 = get_timestamp();
  for(int i = 0; i < e; ++i) {
    Array_ID = Arrays[i];
    access_Address = Addresses[i];
    thread_ID = Threads[i];
    block_ID = Blocks[i];
    tick++;

    /* call ReuseDistanceBuilder's reference callback */

    /* Array_ID       is the array,
     * access_Address is the address of the reference
     * tick           is a logic clock maintained by the client
     * 128            is the L1 cacheline size
     */
    int warp_ID = thread_ID / 32;
    int bytes = 4;
    int set = 0;
    if( l1d_mc.onCoalesce(access_Address, bytes, thread_ID, warp_ID, block_ID, 128) ) {
//      int set = line_addr_to_set(access_Address/128, access_Address, CACHESETS);
      //int set = (access_Address / 128) % CACHESETS;
      l1d_fb[set]->onReference(Array_ID, access_Address, tick, 128);
    }

    if( cst_mc.onCoalesce(access_Address, bytes, thread_ID, warp_ID, block_ID, 128) ) {
//      int set = (access_Address / 128) % CACHESETS;
      cst_fb[set]->onReference(Array_ID, access_Address, tick, 128);
    }

    if( tex_mc.onCoalesce(access_Address, bytes, thread_ID, warp_ID, block_ID, 32) ) {
//      int set = (access_Address / 32) % CACHESETS;
      tex_fb[set]->onReference(Array_ID, access_Address, tick, 32);
    }
  }

  timestamp_t t3 = get_timestamp();


  /* calculate the footprints for array 0..4 */
  for(int i = 0; i < CACHESETS; i++) {
    for(int a = 0 ; a < 7; a++) {
      l1d_fb[i]->calculateReuseDistance(a);
      tex_fb[i]->calculateReuseDistance(a);
      cst_fb[i]->calculateReuseDistance(a);
    }
  }

  timestamp_t t4 = get_timestamp();

  /* an input array vector */
  std::vector<int> arrays;
  std::vector<unsigned> stat;
  std::vector<unsigned> total_stat(6,0);

  arrays.push_back(NONE);
  arrays.push_back(L1D_CACHE);
  arrays.push_back(L1D_CACHE);
  arrays.push_back(L1D_CACHE);
  //arrays.push_back(L1D_CACHE);
  //arrays.push_back(L1D_CACHE);
  //arrays.push_back(L1D_CACHE);
  arrays.push_back(NONE);
  arrays.push_back(NONE);

  timestamp_t t5 = get_timestamp();

  /* calculate miss ratios */
  for(int i = 0; i < CACHESETS; i++) {
    int hits = 0, misses = 0;

    stat = calculateCacheMiss(arrays, 
                              64*1024, 128,  // Cst cache size 64Kb, cacheline size 128b
                              48*1024, 128,  // L1D cache size 48kb, cacheline size 128b
                              12*1024, 32,   // Tex cache size 12kb, cacheline size 32b
                              cst_fs[i], l1d_fs[i], tex_fs[i]);
    for(int j = 0; j < 6; j++)
      total_stat[j] += stat[j];
  }
  
  timestamp_t t6 = get_timestamp();

  /* output */
  std::cout << "Const Hits : "   << total_stat[0] << "\n";
  std::cout << "Const Misses : " << total_stat[1] << "\n";
  std::cout << "Const Ratio : "  << total_stat[1] * 1.0 / (total_stat[1] + total_stat[0]) << "\n";
  std::cout << "L1d   Hits : "   << total_stat[2] << "\n";
  std::cout << "L1d   Misses : " << total_stat[3] << "\n";
  std::cout << "L1d   Ratio : "  << total_stat[3] * 1.0 / (total_stat[2] + total_stat[3]) << "\n";
  std::cout << "Tex   Hits : "   << total_stat[4] << "\n";
  std::cout << "Tex   Misses : " << total_stat[5] << "\n";
  std::cout << "Tex   Ratio : "  << total_stat[5] * 1.0 / (total_stat[5] + total_stat[4]) << "\n";
  std::cout << "Initialization time : " << (t1 - t0) / 1000000.0L << " sec.\n"; 
  std::cout << "Trace scanning time : " << (t3 - t2) / 1000000.0L << " sec.\n"; 
  std::cout << "ReuseDistance calc time : " << (t4 - t3) / 1000000.0L << " sec.\n"; 
  std::cout << "Miss ratio     time : " << (t6 - t5) / 1000000.0L << " sec.\n"; 

  for(int i = 0 ; i < CACHESETS; i++) {
    l1d_fs[i]->cleanup(); delete l1d_fs[i]; delete l1d_fb[i];
    tex_fs[i]->cleanup(); delete tex_fs[i]; delete tex_fb[i];
    cst_fs[i]->cleanup(); delete cst_fs[i]; delete cst_fb[i];
  }

  // close the file
  inFile.close();

}
