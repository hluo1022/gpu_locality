#include <iostream>
#include "footprint.h"

using namespace std;
using namespace footprint;

int main() {
  int tick = 0;

  FootprintState* fs = FootprintState::getFootprintState();
  fs->initialize(2);

  FootprintBuilder fb(fs);
  fb.onReference(1, 0x1000, ++tick, 64);
  fb.onReference(1, 0x2000, ++tick, 64);
  fb.onReference(0, 0x200000, ++tick, 64);
  fb.onReference(0, 0x200000, ++tick, 64);
  fb.onExit(tick);
  fb.calculateFootprint(0);
  fb.calculateFootprint(1);

  fs->print(cout);

  fs->cleanup();
  delete fs;
}
