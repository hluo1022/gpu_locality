/*
 * locality.cpp
 *
 * This file contains two basic algorithms to
 * measure locality.
 *   1. use footprint to predict miss ratio
 *   2. use reuse distance to predict miss ratio
 *
 * These two algorithms are placed in two different
 * namespaces. A usage example is given in test.cpp
 *
 * More detail about the footprint method can be 
 * found in LOCA tool
 *
 *   https://github.com/dcompiler/loca
 *
 * The reuse distance is implemented using unordered_map
 * which requires a compilation flag '-std=c++11'
 * Using map is also ok. The implementation maintains
 * a combination of stack and hashmap. It also has a 
 * static marker array, whose entry points to an entry
 * in the stack of a particular depth.
 *
 * Original code was written by URCS compiler group
 * Rewritten by Hao Luo
 *
 */

#include "reusedistance.h"

using namespace std;

namespace reuse_distance {

//============================================================//
ReuseDistanceState* ReuseDistanceState::getReuseDistanceState() {
//  static FootprintState fs;
//  return &fs;
  return new ReuseDistanceState;
}

void ReuseDistanceState::setupReuseDistanceHisto(unsigned array_num) {

  reuses = new ReuseDistanceHisto[array_num];

  R = new unsigned[array_num];
  for(int i = 0; i < array_num; ++i)
    R[i] = 0;
}

void ReuseDistanceState::cleanup() {
  delete[] reuses;
  delete[] R;
}
bool ReuseDistanceState::initialize(unsigned array_num) {

  // if already initialized, don't do anything
  if (initialized) return false;

  setupReuseDistanceHisto(array_num);

  NArray = array_num;

  initialized = true;
  return true;
}

void ReuseDistanceState::print(std::ostream& out) {
  for(int a = 0; a < NArray; a++) {
    out << "array : " << a << "\n";
    out << "reuse distance\n";
    for(int i=0; i<=reuses[a].length; ++i) {
      out << reuses[a].window[i] << " " << reuses[a].value[i] << "\n";
    }
    out << "\n";
  }
}

//============================================================//
void ReuseDistanceBuilder::onReference(int array, AddressInt address, int tick, unsigned granularity) {

  unsigned dis;
  unsigned i;
  AddressInt addr = Base(address, granularity);

// Hao hack begin 
  RDS->R[array]++;
// Hao hack end 

  // prepare cell for inserting
  stack_cell_type cell;
  cell.addr = addr;
  cell.dis  = 0;

  stack_type::iterator iter;
  if (  !stack[array].find(addr, &iter) ) {
    // not find addr in stack
    // first time accessing addr

    M[array]++;
    if ( M[array] == 1 ) {
      // very first access of the entire trace
      iter = stack[array].insert(cell);
      markers[array][0].mkrCell  = iter;
      markers[array].lastMarker  = 1;
      return;
    }
 
    // this is not the first access of the trace,
    // but the first access to this data
    dis = markers[array].lastMarker - 1;
    // Hao hack begin
    if ( M[array] <= MAX_DIS ) {
    // Hao hack end
      if ( M[array] == markers[array][markers[array].lastMarker].maxDis+1 ) {
        markers[array][markers[array].lastMarker].mkrCell = stack[array].back();
        markers[array].lastMarker++;
      }
    // Hao hack begin
    }
    // Hao hack end
  }
  else {
  
    // found addr in the stack
    if ( iter == markers[array][0].mkrCell) {
      // access the top element
      markers[array][0].count++;
      return;
    }

    // access element in the middle of the stack
    //
    dis = iter->dis - 1;
    
    // update markers
    markers[array][dis+1].count++;

    // a tricky case: the cell is a marker
    if ( markers[array][dis+1].mkrCell == iter ) {
      markers[array][dis+1].mkrCell = --iter;
      ++iter;
    }

    // delete the element
    stack[array].remove(iter);
  }

  /* update the markers */
  markers[array][0].mkrCell->dis = 1;
  for(i=1; i<=dis; i++) {
    markers[array][i].mkrCell->dis = i+1;
    --markers[array][i].mkrCell;
  }

  /* insert the cell at the head of stack */
  iter = stack[array].insert(cell);
  // Hao hack begin
  if ( stack[array].size() > MAX_DIS ) {
    stack[array].pop_back();
  }
  // Hao hack end
  markers[array][0].mkrCell = iter;
}

void ReuseDistanceBuilder::calculateReuseDistance(int array) {

  int i;
  unsigned total = 0;
  for(i=0; i<=markers[array].lastMarker+1; i++) {
    total += markers[array][i].count;
    RDS->reuses[array].window[i] = markers[array][i].maxDis;
    RDS->reuses[array].value[i] = total;
  } 
  RDS->reuses[array].length = markers[array].lastMarker+1;

}

//============================================================//
void calculateCacheMissImpl(GroupType& arrays,        // input array placement
                            unsigned target_cache,    // the target cache
                            unsigned capacity,        // the target footprint
                            ReuseDistanceState* RDS,  // reuse distance state
                            unsigned *miss_out,       // the calculated misses
                            unsigned *hits_out)       // the calculated hits
{
  unsigned long total_refs = 0, hits = 0;
  int total_arrays = arrays.size();
  int partitions = 0;
  unsigned partitioned_capacity;

  for(int array = 0; array < total_arrays; ++array) {
    if (arrays[array] == target_cache) {
//      std::cout << "R " << FS->R[array] << " ";
      total_refs += RDS->R[array];
      partitions++;
    }
  }

  if ( partitions == 0 ) return;

  partitioned_capacity = capacity / partitions;
  
  for(int array = 0; array < total_arrays; ++array) {
    if (arrays[array] == target_cache) {
      // binary search
      int low = 0, high = RDS->reuses[array].length, mid = 0;
      while( low <= high ) {
        mid = low + (high - low)/2;
        if ( partitioned_capacity >= RDS->reuses[array].window[mid] &&
             partitioned_capacity < RDS->reuses[array].window[mid+1] ) {
          // found the reuse distance
          break;
        }
        else if ( partitioned_capacity < RDS->reuses[array].window[mid] ) {
          high = mid - 1;
        }
        else {
          low = mid + 1;
        }
      }
//      std::cout << RDS->reuses[array].window[mid] << " " 
//                << RDS->reuses[array].value[mid]  << "\n";
      hits += RDS->reuses[array].value[mid];
    }
  }

  *hits_out = hits;
  *miss_out = total_refs - hits;
  
}

std::vector<unsigned> 
calculateCacheMiss(GroupType& arrays, 
                   unsigned const_cache_size, 
                   unsigned const_cache_line_size, 
                   unsigned L1_cache_size, 
                   unsigned L1_cache_line_size, 
                   unsigned texture_cache_size, 
                   unsigned texture_cache_line_size,
                   ReuseDistanceState* cst_rds,
                   ReuseDistanceState* l1d_rds,
                   ReuseDistanceState* tex_rds)
{
  unsigned l1d_hits = 0, tex_hits = 0, cst_hits = 0;
  unsigned l1d_misses = 0, tex_misses = 0, cst_misses = 0;

  // call the helper function to calculate the miss and hits
  calculateCacheMissImpl(arrays, L1D_CACHE, 
                         L1_cache_size/L1_cache_line_size, l1d_rds,
                         &l1d_misses, &l1d_hits);

  // call the helper function to calculate the miss and hits
  calculateCacheMissImpl(arrays, TEX_CACHE, 
                         texture_cache_size/texture_cache_line_size, tex_rds,
                         &tex_misses, &tex_hits);

  // call the helper function to calculate the miss and hits
  calculateCacheMissImpl(arrays, CST_CACHE, 
                         const_cache_size/const_cache_line_size, cst_rds,
                         &cst_misses, &cst_hits);
  

  std::vector<unsigned> ret;
  ret.push_back(cst_hits);
  ret.push_back(cst_misses);
  ret.push_back(l1d_hits);
  ret.push_back(l1d_misses);
  ret.push_back(tex_hits);
  ret.push_back(tex_misses);

  return ret;
}

}; // namespace reuse_distance

