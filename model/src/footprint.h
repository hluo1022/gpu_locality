#ifndef _PORPLE_FOOTPRINT_H_
#define _PORPLE_FOOTPRINT_H_

#include<iostream>
#include<vector>
#include<stdint.h>
#include<unordered_map>
#include "common.h"

namespace footprint {

typedef uint64_t AddressInt;
typedef std::vector<int> GroupType;

const unsigned int SUBLOG_BITS  = 8;
const unsigned int MAX_WINDOW   = (65-SUBLOG_BITS)*(1<<SUBLOG_BITS);

inline AddressInt Base(AddressInt address, unsigned granularity) {
  return (address / granularity) * granularity;
}

//============================================================//

class FootprintState {

  friend class FootprintBuilder;
  friend void
  calculateCacheMissImpl(GroupType& arrays, 
                         unsigned target_cache,
                         unsigned capacity, 
                         FootprintState* FS1,
                         FootprintState* FS2,
                         unsigned *miss_out, 
                         unsigned *hits_out);

public:
  template<typename T>
  struct Histogram {
    unsigned long length;
    T value[MAX_WINDOW];
    Histogram() {
      memset(value, 0, sizeof(T)*MAX_WINDOW);
    }
  };

  typedef Histogram<unsigned long> ReuseTimeHisto;
  typedef Histogram<double> FootprintCurve;

private:
  bool initialized;

  ReuseTimeHisto* reuses;
  ReuseTimeHisto* reuses_i;
  ReuseTimeHisto* true_reuses;

  unsigned* M;
  unsigned length;
  unsigned N;
  unsigned NArray;
  unsigned* R;

  FootprintCurve* footprints;

  FootprintState() : initialized(false) {}
  void setupReuseTimeHisto(unsigned array_num);
  void setupFootprint(unsigned array_num);

public:
  // static method to get the footprint state 
  static FootprintState* getFootprintState();
  bool initialize(unsigned array_num);
  void cleanup();
  void print(std::ostream& out);
  inline bool isInitialized() const { return initialized; }
};

//============================================================//
class FootprintBuilder {

/*
public:
  typedef struct {
    int time;
    void* ptr;
  } stamp_table_entry;
*/

private:
  FootprintState* FS;
 
  std::unordered_map<AddressInt, int>* stamp_table;
  //int* stamp_table;

private:
  FootprintBuilder() {}

public:
  FootprintBuilder(FootprintState* _fs) : FS(_fs) {
    stamp_table = new std::unordered_map<AddressInt, int>[FS->NArray];
  }

  ~FootprintBuilder() {
    delete[] stamp_table;
  }

  void onReference(int array, AddressInt address, int tick, unsigned granularity);
  void onExit(int tick);
  void calculateFootprint(int array);

};

//============================================================//
void calculateCacheMissImpl(GroupType& arrays,      // input array placement
                            unsigned target_cache,  // the target cache
                            unsigned capacity,      // the target footprint
                            FootprintState* FS,     // footprint state
                            FootprintState* FS1,     // footprint state
                            unsigned *miss_out,     // the calculated misses
                            unsigned *hits_out);    // the calculated hits

std::vector<unsigned> 
calculateCacheMiss(GroupType& arrays, 
                   unsigned const_cache_size, 
                   unsigned const_cache_line_size, 
                   unsigned L1_cache_size, 
                   unsigned L1_cache_line_size, 
                   unsigned texture_cache_size, 
                   unsigned texture_cache_line_size,
                   FootprintState* cst_fs,
                   FootprintState* l1d_fs,
                   FootprintState* tex_fs);

} // namespace footprint

#endif
