#ifndef _GPU_TRACE_TYPE_H_
#define _GPU_TRACE_TYPE_H_

#include<stdint.h>

typedef uint64_t AddressInt;

class Record {
public:
  int array;
  int type;
  int occurs;
  int loop;
  int thread;
  int warp;
  AddressInt address;
  int block;

  Record() {}
  Record(int a, int ty, int o, int l, int td, AddressInt addr, int b) {
    array = a;
    type = ty;
    occurs = o;
    loop = l;
    thread = td;
    warp = thread / 32;
    address = addr;
    block = b;
  }

};


#endif
