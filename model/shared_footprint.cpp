/*
 * locality.cpp
 *
 * This file contains two basic algorithms to
 * measure locality.
 *   1. use footprint to predict miss ratio
 *   2. use reuse distance to predict miss ratio
 *
 * These two algorithms are placed in two different
 * namespaces. A usage example is given in test.cpp
 *
 * More detail about the footprint method can be 
 * found in LOCA tool
 *
 *   https://github.com/dcompiler/loca
 *
 * The reuse distance is implemented using unordered_map
 * which requires a compilation flag '-std=c++11'
 * Using map is also ok. The implementation maintains
 * a combination of stack and hashmap. It also has a 
 * static marker array, whose entry points to an entry
 * in the stack of a particular depth.
 *
 * Original code was written by URCS compiler group
 * Rewritten by Hao Luo
 *
 */

#include <iostream>
#include <fstream>
#include <map>
#include <list>
#include <math.h>
#include "types.h"
#include "gpu_def.h"
#include "histo.H"
#include "locality.h"

using namespace std;
using namespace histo;

namespace shared_footprint {


typedef struct record_struct {
  AddressInt time;
  int thread;
  record_struct(AddressInt s, int t) {
    time = s;
    thread = t;
  }
} RefRecord;
typedef list<RefRecord> stamp_list_type;

//  hash table, used to store the last reference time
std::map<addr_type, stamp_list_type> *stamp_table;

static struct stamp_table_allocator {
  stamp_table_allocator() {
    stamp_table = new std::map<addr_type, stamp_list_type>[MAP_SIZE];
  }
  ~stamp_table_allocator() {
    delete[] stamp_table;
  }
} __static_stamp_table_allocator;

const unsigned int SUBLOG_BITS  = 8;
const unsigned int MAX_WINDOW   = (65-SUBLOG_BITS)*(1<<SUBLOG_BITS);

// N: the trace length
AddressInt N;
// R: the total reference cout
AddressInt R;
// M: the footprint volume
AddressInt M[MAX_WARPS_PER_SM];

// reuse length histograms
long long reuse_histo[MAX_WARPS_PER_SM][MAX_WINDOW];
long long reuse_histo2[MAX_WARPS_PER_SM][MAX_WINDOW];
long long reuse_histo_i[MAX_WARPS_PER_SM][MAX_WINDOW];

// array to hold the repetitive data references 
// to record the "unconditional" hits
long long reuse_histo2_base[MAX_WINDOW];

void on_reference(addr_type raw_addr, int thread_id, int tick) {

  // get the base address of addr's cacheline
  addr_type  addr = Base(raw_addr);
  AddressInt idx  = AddressMapIndex(addr);

  N = tick;

  stamp_list_type sl = stamp_table[idx][addr];
  stamp_list_type::iterator iter;
  int thd_count = 0;


  for(iter=sl.begin(); iter!=sl.end(); iter++) {

    AddressInt distance = N - iter->time - 1;
    AddressInt index = sublog_value_to_index<MAX_WINDOW, SUBLOG_BITS>(distance);

    //if ( distance < 1559 ) {
    //  cout << hex << "0x" << raw_addr << dec << tick << endl;
    //}
 
    if ( N > iter->time ) { 
      reuse_histo[thd_count][index] ++;
      reuse_histo_i[thd_count][index] += distance;
    }
    reuse_histo2[thd_count][(index+1)%MAX_WINDOW] ++;

    if ( iter->thread == thread_id ) { break; }

    thd_count++;
    
    if ( N > iter->time ) { 
      reuse_histo[thd_count][index] --;
      reuse_histo_i[thd_count][index] -= distance;
    }
  }

  if ( iter == sl.end() ) {
    AddressInt index = sublog_value_to_index<MAX_WINDOW, SUBLOG_BITS>(N-1);
    reuse_histo[thd_count][index] ++;
    reuse_histo_i[thd_count][index] += N - 1;

    M[thd_count] ++;
  }
  else {
    sl.erase(iter);
  }

  RefRecord rec(tick, thread_id);
  sl.push_front(rec);

  R++;
  stamp_table[idx][addr] = sl;
}

void on_exit(int tick) {

  AddressInt i;

  // collect the last reuse intervals
  
  N = tick;

  // traverse the entries in stamp_table
  for(i=0; i<MAP_SIZE; i++) {

    // traverse the entry in each entry of stamp_table
    for(map<addr_type, stamp_list_type>::iterator it = stamp_table[i].begin(); 
        it != stamp_table[i].end(); it++) {

      //traverse the entry in the stamp list
      int thd_count = 0;
      for(stamp_list_type::iterator iter = it->second.begin(); iter != it->second.end(); iter++) {

        AddressInt distance = N - iter->time;
        
        AddressInt idx = sublog_value_to_index<MAX_WINDOW, SUBLOG_BITS>(distance);
        reuse_histo[thd_count][idx] ++;
        reuse_histo_i[thd_count][idx] += distance;

        thd_count++;
      }

    }
  }

}

void print_stat(ostream& sfp_out, ostream& smith_in) {

  AddressInt ws;
  AddressInt i, j;

  double sfp[MAX_WARPS_PER_SM];
  double sum[MAX_WARPS_PER_SM];
  double sum_i[MAX_WARPS_PER_SM]; 

  for(i=0;i<MAX_WARPS_PER_SM;i++) {
    sum[i] = sum_i[i] = sfp[i] = 0;
  }

  for(i=0;i<MAX_WARPS_PER_SM;i++) {
    for(j=1;j<=sublog_value_to_index<MAX_WINDOW, SUBLOG_BITS>(N);j++){
      sum[i] += reuse_histo[i][j];
      sum_i[i] += reuse_histo_i[i][j];
    }
  }

  AddressInt N_index = sublog_value_to_index<MAX_WINDOW, SUBLOG_BITS>(N);

  smith_in << R << endl;
  for(j=1; j<=N_index; j++) {

    ws = sublog_index_to_value<MAX_WINDOW, SUBLOG_BITS>(j);
    sfp_out << ws;
    for(i=0; i<MAX_WARPS_PER_SM; i++) {
      sfp[i] = (double)sum_i[i] / (N-ws+1) - sum[i] * (((double)(ws-1))/(N-ws+1));
      sfp[i] = M[i] - sfp[i];

      sum_i[i] -= reuse_histo_i[i][j];
      sum[i]   -= reuse_histo[i][j];

      sfp_out << "\t" << setprecision(4) << sfp[i];
    }
    smith_in << floor(sfp[0]) << "\t" << reuse_histo2[0][j-1] << "\n";

    sfp_out << "\n";
  }
//  cout << R << endl;

}

}; // namespace shared_footprint

