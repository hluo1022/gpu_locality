#ifndef _GPU_COALESCER_
#define _GPU_COALESCER_

#include <stdint.h>

typedef uint64_t AddressInt;

class MemoryCoalescer {

public:
  virtual bool onCoalesce(AddressInt address, 
                          unsigned bytes, 
                          unsigned tid, 
                          int warp, 
                          int block, 
                          unsigned line_size) = 0;

};

class L1CacheCoalescer : public MemoryCoalescer {

private:
  AddressInt request_buffer[4][32];
  int request_counter[4];

  int last_warp;
  int last_block;
  int last_tid;

public:
  L1CacheCoalescer() : last_warp(0), last_block(0), last_tid(0) {
    for(int i = 0; i < 4; ++i) {
      request_counter[i] = 0;
    }
  }

  // the coalesce function, which returns true if the input address forms a new transaction and false otherwise
  bool onCoalesce(AddressInt address, unsigned bytes, unsigned tid, int warp, int block, unsigned line_size);
  
};

class ConstantCacheCoalescer : public MemoryCoalescer {

private:
  AddressInt request_buffer[32];
  int request_counter;

  int last_warp;
  int last_block;
  int last_tid;

public:
  ConstantCacheCoalescer() : last_warp(0), last_block(0), request_counter(0), last_tid(0) {
  }

  // the coalesce function, which returns true if the input address forms a new transaction and false otherwise
  bool onCoalesce(AddressInt address, unsigned bytes, unsigned tid, int warp, int block, unsigned line_size);
  
};

class TextureCacheCoalescer : public MemoryCoalescer {

private:
  AddressInt request_buffer[8][4];
  int request_counter[8];

  int last_warp;
  int last_block;
  int last_tid;

public:

  TextureCacheCoalescer() : last_warp(0), last_block(0), last_tid(0) {
    for(int i = 0; i < 8; ++i) {
      request_counter[i] = 0;
    }
  }

  // the coalesce function, which returns true if the input address forms a new transaction and false otherwise
  bool onCoalesce(AddressInt address, unsigned bytes, unsigned tid, int warp, int block, unsigned line_size);

};
#endif
