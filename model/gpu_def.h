#ifndef _GPU_DEF_H_
#define _GPU_DEF_H_

/*
 * cache configuration define
 */

/* L1 cache specification */

// cache capacity in (KB)
#define L1_CACHE_CAPACITY        16
#define L1_CACHELINE             128
#define L1_CACHE_ASSOCIATIVITY   128
#define L1_CACHE_LINES           (L1_CACHE_CAPACITY * 1024 / L1_CACHELINE)
#define L1_CACHE_SETS            (L1_CACHE_LINES / L1_CACHE_ASSOCIATIVITY)

#define L1CachelineBase(x)       (((x) / L1_CACHELINE)*L1_CACHELINE)

/* Texture cache specification */

// texture cache capacity in KB
#define TEX_CACHE_CAPACITY       12
#define TEX_CACHELINE            128
#define TEX_SECTOR               32
#define TEX_CACHE_ASSOCIATIVITY  8 //TODO I need to verify this
#define TEX_SECTORS_PER_LINE     (TEX_CACHELINE / TEX_SECTOR)
#define TEX_SECTORS_PER_SET      (TEX_CACHE_ASSOCIATIVITY * TEX_SECTORS_PER_LINE)
#define TEX_CACHE_LINES          (TEX_CACHE_CAPACITY * 1024 / TEX_CACHELINE)
#define TEX_CACHE_SECTORS        (TEX_CACHE_CAPACITY * 1024 / TEX_SECTOR)
#define TEX_CACHE_SETS           (TEX_CACHE_LINES / TEX_CACHE_ASSOCIATIVITY)

#define TexCachelineBase(x)      (((x) / TEX_CACHELINE)*TEX_CACHELINE)
#define TexSectorBase(x)         (((x) / TEX_SECTOR)*TEX_SECTOR)

/* unified definition of cache specification */

#define CACHE_CAPACITY           L1_CACHE_CAPACITY
#define CACHELINE                L1_CACHELINE
#define CACHE_ASSOCIATIVITY      L1_CACHE_ASSOCIATIVITY
#define CACHE_LINES              L1_CACHE_LINES
#define CACHE_SETS               L1_CACHE_SETS

/*
 * Coalesce related define
 */

#define COALESCE_THREAD_SIZE  32
#define COALESCE_MEMORY_SIZE  128
#define COALESCE_STRIDE       (COALESCE_MEMORY_SIZE / COALESCE_THREAD_SIZE)

#define COALESCE_QUAD         4

/*
 * Scaling related define
 */

#define MAX_WARPS_PER_SM              64
#define MAX_THREADS_PER_SM            (MAX_WARPS_PER_SM * 32)
#define MAX_THREAD_BLOCKS_PER_SM      16
#define MAX_THREADS_PER_THREAD_BLOCK  1024

#endif

