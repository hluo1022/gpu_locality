#include <iostream>
#include "gpu_def.h"
#include "cache_sim.h"

using namespace std;

void tex_cache_sim::on_reference(AddressInt raw_addr) {

  int i;
  total++;
  AddressInt line_base = CachelineBase(raw_addr);
  AddressInt sector_base = SectorBase(raw_addr);

  unsigned set_index = SetIndex(line_base);
  unsigned sector_index = (sector_base - line_base) / TEX_SECTOR;
        //cout << hex << raw_addr << " " << set_index << " 0"  << endl;
  
  for(i = 0; i < TEX_CACHE_ASSOCIATIVITY; i++) {

    if ( cache[set_index][i] == line_base ) {
      unsigned v = valid[set_index][i] >> sector_index;
      if (v%2) {
        // a full hit
        //
        // move the cachelines
        hits++;
        //cout << "H:" <<  hex << raw_addr << " " << set_index << " " << i << " " << dec << valid[set_index][i] << endl;
      }
      else {
        // a partial miss
        //
        // move the cachelines and set the flags
        misses++;
        valid[set_index][i] |= (1 << sector_index);
        //cout << "M:" <<  hex << raw_addr << " " << set_index << " " << i << " " << dec << valid[set_index][i] << endl;
      }

      unsigned temp_valid = valid[set_index][i];

      for(int j = i; j >= 1; --j ) {
        cache[set_index][j] = cache[set_index][j-1];
        valid[set_index][j] = valid[set_index][j-1];
      }
      cache[set_index][0] = line_base;
      valid[set_index][0] = temp_valid;
      return;     
    }
  }

  // miss
  misses++;
  unsigned temp_valid = 1 << sector_index;
  for(int j = TEX_CACHE_ASSOCIATIVITY-1; j >= 1; --j ) {
    cache[set_index][j] = cache[set_index][j-1];
    valid[set_index][j] = valid[set_index][j-1];
  }
  cache[set_index][0] = line_base;
  valid[set_index][0] = temp_valid;
  //cout << "M:" << hex << raw_address << " " << set_index << " " << 0 << " " << dec << valid[set_index][0] << endl;
  return;

}

void tex_cache_sim::print_stat() {

  std::cout << "\n============== SIM ================\n" << std::endl;
  std::cout << "Cache capacity : " << TEX_CACHE_CAPACITY << " KB" << std::endl;
  std::cout << "Cacheline size : " << TEX_CACHELINE      << " B"  << std::endl;
  std::cout << "Sector size    : " << TEX_SECTOR         << " B"  << std::endl;
  std::cout << "Cache assoc.   : " << TEX_CACHE_ASSOCIATIVITY << "-way" << std::endl;
  std::cout << "Total accesses : " << total << std::endl;
  std::cout << "Hits           : " << hits << std::endl;
  std::cout << "Misses         : " << misses << std::endl;
  std::cout << "Miss ratio     : " << ((double)misses)/total << std::endl;

}
