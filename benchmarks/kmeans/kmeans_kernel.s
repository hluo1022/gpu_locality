; ModuleID = './kmeans_kernel.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v16:16:16-v32:32:32-v64:64:64-v128:128:128-n16:32:64"
target triple = "nvptx-nvidia-cl.1.0"

%struct.int4 = type { i32, i32, i32, i32 }
%struct.float4 = type { float, float, float, float }

@t_features = internal addrspace(1) global i64 0, align 8
@t_features_flipped = internal addrspace(1) global i64 0, align 8
@t_clusters = internal addrspace(1) global i64 0, align 8
@c_clusters = internal addrspace(4) global [1088 x float] zeroinitializer, align 4
@"_Z11kmeansPointPfiiiPiS_S_S0_$__cuda_local_var_36429_30_non_const_deltas" = internal addrspace(3) global [256 x i32] zeroinitializer, align 4
@"_Z11kmeansPointPfiiiPiS_S_S0_$__cuda_local_var_36475_30_non_const_new_center_ids" = internal addrspace(3) global [256 x i32] zeroinitializer, align 4
@llvm.used = appending global [8 x i8*] [i8* inttoptr (i64 ptrtoint (i64 addrspace(1)* @t_features to i64) to i8*), i8* inttoptr (i64 ptrtoint (i64 addrspace(1)* @t_features_flipped to i64) to i8*), i8* inttoptr (i64 ptrtoint (i64 addrspace(1)* @t_clusters to i64) to i8*), i8* inttoptr (i64 ptrtoint ([1088 x float] addrspace(4)* @c_clusters to i64) to i8*), i8* bitcast (void (float*, float*, i32, i32)* @_Z14invert_mappingPfS_ii to i8*), i8* inttoptr (i64 ptrtoint ([256 x i32] addrspace(3)* @"_Z11kmeansPointPfiiiPiS_S_S0_$__cuda_local_var_36429_30_non_const_deltas" to i64) to i8*), i8* inttoptr (i64 ptrtoint ([256 x i32] addrspace(3)* @"_Z11kmeansPointPfiiiPiS_S_S0_$__cuda_local_var_36475_30_non_const_new_center_ids" to i64) to i8*), i8* bitcast (void (float*, i32, i32, i32, i32*, float*, float*, i32*)* @_Z11kmeansPointPfiiiPiS_S_S0_ to i8*)], section "llvm.metadata"

define void @_Z14invert_mappingPfS_ii(float* %input, float* %output, i32 %npoints, i32 %nfeatures) {
  %input.addr = alloca float*, align 8
  %output.addr = alloca float*, align 8
  %npoints.addr = alloca i32, align 4
  %nfeatures.addr = alloca i32, align 4
  %__cuda_local_var_36369_6_non_const_point_id = alloca i32, align 4
  %__cuda_local_var_36370_6_non_const_i = alloca i32, align 4
  %predef_tmp_comp = alloca i32, align 4
  %predef_tmp_comp1 = alloca i32, align 4
  %predef_tmp_comp2 = alloca i32, align 4
  store float* %input, float** %input.addr, align 8
  store float* %output, float** %output.addr, align 8
  store i32 %npoints, i32* %npoints.addr, align 4
  store i32 %nfeatures, i32* %nfeatures.addr, align 4
  %1 = call i32 @llvm.nvvm.read.ptx.sreg.tid.x()
  store i32 %1, i32* %predef_tmp_comp, align 4
  %2 = load i32* %predef_tmp_comp, align 4
  %3 = call i32 @llvm.nvvm.read.ptx.sreg.ntid.x()
  store i32 %3, i32* %predef_tmp_comp1, align 4
  %4 = load i32* %predef_tmp_comp1, align 4
  %5 = call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x()
  store i32 %5, i32* %predef_tmp_comp2, align 4
  %6 = load i32* %predef_tmp_comp2, align 4
  %7 = mul i32 %4, %6
  %8 = add i32 %2, %7
  store i32 %8, i32* %__cuda_local_var_36369_6_non_const_point_id, align 4
  %9 = load i32* %__cuda_local_var_36369_6_non_const_point_id, align 4
  %10 = load i32* %npoints.addr, align 4
  %11 = icmp slt i32 %9, %10
  br i1 %11, label %12, label %37

; <label>:12                                      ; preds = %0
  store i32 0, i32* %__cuda_local_var_36370_6_non_const_i, align 4
  br label %13

; <label>:13                                      ; preds = %33, %12
  %14 = load i32* %__cuda_local_var_36370_6_non_const_i, align 4
  %15 = load i32* %nfeatures.addr, align 4
  %16 = icmp slt i32 %14, %15
  br i1 %16, label %17, label %36

; <label>:17                                      ; preds = %13
  %18 = load float** %input.addr, align 8
  %19 = load i32* %__cuda_local_var_36369_6_non_const_point_id, align 4
  %20 = load i32* %nfeatures.addr, align 4
  %21 = mul nsw i32 %19, %20
  %22 = load i32* %__cuda_local_var_36370_6_non_const_i, align 4
  %23 = add nsw i32 %21, %22
  %24 = getelementptr inbounds float* %18, i32 %23
  %25 = load float* %24, align 4
  %26 = load float** %output.addr, align 8
  %27 = load i32* %__cuda_local_var_36369_6_non_const_point_id, align 4
  %28 = load i32* %npoints.addr, align 4
  %29 = load i32* %__cuda_local_var_36370_6_non_const_i, align 4
  %30 = mul nsw i32 %28, %29
  %31 = add nsw i32 %27, %30
  %32 = getelementptr inbounds float* %26, i32 %31
  store float %25, float* %32, align 4
  br label %33

; <label>:33                                      ; preds = %17
  %34 = load i32* %__cuda_local_var_36370_6_non_const_i, align 4
  %35 = add nsw i32 %34, 1
  store i32 %35, i32* %__cuda_local_var_36370_6_non_const_i, align 4
  br label %13

; <label>:36                                      ; preds = %13
  br label %37

; <label>:37                                      ; preds = %36, %0
  ret void
}

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.tid.x() #0

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.ntid.x() #0

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #0

define void @_Z11kmeansPointPfiiiPiS_S_S0_(float* %features, i32 %nfeatures, i32 %npoints, i32 %nclusters, i32* %membership, float* %clusters, float* %block_clusters, i32* %block_deltas) {
  %features.addr = alloca float*, align 8
  %nfeatures.addr = alloca i32, align 4
  %npoints.addr = alloca i32, align 4
  %nclusters.addr = alloca i32, align 4
  %membership.addr = alloca i32*, align 8
  %clusters.addr = alloca float*, align 8
  %block_clusters.addr = alloca float*, align 8
  %block_deltas.addr = alloca i32*, align 8
  %__cuda_local_var_36391_21_non_const_block_id = alloca i32, align 4
  %__cuda_local_var_36393_21_non_const_point_id = alloca i32, align 4
  %__cuda_local_var_36395_6_non_const_index = alloca i32, align 4
  %__cuda_local_var_36452_15_non_const_threadids_participating = alloca i32, align 4
  %__cuda_local_var_36472_6_non_const_center_id = alloca i32, align 4
  %__cuda_local_var_36473_6_non_const_dim_id = alloca i32, align 4
  %__cuda_local_var_36486_6_non_const_new_base_index = alloca i32, align 4
  %__cuda_local_var_36487_8_non_const_accumulator = alloca float, align 4
  %predef_tmp_comp = alloca i32, align 4
  %predef_tmp_comp1 = alloca i32, align 4
  %predef_tmp_comp2 = alloca i32, align 4
  %predef_tmp_comp3 = alloca i32, align 4
  %predef_tmp_comp4 = alloca i32, align 4
  %predef_tmp_comp5 = alloca i32, align 4
  %__cuda_local_var_36399_7_non_const_i = alloca i32, align 4
  %__cuda_local_var_36399_10_non_const_j = alloca i32, align 4
  %__cuda_local_var_36400_9_non_const_min_dist = alloca float, align 4
  %__cuda_local_var_36401_9_non_const_dist = alloca float, align 4
  %__cuda_local_var_36405_8_non_const_cluster_base_index = alloca i32, align 4
  %__cuda_local_var_36406_10_non_const_ans = alloca float, align 4
  %__T210 = alloca i64, align 8
  %__T211 = alloca %struct.int4, align 16
  %__T212 = alloca %struct.float4, align 16
  %__cuda_local_var_36410_9_non_const_addr = alloca i32, align 4
  %__cuda_local_var_36411_11_non_const_diff = alloca float, align 4
  %agg.tmp = alloca %struct.int4, align 16
  %predef_tmp_comp6 = alloca i32, align 4
  %predef_tmp_comp7 = alloca i32, align 4
  %predef_tmp_comp8 = alloca i32, align 4
  %predef_tmp_comp9 = alloca i32, align 4
  %predef_tmp_comp10 = alloca i32, align 4
  %predef_tmp_comp11 = alloca i32, align 4
  %predef_tmp_comp12 = alloca i32, align 4
  %predef_tmp_comp13 = alloca i32, align 4
  %predef_tmp_comp14 = alloca i32, align 4
  %predef_tmp_comp15 = alloca i32, align 4
  %predef_tmp_comp16 = alloca i32, align 4
  %predef_tmp_comp17 = alloca i32, align 4
  %predef_tmp_comp18 = alloca i32, align 4
  %predef_tmp_comp19 = alloca i32, align 4
  %predef_tmp_comp20 = alloca i32, align 4
  %predef_tmp_comp21 = alloca i32, align 4
  %predef_tmp_comp22 = alloca i32, align 4
  %predef_tmp_comp23 = alloca i32, align 4
  %i = alloca i32, align 4
  %__T213 = alloca i64, align 8
  %__T214 = alloca i32, align 4
  %__T215 = alloca %struct.int4, align 16
  %__T216 = alloca %struct.float4, align 16
  %__cuda_local_var_36492_10_non_const_val = alloca float, align 4
  %agg.tmp24 = alloca %struct.int4, align 16
  %predef_tmp_comp26 = alloca i32, align 4
  %predef_tmp_comp27 = alloca i32, align 4
  %predef_tmp_comp28 = alloca i32, align 4
  %predef_tmp_comp29 = alloca i32, align 4
  store float* %features, float** %features.addr, align 8
  store i32 %nfeatures, i32* %nfeatures.addr, align 4
  store i32 %npoints, i32* %npoints.addr, align 4
  store i32 %nclusters, i32* %nclusters.addr, align 4
  store i32* %membership, i32** %membership.addr, align 8
  store float* %clusters, float** %clusters.addr, align 8
  store float* %block_clusters, float** %block_clusters.addr, align 8
  store i32* %block_deltas, i32** %block_deltas.addr, align 8
  %1 = call i32 @llvm.nvvm.read.ptx.sreg.nctaid.x()
  store i32 %1, i32* %predef_tmp_comp, align 4
  %2 = load i32* %predef_tmp_comp, align 4
  %3 = call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y()
  store i32 %3, i32* %predef_tmp_comp1, align 4
  %4 = load i32* %predef_tmp_comp1, align 4
  %5 = mul i32 %2, %4
  %6 = call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x()
  store i32 %6, i32* %predef_tmp_comp2, align 4
  %7 = load i32* %predef_tmp_comp2, align 4
  %8 = add i32 %5, %7
  store i32 %8, i32* %__cuda_local_var_36391_21_non_const_block_id, align 4
  %9 = load i32* %__cuda_local_var_36391_21_non_const_block_id, align 4
  %10 = call i32 @llvm.nvvm.read.ptx.sreg.ntid.x()
  store i32 %10, i32* %predef_tmp_comp3, align 4
  %11 = load i32* %predef_tmp_comp3, align 4
  %12 = mul i32 %9, %11
  %13 = call i32 @llvm.nvvm.read.ptx.sreg.ntid.y()
  store i32 %13, i32* %predef_tmp_comp4, align 4
  %14 = load i32* %predef_tmp_comp4, align 4
  %15 = mul i32 %12, %14
  %16 = call i32 @llvm.nvvm.read.ptx.sreg.tid.x()
  store i32 %16, i32* %predef_tmp_comp5, align 4
  %17 = load i32* %predef_tmp_comp5, align 4
  %18 = add i32 %15, %17
  store i32 %18, i32* %__cuda_local_var_36393_21_non_const_point_id, align 4
  store i32 -1, i32* %__cuda_local_var_36395_6_non_const_index, align 4
  %19 = load i32* %__cuda_local_var_36393_21_non_const_point_id, align 4
  %20 = load i32* %npoints.addr, align 4
  %21 = icmp ult i32 %19, %20
  br i1 %21, label %22, label %82

; <label>:22                                      ; preds = %0
  store float 0x47EFFFFFE0000000, float* %__cuda_local_var_36400_9_non_const_min_dist, align 4
  store i32 0, i32* %__cuda_local_var_36399_7_non_const_i, align 4
  br label %23

; <label>:23                                      ; preds = %78, %22
  %24 = load i32* %__cuda_local_var_36399_7_non_const_i, align 4
  %25 = load i32* %nclusters.addr, align 4
  %26 = icmp slt i32 %24, %25
  br i1 %26, label %27, label %81

; <label>:27                                      ; preds = %23
  %28 = load i32* %__cuda_local_var_36399_7_non_const_i, align 4
  %29 = load i32* %nfeatures.addr, align 4
  %30 = mul nsw i32 %28, %29
  store i32 %30, i32* %__cuda_local_var_36405_8_non_const_cluster_base_index, align 4
  store float 0.000000e+00, float* %__cuda_local_var_36406_10_non_const_ans, align 4
  store i32 0, i32* %__cuda_local_var_36399_10_non_const_j, align 4
  br label %31

; <label>:31                                      ; preds = %66, %27
  %32 = load i32* %__cuda_local_var_36399_10_non_const_j, align 4
  %33 = load i32* %nfeatures.addr, align 4
  %34 = icmp slt i32 %32, %33
  br i1 %34, label %35, label %69

; <label>:35                                      ; preds = %31
  %36 = load i32* %__cuda_local_var_36393_21_non_const_point_id, align 4
  %37 = load i32* %__cuda_local_var_36399_10_non_const_j, align 4
  %38 = load i32* %npoints.addr, align 4
  %39 = mul nsw i32 %37, %38
  %40 = add i32 %36, %39
  store i32 %40, i32* %__cuda_local_var_36410_9_non_const_addr, align 4
  %41 = call i64 @llvm.nvvm.texsurf.handle.p1i64(metadata !6, i64 addrspace(1)* @t_features)
  store i64 %41, i64* %__T210, align 8
  %42 = load i64* %__T210, align 8
  %43 = load i32* %__cuda_local_var_36410_9_non_const_addr, align 4
  %44 = getelementptr inbounds %struct.int4* %__T211, i32 0, i32 0
  store i32 %43, i32* %44, align 16
  %45 = getelementptr inbounds %struct.int4* %__T211, i32 0, i32 1
  store i32 0, i32* %45, align 4
  %46 = getelementptr inbounds %struct.int4* %__T211, i32 0, i32 2
  store i32 0, i32* %46, align 8
  %47 = getelementptr inbounds %struct.int4* %__T211, i32 0, i32 3
  store i32 0, i32* %47, align 4
  %48 = load %struct.int4* %__T211, align 16
  store %struct.int4 %48, %struct.int4* %agg.tmp, align 16
  %49 = load %struct.int4* %agg.tmp, align 16
  %call = call %struct.float4 @__ftexfetchi1D(i64 %42, %struct.int4 %49)
  store %struct.float4 %call, %struct.float4* %__T212, align 16
  %50 = getelementptr inbounds %struct.float4* %__T212, i32 0, i32 0
  %51 = load float* %50, align 16
  %52 = call i8* @llvm.nvvm.ptr.constant.to.gen.p0i8.p4i8(i8 addrspace(4)* bitcast ([1088 x float] addrspace(4)* @c_clusters to i8 addrspace(4)*))
  %53 = bitcast i8* %52 to [1088 x float]*
  %54 = getelementptr inbounds [1088 x float]* %53, i32 0, i32 0
  %55 = load i32* %__cuda_local_var_36405_8_non_const_cluster_base_index, align 4
  %56 = load i32* %__cuda_local_var_36399_10_non_const_j, align 4
  %57 = add nsw i32 %55, %56
  %58 = getelementptr inbounds float* %54, i32 %57
  %59 = load float* %58, align 4
  %60 = fsub float %51, %59
  store float %60, float* %__cuda_local_var_36411_11_non_const_diff, align 4
  %61 = load float* %__cuda_local_var_36411_11_non_const_diff, align 4
  %62 = load float* %__cuda_local_var_36411_11_non_const_diff, align 4
  %63 = fmul float %61, %62
  %64 = load float* %__cuda_local_var_36406_10_non_const_ans, align 4
  %65 = fadd float %64, %63
  store float %65, float* %__cuda_local_var_36406_10_non_const_ans, align 4
  br label %66

; <label>:66                                      ; preds = %35
  %67 = load i32* %__cuda_local_var_36399_10_non_const_j, align 4
  %68 = add nsw i32 %67, 1
  store i32 %68, i32* %__cuda_local_var_36399_10_non_const_j, align 4
  br label %31

; <label>:69                                      ; preds = %31
  %70 = load float* %__cuda_local_var_36406_10_non_const_ans, align 4
  store float %70, float* %__cuda_local_var_36401_9_non_const_dist, align 4
  %71 = load float* %__cuda_local_var_36401_9_non_const_dist, align 4
  %72 = load float* %__cuda_local_var_36400_9_non_const_min_dist, align 4
  %73 = fcmp olt float %71, %72
  br i1 %73, label %74, label %77

; <label>:74                                      ; preds = %69
  %75 = load float* %__cuda_local_var_36401_9_non_const_dist, align 4
  store float %75, float* %__cuda_local_var_36400_9_non_const_min_dist, align 4
  %76 = load i32* %__cuda_local_var_36399_7_non_const_i, align 4
  store i32 %76, i32* %__cuda_local_var_36395_6_non_const_index, align 4
  br label %77

; <label>:77                                      ; preds = %74, %69
  br label %78

; <label>:78                                      ; preds = %77
  %79 = load i32* %__cuda_local_var_36399_7_non_const_i, align 4
  %80 = add nsw i32 %79, 1
  store i32 %80, i32* %__cuda_local_var_36399_7_non_const_i, align 4
  br label %23

; <label>:81                                      ; preds = %23
  br label %82

; <label>:82                                      ; preds = %81, %0
  %83 = call i32 @llvm.nvvm.read.ptx.sreg.tid.x()
  store i32 %83, i32* %predef_tmp_comp6, align 4
  %84 = load i32* %predef_tmp_comp6, align 4
  %85 = icmp ult i32 %84, 256
  br i1 %85, label %86, label %94

; <label>:86                                      ; preds = %82
  %87 = call i8* @llvm.nvvm.ptr.shared.to.gen.p0i8.p3i8(i8 addrspace(3)* bitcast ([256 x i32] addrspace(3)* @"_Z11kmeansPointPfiiiPiS_S_S0_$__cuda_local_var_36429_30_non_const_deltas" to i8 addrspace(3)*))
  %88 = bitcast i8* %87 to [256 x i32]*
  %89 = getelementptr inbounds [256 x i32]* %88, i32 0, i32 0
  %90 = call i32 @llvm.nvvm.read.ptx.sreg.tid.x()
  store i32 %90, i32* %predef_tmp_comp7, align 4
  %91 = load i32* %predef_tmp_comp7, align 4
  %92 = zext i32 %91 to i64
  %93 = getelementptr inbounds i32* %89, i64 %92
  store i32 0, i32* %93, align 4
  br label %94

; <label>:94                                      ; preds = %86, %82
  %95 = load i32* %__cuda_local_var_36393_21_non_const_point_id, align 4
  %96 = load i32* %npoints.addr, align 4
  %97 = icmp ult i32 %95, %96
  br i1 %97, label %98, label %120

; <label>:98                                      ; preds = %94
  %99 = load i32** %membership.addr, align 8
  %100 = load i32* %__cuda_local_var_36393_21_non_const_point_id, align 4
  %101 = zext i32 %100 to i64
  %102 = getelementptr inbounds i32* %99, i64 %101
  %103 = load i32* %102, align 4
  %104 = load i32* %__cuda_local_var_36395_6_non_const_index, align 4
  %105 = icmp ne i32 %103, %104
  br i1 %105, label %106, label %114

; <label>:106                                     ; preds = %98
  %107 = call i8* @llvm.nvvm.ptr.shared.to.gen.p0i8.p3i8(i8 addrspace(3)* bitcast ([256 x i32] addrspace(3)* @"_Z11kmeansPointPfiiiPiS_S_S0_$__cuda_local_var_36429_30_non_const_deltas" to i8 addrspace(3)*))
  %108 = bitcast i8* %107 to [256 x i32]*
  %109 = getelementptr inbounds [256 x i32]* %108, i32 0, i32 0
  %110 = call i32 @llvm.nvvm.read.ptx.sreg.tid.x()
  store i32 %110, i32* %predef_tmp_comp8, align 4
  %111 = load i32* %predef_tmp_comp8, align 4
  %112 = zext i32 %111 to i64
  %113 = getelementptr inbounds i32* %109, i64 %112
  store i32 1, i32* %113, align 4
  br label %114

; <label>:114                                     ; preds = %106, %98
  %115 = load i32* %__cuda_local_var_36395_6_non_const_index, align 4
  %116 = load i32** %membership.addr, align 8
  %117 = load i32* %__cuda_local_var_36393_21_non_const_point_id, align 4
  %118 = zext i32 %117 to i64
  %119 = getelementptr inbounds i32* %116, i64 %118
  store i32 %115, i32* %119, align 4
  br label %120

; <label>:120                                     ; preds = %114, %94
  call void @llvm.cuda.syncthreads()
  store i32 128, i32* %__cuda_local_var_36452_15_non_const_threadids_participating, align 4
  br label %121

; <label>:121                                     ; preds = %150, %120
  %122 = load i32* %__cuda_local_var_36452_15_non_const_threadids_participating, align 4
  %123 = icmp ugt i32 %122, 1
  br i1 %123, label %124, label %153

; <label>:124                                     ; preds = %121
  %125 = call i32 @llvm.nvvm.read.ptx.sreg.tid.x()
  store i32 %125, i32* %predef_tmp_comp9, align 4
  %126 = load i32* %predef_tmp_comp9, align 4
  %127 = load i32* %__cuda_local_var_36452_15_non_const_threadids_participating, align 4
  %128 = icmp ult i32 %126, %127
  br i1 %128, label %129, label %149

; <label>:129                                     ; preds = %124
  %130 = call i8* @llvm.nvvm.ptr.shared.to.gen.p0i8.p3i8(i8 addrspace(3)* bitcast ([256 x i32] addrspace(3)* @"_Z11kmeansPointPfiiiPiS_S_S0_$__cuda_local_var_36429_30_non_const_deltas" to i8 addrspace(3)*))
  %131 = bitcast i8* %130 to [256 x i32]*
  %132 = getelementptr inbounds [256 x i32]* %131, i32 0, i32 0
  %133 = call i32 @llvm.nvvm.read.ptx.sreg.tid.x()
  store i32 %133, i32* %predef_tmp_comp10, align 4
  %134 = load i32* %predef_tmp_comp10, align 4
  %135 = load i32* %__cuda_local_var_36452_15_non_const_threadids_participating, align 4
  %136 = add i32 %134, %135
  %137 = zext i32 %136 to i64
  %138 = getelementptr inbounds i32* %132, i64 %137
  %139 = load i32* %138, align 4
  %140 = call i8* @llvm.nvvm.ptr.shared.to.gen.p0i8.p3i8(i8 addrspace(3)* bitcast ([256 x i32] addrspace(3)* @"_Z11kmeansPointPfiiiPiS_S_S0_$__cuda_local_var_36429_30_non_const_deltas" to i8 addrspace(3)*))
  %141 = bitcast i8* %140 to [256 x i32]*
  %142 = getelementptr inbounds [256 x i32]* %141, i32 0, i32 0
  %143 = call i32 @llvm.nvvm.read.ptx.sreg.tid.x()
  store i32 %143, i32* %predef_tmp_comp11, align 4
  %144 = load i32* %predef_tmp_comp11, align 4
  %145 = zext i32 %144 to i64
  %146 = getelementptr inbounds i32* %142, i64 %145
  %147 = load i32* %146, align 4
  %148 = add nsw i32 %147, %139
  store i32 %148, i32* %146, align 4
  br label %149

; <label>:149                                     ; preds = %129, %124
  call void @llvm.cuda.syncthreads()
  br label %150

; <label>:150                                     ; preds = %149
  %151 = load i32* %__cuda_local_var_36452_15_non_const_threadids_participating, align 4
  %152 = udiv i32 %151, 2
  store i32 %152, i32* %__cuda_local_var_36452_15_non_const_threadids_participating, align 4
  br label %121

; <label>:153                                     ; preds = %121
  %154 = call i32 @llvm.nvvm.read.ptx.sreg.tid.x()
  store i32 %154, i32* %predef_tmp_comp12, align 4
  %155 = load i32* %predef_tmp_comp12, align 4
  %156 = icmp ult i32 %155, 1
  br i1 %156, label %157, label %176

; <label>:157                                     ; preds = %153
  %158 = call i8* @llvm.nvvm.ptr.shared.to.gen.p0i8.p3i8(i8 addrspace(3)* bitcast ([256 x i32] addrspace(3)* @"_Z11kmeansPointPfiiiPiS_S_S0_$__cuda_local_var_36429_30_non_const_deltas" to i8 addrspace(3)*))
  %159 = bitcast i8* %158 to [256 x i32]*
  %160 = getelementptr inbounds [256 x i32]* %159, i32 0, i32 0
  %161 = call i32 @llvm.nvvm.read.ptx.sreg.tid.x()
  store i32 %161, i32* %predef_tmp_comp13, align 4
  %162 = load i32* %predef_tmp_comp13, align 4
  %163 = add i32 %162, 1
  %164 = zext i32 %163 to i64
  %165 = getelementptr inbounds i32* %160, i64 %164
  %166 = load i32* %165, align 4
  %167 = call i8* @llvm.nvvm.ptr.shared.to.gen.p0i8.p3i8(i8 addrspace(3)* bitcast ([256 x i32] addrspace(3)* @"_Z11kmeansPointPfiiiPiS_S_S0_$__cuda_local_var_36429_30_non_const_deltas" to i8 addrspace(3)*))
  %168 = bitcast i8* %167 to [256 x i32]*
  %169 = getelementptr inbounds [256 x i32]* %168, i32 0, i32 0
  %170 = call i32 @llvm.nvvm.read.ptx.sreg.tid.x()
  store i32 %170, i32* %predef_tmp_comp14, align 4
  %171 = load i32* %predef_tmp_comp14, align 4
  %172 = zext i32 %171 to i64
  %173 = getelementptr inbounds i32* %169, i64 %172
  %174 = load i32* %173, align 4
  %175 = add nsw i32 %174, %166
  store i32 %175, i32* %173, align 4
  br label %176

; <label>:176                                     ; preds = %157, %153
  call void @llvm.cuda.syncthreads()
  %177 = call i32 @llvm.nvvm.read.ptx.sreg.tid.x()
  store i32 %177, i32* %predef_tmp_comp15, align 4
  %178 = load i32* %predef_tmp_comp15, align 4
  %179 = icmp eq i32 %178, 0
  br i1 %179, label %180, label %197

; <label>:180                                     ; preds = %176
  %181 = call i8* @llvm.nvvm.ptr.shared.to.gen.p0i8.p3i8(i8 addrspace(3)* bitcast ([256 x i32] addrspace(3)* @"_Z11kmeansPointPfiiiPiS_S_S0_$__cuda_local_var_36429_30_non_const_deltas" to i8 addrspace(3)*))
  %182 = bitcast i8* %181 to [256 x i32]*
  %183 = getelementptr inbounds [256 x i32]* %182, i32 0, i32 0
  %184 = getelementptr inbounds i32* %183, i32 0
  %185 = load i32* %184, align 4
  %186 = load i32** %block_deltas.addr, align 8
  %187 = call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y()
  store i32 %187, i32* %predef_tmp_comp16, align 4
  %188 = load i32* %predef_tmp_comp16, align 4
  %189 = call i32 @llvm.nvvm.read.ptx.sreg.nctaid.x()
  store i32 %189, i32* %predef_tmp_comp17, align 4
  %190 = load i32* %predef_tmp_comp17, align 4
  %191 = mul i32 %188, %190
  %192 = call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x()
  store i32 %192, i32* %predef_tmp_comp18, align 4
  %193 = load i32* %predef_tmp_comp18, align 4
  %194 = add i32 %191, %193
  %195 = zext i32 %194 to i64
  %196 = getelementptr inbounds i32* %186, i64 %195
  store i32 %185, i32* %196, align 4
  br label %197

; <label>:197                                     ; preds = %180, %176
  %198 = call i32 @llvm.nvvm.read.ptx.sreg.tid.x()
  store i32 %198, i32* %predef_tmp_comp19, align 4
  %199 = load i32* %predef_tmp_comp19, align 4
  %200 = load i32* %nfeatures.addr, align 4
  %201 = udiv i32 %199, %200
  store i32 %201, i32* %__cuda_local_var_36472_6_non_const_center_id, align 4
  %202 = call i32 @llvm.nvvm.read.ptx.sreg.tid.x()
  store i32 %202, i32* %predef_tmp_comp20, align 4
  %203 = load i32* %predef_tmp_comp20, align 4
  %204 = load i32* %nfeatures.addr, align 4
  %205 = load i32* %__cuda_local_var_36472_6_non_const_center_id, align 4
  %206 = mul nsw i32 %204, %205
  %207 = sub i32 %203, %206
  store i32 %207, i32* %__cuda_local_var_36473_6_non_const_dim_id, align 4
  %208 = load i32* %__cuda_local_var_36395_6_non_const_index, align 4
  %209 = call i8* @llvm.nvvm.ptr.shared.to.gen.p0i8.p3i8(i8 addrspace(3)* bitcast ([256 x i32] addrspace(3)* @"_Z11kmeansPointPfiiiPiS_S_S0_$__cuda_local_var_36475_30_non_const_new_center_ids" to i8 addrspace(3)*))
  %210 = bitcast i8* %209 to [256 x i32]*
  %211 = getelementptr inbounds [256 x i32]* %210, i32 0, i32 0
  %212 = call i32 @llvm.nvvm.read.ptx.sreg.tid.x()
  store i32 %212, i32* %predef_tmp_comp21, align 4
  %213 = load i32* %predef_tmp_comp21, align 4
  %214 = zext i32 %213 to i64
  %215 = getelementptr inbounds i32* %211, i64 %214
  store i32 %208, i32* %215, align 4
  call void @llvm.cuda.syncthreads()
  %216 = load i32* %__cuda_local_var_36393_21_non_const_point_id, align 4
  %217 = call i32 @llvm.nvvm.read.ptx.sreg.tid.x()
  store i32 %217, i32* %predef_tmp_comp22, align 4
  %218 = load i32* %predef_tmp_comp22, align 4
  %219 = sub i32 %216, %218
  %220 = load i32* %nfeatures.addr, align 4
  %221 = mul i32 %219, %220
  %222 = load i32* %__cuda_local_var_36473_6_non_const_dim_id, align 4
  %223 = add i32 %221, %222
  store i32 %223, i32* %__cuda_local_var_36486_6_non_const_new_base_index, align 4
  store float 0.000000e+00, float* %__cuda_local_var_36487_8_non_const_accumulator, align 4
  %224 = call i32 @llvm.nvvm.read.ptx.sreg.tid.x()
  store i32 %224, i32* %predef_tmp_comp23, align 4
  %225 = load i32* %predef_tmp_comp23, align 4
  %226 = load i32* %nfeatures.addr, align 4
  %227 = load i32* %nclusters.addr, align 4
  %228 = mul nsw i32 %226, %227
  %229 = icmp ult i32 %225, %228
  br i1 %229, label %230, label %287

; <label>:230                                     ; preds = %197
  store i32 0, i32* %i, align 4
  br label %231

; <label>:231                                     ; preds = %264, %230
  %232 = load i32* %i, align 4
  %233 = icmp slt i32 %232, 256
  br i1 %233, label %234, label %267

; <label>:234                                     ; preds = %231
  %235 = call i64 @llvm.nvvm.texsurf.handle.p1i64(metadata !7, i64 addrspace(1)* @t_features_flipped)
  store i64 %235, i64* %__T213, align 8
  %236 = load i32* %__cuda_local_var_36486_6_non_const_new_base_index, align 4
  %237 = load i32* %i, align 4
  %238 = load i32* %nfeatures.addr, align 4
  %239 = mul nsw i32 %237, %238
  %240 = add nsw i32 %236, %239
  store i32 %240, i32* %__T214, align 4
  %241 = load i64* %__T213, align 8
  %242 = load i32* %__T214, align 4
  %243 = getelementptr inbounds %struct.int4* %__T215, i32 0, i32 0
  store i32 %242, i32* %243, align 16
  %244 = getelementptr inbounds %struct.int4* %__T215, i32 0, i32 1
  store i32 0, i32* %244, align 4
  %245 = getelementptr inbounds %struct.int4* %__T215, i32 0, i32 2
  store i32 0, i32* %245, align 8
  %246 = getelementptr inbounds %struct.int4* %__T215, i32 0, i32 3
  store i32 0, i32* %246, align 4
  %247 = load %struct.int4* %__T215, align 16
  store %struct.int4 %247, %struct.int4* %agg.tmp24, align 16
  %248 = load %struct.int4* %agg.tmp24, align 16
  %call25 = call %struct.float4 @__ftexfetchi1D(i64 %241, %struct.int4 %248)
  store %struct.float4 %call25, %struct.float4* %__T216, align 16
  %249 = getelementptr inbounds %struct.float4* %__T216, i32 0, i32 0
  %250 = load float* %249, align 16
  store float %250, float* %__cuda_local_var_36492_10_non_const_val, align 4
  %251 = call i8* @llvm.nvvm.ptr.shared.to.gen.p0i8.p3i8(i8 addrspace(3)* bitcast ([256 x i32] addrspace(3)* @"_Z11kmeansPointPfiiiPiS_S_S0_$__cuda_local_var_36475_30_non_const_new_center_ids" to i8 addrspace(3)*))
  %252 = bitcast i8* %251 to [256 x i32]*
  %253 = getelementptr inbounds [256 x i32]* %252, i32 0, i32 0
  %254 = load i32* %i, align 4
  %255 = getelementptr inbounds i32* %253, i32 %254
  %256 = load i32* %255, align 4
  %257 = load i32* %__cuda_local_var_36472_6_non_const_center_id, align 4
  %258 = icmp eq i32 %256, %257
  br i1 %258, label %259, label %263

; <label>:259                                     ; preds = %234
  %260 = load float* %__cuda_local_var_36492_10_non_const_val, align 4
  %261 = load float* %__cuda_local_var_36487_8_non_const_accumulator, align 4
  %262 = fadd float %261, %260
  store float %262, float* %__cuda_local_var_36487_8_non_const_accumulator, align 4
  br label %263

; <label>:263                                     ; preds = %259, %234
  br label %264

; <label>:264                                     ; preds = %263
  %265 = load i32* %i, align 4
  %266 = add nsw i32 %265, 1
  store i32 %266, i32* %i, align 4
  br label %231

; <label>:267                                     ; preds = %231
  %268 = load float* %__cuda_local_var_36487_8_non_const_accumulator, align 4
  %269 = load float** %block_clusters.addr, align 8
  %270 = call i32 @llvm.nvvm.read.ptx.sreg.ctaid.y()
  store i32 %270, i32* %predef_tmp_comp26, align 4
  %271 = load i32* %predef_tmp_comp26, align 4
  %272 = call i32 @llvm.nvvm.read.ptx.sreg.nctaid.x()
  store i32 %272, i32* %predef_tmp_comp27, align 4
  %273 = load i32* %predef_tmp_comp27, align 4
  %274 = mul i32 %271, %273
  %275 = call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x()
  store i32 %275, i32* %predef_tmp_comp28, align 4
  %276 = load i32* %predef_tmp_comp28, align 4
  %277 = add i32 %274, %276
  %278 = load i32* %nclusters.addr, align 4
  %279 = mul i32 %277, %278
  %280 = load i32* %nfeatures.addr, align 4
  %281 = mul i32 %279, %280
  %282 = call i32 @llvm.nvvm.read.ptx.sreg.tid.x()
  store i32 %282, i32* %predef_tmp_comp29, align 4
  %283 = load i32* %predef_tmp_comp29, align 4
  %284 = add i32 %281, %283
  %285 = zext i32 %284 to i64
  %286 = getelementptr inbounds float* %269, i64 %285
  store float %268, float* %286, align 4
  br label %287

; <label>:287                                     ; preds = %267, %197
  ret void
}

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.nctaid.x() #0

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.ctaid.y() #0

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.ntid.y() #0

; Function Attrs: nounwind readnone
declare i64 @llvm.nvvm.texsurf.handle.p1i64(metadata, i64 addrspace(1)*) #0

declare %struct.float4 @__ftexfetchi1D(i64, %struct.int4)

; Function Attrs: nounwind readnone
declare i8* @llvm.nvvm.ptr.constant.to.gen.p0i8.p4i8(i8 addrspace(4)*) #0

; Function Attrs: nounwind readnone
declare i8* @llvm.nvvm.ptr.shared.to.gen.p0i8.p3i8(i8 addrspace(3)*) #0

; Function Attrs: nounwind
declare void @llvm.cuda.syncthreads() #1

attributes #0 = { nounwind readnone }
attributes #1 = { nounwind }

!nvvm.annotations = !{!0, !1, !2, !3, !4, !5}

!0 = metadata !{i64 addrspace(1)* @t_features, metadata !"texture", i32 1}
!1 = metadata !{i64 addrspace(1)* @t_features_flipped, metadata !"texture", i32 1}
!2 = metadata !{i64 addrspace(1)* @t_clusters, metadata !"texture", i32 1}
!3 = metadata !{void (float*, float*, i32, i32)* @_Z14invert_mappingPfS_ii, metadata !"kernel", i32 1}
!4 = metadata !{void (float*, i32, i32, i32, i32*, float*, float*, i32*)* @_Z11kmeansPointPfiiiPiS_S_S0_, metadata !"kernel", i32 1}
!5 = metadata !{%struct.float4 (i64, %struct.int4)* @__ftexfetchi1D, metadata !"align", i32 16, metadata !"align", i32 131088}
!6 = metadata !{i64 addrspace(1)* @t_features}
!7 = metadata !{i64 addrspace(1)* @t_features_flipped}
