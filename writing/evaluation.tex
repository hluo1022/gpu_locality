\section{Evaluation}
\label{sec:eval}

We evaluate our three techniques in two ways: the accuracy in
performance prediction and the improvement in GPU code optimization.

\subsection{Experimental Setup}
\label{sec:eval_setup}

\begin{comment}
\paragraph{Implementation}
\label{sec:sum}
We have implemented a tool to analyze a program in three steps.

\begin{itemize}

\item \emph{Step 1: trace collection.} We have implemented
  a compiler pass to insert a function before each memory 
  reference in a program to record the target address, data 
  width and array ID of each data access at run time.

\item \emph{Step 2: trace processing.}  The analysis scans the memory
  trace and performs access coalescing according to the specifications
  of the GPU architecture.  It measures the mapped parallel footprint
  of each array for the L1 cache and the dual granularity mapped
  parallel footprint for the texture cache (see
  Section~\ref{sec:combo}).  A cache block is 128 bytes in both
  caches.  A sector is 32 bytes in the texture cache.

\item \emph{Step 3: data centric analysis.}  For the placement of each
  array group in each cache configuration (type, set count and
  associativity), predict the cache miss ratio.
\end{itemize}

Coalescing happens for each array in the second step and does not
affect the ability to compose in the third step.
\end{comment}

\paragraph{Benchmarks}
We evaluate using 13 kernels from SHOC~\cite{SHOC:GPGPU10} 
and CUDA Code Samples~\cite{CUDASDK} listed in Table~\ref{tbl:bench}. 

\begin{table}[ht!]
  \centering
  \caption{Benchmarks for evaluation}
  \vspace{1mm}
  \resizebox{0.48\textwidth}{!}{
  \begin{tabular}{|c|c|c|c|}
    \hline
    \textbf{Benchmarks} & \textbf{Description} & \textbf{Benchmarks} & \textbf{Description} \\
    \hline
    \hline
    \textbf{BFS} & graph algorithm & \textbf{Trans} & matrix transpose \\
    \hline
    \textbf{FFT} & numerical algorithm & \textbf{MM} & matrix multiply \\
    \hline
    \textbf{Scan} & parallel prefix sum & \textbf{MD} & N-Body simulation \\
    \hline
    \textbf{Reduction} & reduce routine & \textbf{SpMV} & matrix-vector multiply \\
    \hline
    \textbf{Sort} & sorting & \textbf{CFD} & unstructured grid \\
    \hline
    \textbf{Triad} & vector addition & \textbf{ParticleFilter} & structured grid \\
    \hline
    \textbf{Stencil2D} & structured grid &  & \\
    \hline
  \end{tabular}
  }
  \label{tbl:bench}
\end{table}

\paragraph{Methodology}
We compare with three existing solutions
listed in Table~\ref{tbl:models}.

\begin{table}[ht!]
%\tabcolsep=0.11cm
  \centering
  \caption{Three alternative cache models}
  \vspace{1mm}
  \resizebox{0.48\textwidth}{!}{
  \begin{tabular}{|c|l|}
    \hline
    \textbf{Models}                           & \multicolumn{1}{c|}{\textbf{Features}}\\
    \hline
    \hline
    \multirow{2}{*}{\textbf{Baseline}}      & simulated set associative LRU cache and texture cache\\
                                              & (Section~\ref{sec:tex_management}).\\
    \hline
    \multirow{2}{*}{\textbf{Set-RD}~\cite{Nugteren+:HPCA14}} & a fixed number of cache sets and one reuse distance\\
                                                               & histogram per cache set.\\
    \hline
    \multirow{2}{*}{\textbf{PORPLE}~\cite{Chen+:Micro14}}      &
    \textbf{1}. reuse distance of whole trace, no set associativity.\\
                                                               & \textbf{2}. assume arrays partition cache space equally.\\
    \hline
  \end{tabular}
  }
  \label{tbl:models}
\end{table}

In all the methods, 
the memory trace is collected 
from a Tesla M2075 general-purpose graphics processor, 
a Fermi device with compute capacity 2.0. 
To reduce the overhead of instrumentation, 
only one streaming multiprocessor's memory references
are instrumented and recorded. 
%The trace obtained this way
%may differ from the actual execution. 

All the methods model the memory system on Fermi.
On Fermi, the cache block is 128 bytes for L1 and the texture cache.
L1 can be configured in two sizes: 16KB or 48KB.  It has 32 cache sets
and 4 ways per set in 16KB and 64
sets and 6 ways per set in 48KB.
The set index hashing function follows Nugteren et al.\cite{Nugteren+:HPCA14}
The texture cache is 12KB, divided in 12 sets and 8 ways per set.  
These settings are selected to match the 
real system parameters on Tesla M2075. 

The baseline is cache 
simulation instead of measurement from hardware counters.
Cache simulation serves three purposes.  The first is to separate the
error of cache modeling from trace collection.  The comparison with
simulation shows just the error of cache modeling.  The second is
accuracy.  A hardware counter may over count, for example, by counting
the same miss multiple times~\cite{Nugteren+:HPCA14}.  In this case,
the simulator reflects the actual run-time cost better than the
hardware counter does.  
The third is to avoid bias in the results.  A simulator shows the complete and
precise assumptions we make about the hardware.  The comparison shows
how well a technique models the cache design it is intended to model.
The actual cache design is proprietary and may have hidden features.
The prediction accuracy or error may be by accident rather than by
merit.  In addition, the observation based on the simulation does not
become invalid if the actual cache design changes in future
generations of the hardware.

\subsection{Overall Accuracy}
\label{sec:overall}

We enumerate
the miss ratio results of different array placements
in detail in Table~\ref{tbl:mr_results}.
An array may be placed in L1 or not
cached.  If it is read only, it has a third choice, the texture cache.
10 out of 13 programs have 2 to 7 arrays, which means in maximum $9$ to
$3^7 = 2187$ possible placements.  The table counts the number of
cache measurements, which is at most $9$ to $2187$.  Each row is
a measurement and may represent multiple placement choices.  For
example, the second row of \emph{Sort} has Array I in L1 and Array II
in the texture cache.  It also gives the cache performance for two
more placements: when Array I is L1 and Array II not cached,
and when Array I is not cached and Array II in texture cache.
 
For each placement, the table shows two measured miss ratios: Baseline
and Set-RD, and two predictions: Footprint and PORPLE.  The
measurement runs the memory trace once for each placement.  The prediction
profiles the memory trace just once.  Set-RD is accurate.  It gives the
same result as simulation, so the table shows the two in a single column.  

\begin{table*}[ht!]
\centering
\caption{Cache miss ratios by different array placements.  Each array
  has at most three placing options: L1, texture cache or not
  cached. The column \emph{array placement in cache} shows the
  placement for at most four arrays, where ``L1'' means L1 data cache,
  configured 48KB, ``Tex'' means the texture cache, and ``-'' means either no such array or that the array is not cached in either L1 or Tex.  For some benchmarks, not all placements are shown for brevity.  For each placement, the table compares four methods: two measured miss ratios, Baseline and Set-RD; and two predictions, Footprint and PORPLE.   The measurement runs a program once for each placement.  The prediction runs a program just once.} 
\vspace{1mm}
%\tabcolsep=0.11cm
\resizebox{\textwidth}{!}{
\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|}
\hline
\multirow{3}{*}{}                       & \textbf{Number of}  & \multicolumn{4}{c|}{\textbf{Array placement in caches}}      & \multicolumn{4}{c|}{\textbf{L1 cache miss ratio (\%)}} & \multicolumn{3}{c|}{\textbf{Texture cache miss ratio (\%)}} \\ 
\cline{3-13}
\multicolumn{1}{|c|}{}                  & \textbf{placements} & \multirow{2}{*}{I} & \multirow{2}{*}{II}  & \multirow{2}{*}{III} & \multirow{2}{*}{IV}  & \multicolumn{2}{c|}{\textbf{Measurement}}     & \multicolumn{2}{c|}{\textbf{Model}}   & \textbf{Measurement}  &  \multicolumn{2}{c|}{\textbf{Model}} \\
\cline{7-13}
\multicolumn{1}{|c|}{}                  &                     &     &     &     &     & \textbf{Baseline}     & \textbf{Set-RD} & \textbf{Footprint} & \textbf{PORPLE}   & \textbf{Baseline}  & \textbf{Footprint}  & \textbf{PORPLE}      \\ \hline\hline
\multirow{7}{*}{\textbf{BFS}}           & \multirow{7}{*}{\textbf{8}}  & L1  & L1  & L1  & -   & \multicolumn{2}{c|}{1.67}         & 1.67          & 1.67  & -               & -          & -        \\
                                        &                     & L1  & Tex & L1  & -   & \multicolumn{2}{c|}{2.80}  & 2.80          & 2.80  & 1.16          & 1.23     & 0.53   \\
                                        &                     & L1  & Tex & Tex & -   & \multicolumn{2}{c|}{3.20}  & 3.30          & 3.20  & 1.70          & 1.75     & 1.02   \\
                                        &                     & -   & L1  & L1  & -   & \multicolumn{2}{c|}{1.43}  & 1.43          & 1.43  & -               & -          & -        \\
                                        & (3 arrays)                    & L1  & L1  & Tex & -   & \multicolumn{2}{c|}{1.98}  & 1.98          & 1.98  & 3.36          & 3.58     & 13.02   \\
                                        &                     & -   & Tex & L1  & -   & \multicolumn{2}{c|}{3.36}   & 3.36          & 3.36  & 1.16          & 1.23     & 0.53   \\
                                        &                     & -   & L1  & Tex & -   & \multicolumn{2}{c|}{1.16}   & 1.16          & 1.10  & 3.36          & 3.58     & 13.02   \\
\hline
\multirow{2}{*}{\textbf{Scan}}          & \multirow{2}{*}{\textbf{2}}  & L1  & -   & -   & -   & \multicolumn{2}{c|}{100.00}     & 100.00           & 100.00  & -               & -          & -        \\
                                        &                     & Tex & -   & -   & -   & \multicolumn{2}{c|}{-}   & -               & -       & 100.00          & 100.00     & 100.00   \\
\hline
\multirow{2}{*}{\textbf{Reduction}}     & \multirow{2}{*}{\textbf{2}}  & L1  & -   & -   & -   & \multicolumn{2}{c|}{99.70}    & 100.00          & 100.00  & -               & -          & -        \\
                                        &                     & Tex & -   & -   & -   & \multicolumn{2}{c|}{-}  & -               & -       & 49.95          & 49.95     & 50.00   \\
\hline
\multirow{4}{*}{\textbf{Sort}}          & \multirow{4}{*}{}   & L1  & L1  & -   & -   & \multicolumn{2}{c|}{100.00}    & 100.00          & 100.00  & -               & -           & -       \\
                                        & \textbf{4}                   & Tex & L1  & -   & -   & \multicolumn{2}{c|}{100.00}    & 100.00          & 100.00  & 100.00          & 100.00      & 100.00  \\
                                        & (2 arrays)          & Tex & Tex & -   & -   & \multicolumn{2}{c|}{-}        & -               & -       & 99.90          & 99.90      & 100.00  \\
                                        &                     & L1  & Tex & -   & -   & \multicolumn{2}{c|}{100.00}   & 100.00          & 100.00  & 99.80          & 99.80      & 100.00  \\
\hline
\multirow{2}{*}{\textbf{Stencil2D}}     & \multirow{2}{*}{\textbf{2}}  & L1  & -   & -   & -   & \multicolumn{2}{c|}{38.09}   & 38.09          & 38.62  & -               & -           & -       \\
                                        &                     & Tex & -   & -   & -   & \multicolumn{2}{c|}{-}    & -               & -       & 46.79          & 46.79      & 33.56  \\
\hline
\multirow{2}{*}{\textbf{Trans}}         & \multirow{2}{*}{\textbf{2}}  & L1  & -   & -   & -   & \multicolumn{2}{c|}{75.00}    & 75.00          & 75.00  & -               & -           & -       \\
                                        &                     & Tex & -   & -   & -   & \multicolumn{2}{c|}{-}    & -               & -       & 49.95          & 49.95      & 50.00  \\
\hline
\multirow{3}{*}{\textbf{ParticleFilter}}& \multirow{3}{*}{\textbf{3}}  & L1  & L1  & -   & -   & \multicolumn{2}{c|}{0.38}   & 0.31          & 0.58  & -               & -           & -       \\
                                        &                     & L1  & -   & -   & -   & \multicolumn{2}{c|}{0.88}    & 0.83          & 1.59  & -               & -           & -       \\
                                        &                     & -   & L1  & -   & -   & \multicolumn{2}{c|}{0.02}    & 0.02          & 0.02  & -               & -           & -       \\
\hline
\multirow{11}{*}{\textbf{SpMV}}         & \multirow{11}{*}{\textbf{16}} & L1  & Tex & Tex & Tex & \multicolumn{2}{c|}{6.66}    & 6.60          & 6.66  & 45.32          & 49.10      & 49.93  \\
                                        &                     & Tex & L1  & Tex & Tex & \multicolumn{2}{c|}{50.18}    & 50.18          & 50.18  & 40.38          & 45.34      & 53.80  \\
                                        &                     & Tex & Tex & L1  & Tex & \multicolumn{2}{c|}{6.31}    & 6.31          & 6.31  & 35.67          & 35.76      & 35.60  \\
                                        &                     & Tex & Tex & Tex & L1  & \multicolumn{2}{c|}{51.20}    & 51.20          & 50.18  & 40.52          & 45.34      & 53.82  \\
                                        &                     & Tex & L1  & L1  & L1  & \multicolumn{2}{c|}{22.13}    & 20.81          & 24.62  & 1.56          & 1.95      & 1.66  \\
                                        &                     & L1  & Tex & L1  & L1  & \multicolumn{2}{c|}{14.80}    & 14.08          & 19.80  & 35.74          & 35.76      & 36.08  \\
                                        & (4 arrays)          & L1  & L1  & Tex & L1  & \multicolumn{2}{c|}{49.71}    & 49.71          & 49.71  & 29.45          & 36.33      & 38.74  \\
                                        &                     & L1  & L1  & L1  & Tex & \multicolumn{2}{c|}{14.66}    & 14.06          & 19.77  & 35.76          & 35.77      & 36.05  \\
                                        &                     & L1  & L1  & L1  & L1  & \multicolumn{2}{c|}{22.09}    & 20.77          & 43.22  & -               & -           & -       \\
                                        &                     & Tex & Tex & L1  & L1  & \multicolumn{2}{c|}{14.83}    & 14.11          & 19.84  & 35.16          & 34.79      & 35.15  \\
                                        &                     & Tex & Tex & Tex & Tex & \multicolumn{2}{c|}{-}         & -               & -       & 45.05          & 48.77      & 59.66  \\
\hline
\multirow{2}{*}{\textbf{FFT}}           & \multirow{2}{*}{\textbf{2}}  & L1  & -   & -   & -   & \multicolumn{2}{c|}{57.18}    & 56.25          & 57.81  & -               & -          & -        \\
                                        &                     & Tex & -   & -   & -   & \multicolumn{2}{c|}{-}         & -               & -       & 72.98          & 73.68     & 73.43   \\
\hline
\multirow{4}{*}{\textbf{Triad}}         & \multirow{4}{*}{\textbf{4}}   & L1  & L1  & -   & -   & \multicolumn{2}{c|}{98.46}   & 100.00          & 100.00  & -               & -           & -       \\
                                        &                     & Tex & L1  & -   & -   & \multicolumn{2}{c|}{96.96}    & 100.00          & 100.00  & 50.00          & 50.00      & 50.00  \\
                                        &                     & Tex & Tex & -   & -   & \multicolumn{2}{c|}{-}   & -               & -       & 49.90          & 49.90      & 50.00  \\
                                        &                     & L1  & Tex & -   & -   & \multicolumn{2}{c|}{100.00}   & 100.00          & 100.00  & 49.80          & 49.80      & 50.00  \\
\hline
\multirow{4}{*}{\textbf{MD}}            & \multirow{4}{*}{\textbf{4}}  & L1  & L1  & -   & -   & \multicolumn{2}{c|}{78.62}    & 76.47          & 91.30  & -               & -           & -       \\
                                        &                     & Tex & L1  & -   & -   & \multicolumn{2}{c|}{100.00}    & 100.00          & 100.00  & 96.81          & 96.89      & 94.38  \\
                                        &                     & L1  & Tex & -   & -   & \multicolumn{2}{c|}{77.14}   & 75.00          & 82.83  & 50.00          & 50.00      & 50.00  \\
                                        &                     & Tex & Tex & -   & -   & \multicolumn{2}{c|}{-}     & -               & -       & 87.47          & 87.52      & 87.17  \\
\hline
\multirow{4}{*}{\textbf{CFD}}           & \multirow{4}{*}{}   & L1  & L1  & L1  & L1  & \multicolumn{2}{c|}{37.69}    & 31.40          & 57.50  & -               & -           & -       \\
                                        & \textbf{128}       & Tex & L1  & L1  & L1  & \multicolumn{2}{c|}{26.74}    & 20.54          & 7.03  & 59.90          & 59.23      & 53.15  \\
                                        & (7 arrays)          & Tex & Tex & L1  & L1  & \multicolumn{2}{c|}{16.37}    & 15.65          & 7.03  & 80.09          & 78.67      & 75.86  \\
                                        &                     & Tex & L1  & Tex & Tex & \multicolumn{2}{c|}{7.03}    & 7.03          & 7.03  & 80.19          & 79.32      & 75.34  \\
\hline
\multirow{4}{*}{\textbf{MM}}            & \multirow{4}{*}{\textbf{4}}  & L1  & L1  & -   & -   & \multicolumn{2}{c|}{57.82}    & 54.78          & 74.89  & -               & -           & -       \\
                                        &                     & L1  & Tex & -   & -   & \multicolumn{2}{c|}{50.11}   & 50.11          & 50.10  & 29.64          & 29.64      & 50.00  \\
                                        &                     & Tex & L1  & -   & -   & \multicolumn{2}{c|}{41.78}   & 48.68          & 3.42  & 30.97          & 30.97      & 50.00  \\
                                        &                     & Tex & Tex & -   & -   & \multicolumn{2}{c|}{-}       & -               & -       & 30.29          & 30.29      & 50.00  \\
\hline
\end{tabular}
}
\label{tbl:mr_results}
\end{table*}

Table~\ref{tbl:mr_results} shows that data centric analysis is highly
accurate.  Most predictions are either identical to the measurement or
differ by a negligible amount.  The largest error is 49\% prediction
vs 42\% measurement in one of the four placements of \emph{MM}, which
we will discuss later.

In comparison, PORPLE does not model data nonuniformity.  When a set of array share
the cache, it assumes equal cache occupancy.  
For L1 cache, PORPLE is mostly accurate but has large errors occasionally,
e.g.  by 15\% in the first case of \emph{MD}, 5\% 
to 23\% in four cases in \emph{SpMV}, a factor of 2 in two
cases in \emph{ParticleFilter} and one case in \emph{CFD}.  
Array III of \emph{SpMV} is accessed irregularly
and causes contention in L1 that disrupts the dynamic balance.
PORPLE prediction is almost always wrong.
The relative error is as high as 83\%. These results show
that there is significant data nonuniformity.

The average absolute miss-ratio error for L1 is shown in
Figure~\ref{fig:mr_results}.  For half of the programs (7 out of 13),
data centric analysis has no error.  The highest error is 3.3\% in
\emph{MM}, most due to the 7\% error in one placement
mentioned earlier.  PORPLE has no error in 5 programs, 0.3\% to 3.9\%
in 5 other programs, and 6\%, 12\% and 18\% for 3 remaining programs.
We will evaluate the effect of these errors on program optimization
later.  The arithmetic average across the 13 programs is 3.4\% error
for PORPLE 
and 0.8\%
for data centric analysis, 4.3 times smaller error than PORPLE.

\begin{comment}
	Footprint	PORPLE
BFS	                0	0
Scan	        0	0
Reduction	0	0
Sort	                0	0
Stencil2D	        0	0.6
Trans	        0	0
ParticleFilter	0	0.3
 SpMV	        0.47	3.9
FFT	               0.93	0.63
Triad	        1.526	1.526
MD	                1.5   6
CFD         	3.2	12.2
MM	                3.3	18.45
avg                  0.8    3.4
\end{comment}

For each array placement, the prediction is computed entirely from the
parallel, mapped and dual granularity footprints, yet it matches the
measurement obtained by simulating or profiling the entire program
execution.

\begin{comment}
% this part is important but not properly motivated or fully explained.  let's leave it out for now.
 With parallel footprint we have a way to evaluate 
the HOTL method. HOTL decomposes each array's memory 
accesses into subtraces with possibly unequal lengths
and evenly align them at the moment of composition.
In contrast, parallel footprint retains the interleaving 
information of array's memory accesses. We compared
these two approaches on predicting a fully associative
16KB L1 cache and observe the improvement of parallel
footprint. We found that the HOTL approach has high accuracy (less than
1\%) for all benchmarks except \emph{MM} and \emph{CFD}. 
For these two benchmarks, the error in predicted miss ratio 
can be as high as 10\%. Using time-preserving trace on these
benchmarks eliminates the error on \emph{MM} and reduces
the error on \emph{CFD} to 2\%. This experiment 
confirms that \emph{the use of traditional composition as in HOTL
does introduce error and parallel footprint 
improves the previous footprint composition 
(HOTL Theory in Section~\ref{sec:fp}).}
\end{comment}

\begin{figure}[ht]
\centering
\includegraphics[width=0.48\textwidth, trim=4 4 4 4,clip]{figures/misprediction}
\caption{The average absolute error of miss ratio for L1 placements by each benchmark. }
\label{fig:mr_results}
\end{figure}

\subsection{Modeling Cost}
\label{sec:cost}

In a fraction of the time it takes to simulate a program for one data placement,
data centric analysis predicts the miss ratio for all placements.
As a result, it gains unprecedented speed compared to
measurement methods.  

Table~\ref{tbl:overhead} shows the total cost of
modeling for each test program, normalized by the time of a single
simulation run.  Most of the cost of
data centric analysis is profiling for reuse time.  The rest is converting
it to footprints and miss ratios.  The time for predicting for one
placement is negligible, less than $0.1\%$ of profiling.  The 
prediction time is not included in the table.

\begin{table}[h!]
%\tabcolsep=0.11cm
  \centering
  \caption{Time cost of different techniques, measured by the total time normalized to the time it takes to simulate the program for a single data placement. }
  \vspace{1mm}
  \resizebox{0.48\textwidth}{!}{
  \begin{tabular}{|c|c|c|c|c|}
    \hline
    & \multicolumn{4}{c|}{\textbf{Normalized cost}} \\
    \hline
    \textbf{benchmarks} & \textbf{Baseline} & \textbf{Footprint} & \textbf{PORPLE} & \textbf{Set-RD}\\
    \hline
    \hline
    \textbf{BFS} & 8.0 & 0.4 & 0.1 & 21 \\
    \hline
    \textbf{FFT} & 2.0 & 1.5 & 6.8 & 350 \\
    \hline
    \textbf{Scan} & 2.0 & 1.1 & 1.3 & 54 \\
    \hline
    \textbf{Reduction} & 2.0 & 0.8 & 0.7 & 118 \\
    \hline
    \textbf{Sort} & 4.0 & 1.9 & 0.8 & 196\\
    \hline
    \textbf{Triad} & 4.0 & 2.2 & 0.2 & 240 \\
    \hline
    \textbf{Stencil2D} & 2.0 & 0.7 & 0.5 & 78 \\
    \hline
    \textbf{Trans} & 2.0 & 1.4 & 0.5 & 54 \\
    \hline
    \textbf{MM} & 4.0 & 0.3 & 6.7 & 344 \\
    \hline
    \textbf{MD} & 4.0 & 1.1 & 39 & 364 \\  % don't highlight 
    \hline
    \textbf{SpMV} & 16.0 & 0.8 & 4.0 & 1072 \\
    \hline
    \textbf{CFD} & 128.0 & 4.2 & 0.6 & 3840 \\
    \hline
    \textbf{ParticleFilter} & 3.0 & 0.2 & 0.6 & 90 \\
    \hline
    \hline
    \textbf{mean} & 14 & 1.6 & 4.7 & 524\\
    \hline
%    \textbf{gmean} & 4.6 & 1.3 & 1.1 & 190\\
%    \hline
  \end{tabular}
  }
  \label{tbl:overhead}
\end{table}

PORPLE and Set-RD profile a program to measure the reuse distance. The
implementations follow the algorithms by Kim
et al.~\cite{Kim+:SIGMETRICS91} and Almasi et al.~\cite{Almasi+:MSP02}
respectively.  Both implementations use stack
processing\footnote{Asymptotically more efficient reuse distance
  measurements exist but we retain the original design of these two
  systems.}.
Stack processing has a high space overhead and greater asymptotic
complexity.  The cost increases dramatically for large traces.  PORPLE
is fast in most cases, but for one program it has 39 times slowdown.
In contrast, footprint analysis is constant time per trace element
and has linear time complexity.

Set-RD is accurate and predicts the miss ratio for all cache
associativity.  PORPLE is 111 times faster but does not model set
associativity.  Data centric analysis is 3 times faster than PORPLE.
The faster speed, 327 times faster than SetRD, and higher accuracy,
4.3 times lower error, 
show the benefit of the combined analysis.  We next evaluate
its component models.

\subsection{Effect of Mapped Footprint}
\label{sec:eval_mfp}

From Table~\ref{tbl:mr_results},  PORPLE's prediction
has a high error for two benchmarks, \emph{MM} and \emph{CFD}.  
Take the third case of \emph{MM} as an example.  PORPLE
predicts miss ratio 3.4\%, while the simulation shows
42\%, the difference, 39\%, is conflict misses.  
Table~\ref{tbl:smith-rd-fp-conflict} compares the four methods
for this case.
The Smith model, introduced in Section~\ref{sec:conflict}, is
the mostly widely used method in the last four decades, predicts 23\%.
The 19\% error is caused by uneven data mapping among cache sets.  
Mapped footprint predicts 49\%, reducing the error to 7\%.  
%Although less accurate, Footprint can predict conflicts in all 
%data placements, while Set-RD has to re-measure for each 
%placement.  

\begin{table}[h!]
%\tabcolsep=0.11cm
  \centering
  \caption{Model accuracy for Array II of \emph{MM}}
  \vspace{1mm}
  \resizebox{0.48\textwidth}{!}{
  \begin{tabular}{|c|c|c|c|c|}
    \hline
     &  & \multicolumn{3}{c|}{cache conflict models (Section~\ref{sec:conflict})} \\ \cline{3-5}
     \textbf{Baseline}  & \textbf{PORPLE} & \textbf{Smith} & \textbf{Set-RD} & \textbf{Footprint} \\
    \hline
    \hline
    42\% & 3.4\% & 23\% & 42\% & 49\% \\
    \hline
  \end{tabular}
  }
  \label{tbl:smith-rd-fp-conflict}
\end{table}

%Mapped footprint can model 
%both of these configurations from one-time profiling
%and tackles the cache non-uniformity. Set-RD and cache
%simulation has to profile the trace once for every
%configurations. Therefore the time saved from 
%repetive trace profiling is proportional to the
%number of possible cache configurations.
Figure~\ref{fig:conflict_effect} shows the prediction accuracy of four
benchmarks under two different cache configurations: 16KB and 48KB.
In the two cases circled in Figure~\ref{fig:conflict_effect}, the
set-associative cache (first bar) incurs a lower miss count than the
full-associative cache (second bar).  The conflict miss count is
\emph{negative}.  The Smith model assumes even data mapping and cannot
model these effects.

\begin{figure}[h]
\centering
\includegraphics[scale=0.23,trim=4 4 4 4,clip]{figures/conflict_effect_1}
\caption{Model accuracy for two programs with significant cache conflicts. The 16KB
cache has 32 cache sets
and 4 ways per set, while the 48KB cache 64
sets and 6 ways per set. }
\label{fig:conflict_effect}
\end{figure}

Set-RD is accurate, which we use as the baseline.  It captures the
exact memory mapping, but the number of cache set is fixed.  On Fermi,
the L1 cache may have 32 sets or 64 sets, and the texture cache has 12
sets.  Set-RD has to profile three times, one for each set count.  
Mapped footprint models the uneven mapping among cache sets
and profiles just once to analyze all three set counts.  

\begin{figure*}
\begin{tabular}{cc}
\includegraphics[width=.49\textwidth]{figures/100new.pdf} &
\includegraphics[width=.49\textwidth]{figures/10new.pdf} \\
(a) Iterations = 100 & (b) Iterations = 10
\end{tabular}
\centering
\caption{Speedup of benchmarks on Tesla M2075 with a different number of kernel invocations.
PORPLE-RD: original PORPLE using reuse distance; PORPLE-FP: PORPLE with
data centric analysis.}
\label{fig:speedup}
\end{figure*}

\subsection{Effect of Dual Granularity Footprint}
\label{sec:eval_dfp}

The texture cache adds heterogeneity in two aspects.  Within itself,
not all sectors in a cache block are occupied.  When used by a
programmer, a read-only array can be allocated to memory (no
cache), L1 or the texture cache.  It means $3^n$ placements for $n$
such arrays.  Dual granularity footprint allows data centric analysis
to model the full heterogeneity and directly compute the
performance of all $3^n$ placements.

As Table~\ref{tbl:mr_results} shows, data centric analysis is as
accurate for the texture cache as it is for L1.  The highest error is
36\% prediction vs 29\% measurement in one of the 11 placements of
\emph{SpMV}.  In contrast, PORPLE cannot distinguish cache types and
has to treat the texture cache as (12KB) L1.  PORPLE errors are
greater in the texture cache, e.g. 20\% in all 3 cases of \emph{MM}.
The larger errors of PORPLE indicate that the texture cache differs
from L1, and data centric analysis captures this difference.

\begin{comment}
\subsection{Summary}
\label{sec:eval_summary}

In summary, the technique of parallel footprint enables
a new way of tackling data non-uniformity within programs. It
also improves the traditional footprint method's prediction
accuracy. Mapped footprint improves upon the existing
model of set associativity. It enables the model to be
composable and solves the problem of cache non-uniformity.
The use of dual granularity footprint models GPU's texture cache and 
makes this technique applicable on modeling the cache 
resources on GPU devices.

\dpar{Comparison with Hardware Counters} 
We have collected the hardware counter results.  The prediction is
accurate for some programs, but the error is 5\% or higher for 8
programs.  The largest error is as high as 50\% in \emph{MM}.
However, it is unscientific to draw conclusion from this comparison
for three reasons.  First, we model cache but not thread and
thread block scheduling.  Second, the hardware counter may double
count misses, which do not reflect real memory costs.  Third, the
hardware cache design is not completely known, so a matching
prediction may be by accident rather than a correct model.
These three reasons are discussed in more detail in
Section~\ref{sec:eval_setup}.  

There are many ways to collect a trace. We instrument real program
execution, so did Chen et. al.~\cite{Chen+:Micro14}.  Another way is
emulation, e.g. by GPU Ocelot~\cite{Diamos+:PACT10}, as used by
Nugteren et. al.~\cite{Nugteren+:HPCA14}.  Baghsorkhi et. al.  employed a
Monte Carlo method to construct multiple variants of a memory trace to
account for non-determinism~\cite{Baghsorkhi+:PPOPP12}.
In all these cases, an accurate cache model is needed.  In fact, we
have used GPU Ocelot, which assumes uniform interleaving, and studied
the variation in performance prediction.
\end{comment}

\begin{comment}
\dpar{Summary} Figure~\ref{fig:mr_results} summarizes the absolute
error of miss ratio from L1 cache measurement and prediction.
Footprint has 5\% error in one program, 3.3\% in one program, 1\%-2\%
in four programs, and 0 or nearly 0 in the remaining 7 programs, over
half of the test suite.  PORPLE's model show over 10\% error in 2
programs and one program over 8\%.  The Footprint
error is less than 1/4 of PORPLE on average, so it is more 
than 4 times more accurate than the previous prediction.  
\end{comment}

\begin{comment}
\subsection{Generality}
\label{sec:eval_generality}

Table~\ref{tbl:generality} compares the four modeling techniques by
their ``generality'', which is defined as the capability of generalizing
the result of a single-pass analysis to predict the performance for
multiple hardware or software configurations. 

Baseline simulation can model the texture cache.  Set-RD uses the
reuse distance profile per cache set, so it generalizes to cache with
different associativities, as long as the number of cache sets is
fixed.  Both have to measure an execution once for each data placement.
PORPLE generalizes to different data placements and the texture
cache.  However, it does not model set-associative cache.  

Footprint is the most general of all and can predict the performance
of any data placement for both normal and texture cache for different
number of cache sets and associativity.  More importantly, this
generality comes with unprecedented speed and consistently high
accuracy, as we have examined earlier in this section.  We next
use the model to optimize GPU code.
 
\begin{table}[h!]
%\tabcolsep=0.11cm
  \centering
  \caption{A comparison of the modeling techniques by their generality, which
    means whether a single-pass analysis can predict results for the following
    hardware/software configurations: \textbf{Cache set}
    and \textbf{associativity} are set-associative cache parameters, \textbf{Data placement} is different array assignments to cache, and \textbf{Texture cache} is the special GPU cache.}
  \vspace{1mm}
  \resizebox{0.48\textwidth}{!}{
  \begin{tabular}{|c|c|c|c|c|}
    \hline
                            & \textbf{Baseline} & \textbf{Set-RD} & \textbf{PORPLE} & \textbf{Footprint}\\
    \hline
    \hline
    \textbf{cache sets}     & & & & \Checkmark \\
    \hline
    \textbf{associativity}  & & \Checkmark & & \Checkmark \\
    \hline
    \textbf{data placement} & & & \Checkmark & \Checkmark \\
    \hline
    \textbf{texture cache}  & \Checkmark & & \Checkmark & \Checkmark \\
    \hline
  \end{tabular}
  }
  \label{tbl:generality}
\end{table}
\end{comment}
