\documentclass[nocopyrightspace,preprint,9pt]{sigplanconf}

\usepackage{amsmath}
\usepackage{bbding}
\usepackage[normalem]{ulem}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{array}
\usepackage{float}
\usepackage{hyperref}
\usepackage{algorithm} %ctan.org\pkg\algorithms
\usepackage{algorithmic}
\usepackage{listings}
\usepackage{multirow}
%\usepackage{algpseudocode}
\usepackage{comment}

%\floatsetup[table]{capposition=top}

%\algsetup{linenosize=\small}
\renewcommand{\algorithmicrequire}{\textbf{Input:}}
\renewcommand{\algorithmicensure}{\textbf{Output:}}
\usepackage{xcolor}
\newcommand{\TODO}[1]{{\it \color{blue}\{TODO: #1\}}}

\definecolor{Brown}{cmyk}{0,0.81,1,0.60}
\definecolor{OliveGreen}{cmyk}{0.64,0,0.95,0.40}
\definecolor{CadetBlue}{cmyk}{0.62,0.57,0.23,0}
\definecolor{lightlightgray}{gray}{0.9}

\newcommand{\dpar}[1]{\medskip\noindent {\bf #1 }}

\clubpenalty = 10000
\widowpenalty = 10000

\setcounter{page}{1}
\pagenumbering{arabic}

\begin{document}

%\title{Data-centric Combinatorial Optimization of Parallel Code \vspace{-2cm}}
\title{Data-centric Combinatorial Optimization of Parallel Code \vspace{-1.5cm}}
%\title{Composable Modeling of GPU Cache Performance}

%\authorinfo{Hao Luo${}^\ast$, Guoyang Chen${}^+$, Pengcheng Li${}^\ast$, Chen Ding${}^\ast$, Xipeng Shen${}^+$}
%           {${}^\ast$ University of Rochester\\
%            ${}^+$ North Carolina State University}
%           {${}^\ast$\{hluo, pli, cding\}@cs.rochester.edu, 
%            ${}^+$\{gchen11, xshen5\}@ncsu.edu}
\authorinfo{}{}

\maketitle

\begin{abstract}

  Memory performance is essential for tapping into the full potential
  of the massive parallelism of GPU. It has motivated some recent
  efforts in GPU cache performance modeling.  This paper presents a
  new way to model GPU cache performance. The new model is composable:
  it accurately predicts the performance of all possible data
  placements of a program by profiling the program just once.  It
  predicts set-associative cache including both normal and texture
  cache.

  Evaluation on 13 GPU benchmarks shows that the composable model has
  unprecedented speed and consistently high accuracy (99.2\%).  It is hundred
  times faster or four times as accurate than two previous techniques.
  When it is applied to guide data placement optimizations, the new
  model boosts the average speedup from 9\% to 19\%. It opens new
  opportunities for understanding and improving GPU memory
  performance.
%% Modern GPUs have a heterogeneous memory hierarchy with explicitly 
%% managed fast memory and multiple types of cache memory.  The GPU 
%% performance depends significantly on how 
%% the program data is placed in memory, yet trying to find the optimal
%% placement by testing all possible layouts
%% is inefficient and impractical.

%% This paper presents a profile-based, composable solution to
%% efficiently and accurately model GPU cache performance. It profiles
%% an execution once and then predicts the miss ratio of all possible
%% array placements on varying cache configurations.  The composable
%% analysis is the first for regular cache, texture cache, and the
%% effect of set-associativity.  Evaluations on 13 GPU benchmark tests
%% show that the model is 98\% accurate, runs 80 times faster, and
%% optimizes the programs better than two previous solutions.

\end{abstract}

\section{Introduction}

% new introduction to speak to both GPU and non-GPU experts and
% emphasize the value of fundamental work

Memory performance is essential to parallel performance.  This paper
is concerned with highly coupled parallelism on the current GPU
architecture.  While the degree of parallelism on a GPU chip is not
overly large compared to traditional massively parallel processing
(MPPs), the memory system is very different.  The speed of access is
far greater when the datum is on-chip.  When it is not, the aggregate
bandwidth is far lower, since all data reside on one module of main
memory.

Indeed, memory is the bottleneck on GPUs and as a result, has the most
complex design.  It includes shared memory (registers) and various
types of cache: L1/L2, texture and constant caches.  The memory
complexity continues to increase as GPU architects try to maximize the
throughput of highly threaded executions.

A programmer now has the burden of dealing with many parameters.  She
chooses what data to cache and what size to configure the cache and
non-cache memory.  Each parameter may have a large impact on
performance.  Exhaustive testing, either through direct execution or
simulation, would take too long and even after it is done, the result
offers little insight about a program or its machine.

In this paper, we take the approach of modeling, of both a parallel
program and the GPU hardware.  Modeling is fast.  It profiles a
program execution just once and then calculates the performance when
we examine all parameters.  It picks the best values after examining
all possible values of parameters.  The combinatorial search is based
on a model of the program rather than repetitive, exhaustive testing.

To build such model, we formulate and solve a fundamental problem on
multi-core and many-core processors --- the heterogeneity problem.
Heterogeneity exists in software and hardware:

\begin{itemize}
\item \emph{Data nonuniformity}.  
  Parallel tasks may be asymmetric and therefore show
  nonuniform memory access patterns. Even for symmetric
  tasks, nonuniform locality can be found between different
  data. This is the heterogeneity in software.
\begin{comment}
  % Hao : This claim sounds weird. You claim data nonuniformity,
  % but instead of explaining it, you added "asymmetric task"? 
  % is this about task or data?
  Parallel tasks may be asymmetric. Even when they are 
  symmetric, e.g. in data parallel code, the data they 
  use have different access patterns.
\end{comment}

\item \emph{Memory heterogeneity}.  
  On-chip memory has different types. For example, L1 data
  cache and texture cache on GPUs. L1 cache has larger capacity
  and texture cache has longer access latency. This is the 
  heterogeneity on memory resources.
\begin{comment}
  % Hao : what is direct access? what is automatic caching?
  % This bullet is about introducing the concept of memory
  % heterogeneity, I don't think we need to put our work in
  % under this bullet.
  On-chip memory may be used for
  direct access (registers) or automatic caching.  Hardware cache has
  different types.  We will consider the sector cache which loads data
  at the granularity of a partial rather than full block and solve it
  as a problem of heterogeneity.
\end{comment}

% Hao : XXX 
% The following bullet is confusing. It appears as the classic technique
% is using set-associative cache and we have a better design than set 
% associative cache? 
\item \emph{Cache nonuniformity}.  
  Even within one type of cache, nonuniformity in cache configurations
  exists. For example, L1 data cache on GPUs can be configured
  to different capacity and different number of cache sets.  
  This is the heterogeneity in memory resource's configurations.
\begin{comment}
  Hardware cache is set associative.
  We will solve it better than the classic technique on 
  set associativity used in the last four decades, which assumes uniformity.
\end{comment}
\end{itemize}

% Hao : What exactly is 'interact'? How could array actively compete
% for cache space? And here, it seems the term 'heterogeneity' is already
% being used for 'data nonuniformity'?
Different heterogeneities interact.  Asymmetric tasks and different
arrays compete for the same cache space, and the individual occupancy
varies depending on the interaction with its peers.  The
cache occupancy further depends on the associativity, data
mapping and cache management.  It is in this vast space of non-uniform
interaction there lies the greatest potential for optimization and
also the newest challenge for modeling.

We present three solutions to solve three problems of heterogeneity.
The first solves the data nonuniformity by modeling the interaction of
arrays in cache.  The next two solve the memory and cache nonuniformity by
modeling the non-uniform associativity and granularity.

The three solutions are grounded on a single theory based on the
footprint of data access.  The footprint gives the average size of the
data used in a time period.  In this paper, we make three orthogonal
extensions, each for use by one of the three solutions.  We can then
combine the methods to cover the parameter space in three dimensions
and model the interaction within each type and across all three types
of heterogeneity.

The combined analysis helps a programmer to gain insight.  For
example, we can dissect and quantify the performance of a single array
as we change its peer arrays in cache and change the size,
associativity or type of cache.  All three solutions use just one
measurement, the footprint, and do not require a new measurement for
each analysis, improving speed as well as consistency.

The new models overcome the major difficulties of existing work.
Testing and simulation show what happen for a given configuration but
not what may happen in all configurations.  Concurrent reuse distance
gives the performance of cache of all sizes~\cite{Wu+:ISCA13} for fixed rather
than composable groups.  Working-set models handle composition for
independent programs but not the threads of a parallel application.
They do not model the effect of associative cache, which is more
pronounced on GPUs than on CPUs.  Recently, Nugteren et al. gave the a
reuse distance model for GPUs~\cite{Nugteren+:HPCA14}.  It is precise, but the precision
assumes a fixed number of cache sets (and a fixed array group).  Our
new methods remove these limitations and combine their strengths into
a general model.

\iffalse
For a massively parallel architecture like Graphic Processing Units
(GPU), it is important to narrow the gap between the memory throughput
that its hundreds of cores demand and what memory systems can provide.
The issue has prompted the adoption of sophisticated designs of GPU
memory systems. They rely on various on-chip memory resources (L1/L2, shared memory,
texture cache and constant cache) for a high throughput.

To better utilize different on-chip memories, 
many performance models have been proposed in recent years to 
help understand software's memory access patterns
and their implication on memory performance~\cite{Baghsorkhi+:PPOPP10, 
Jang+:TPDS11, Sung+:PACT10, ZhangO:HPCA11, Sim+:PPOPP12, 
Nugteren+:HPCA14, Chen+:Micro14, Li+:CGO15}.
An important class is trace-based modeling, 
which estimates GPU cache's performance based on the 
memory access stream of a program~\cite{Baghsorkhi+:PPOPP10, 
Nugteren+:HPCA14}. Having a good GPU cache performance model is 
essential for guiding GPU cache architecture design and program 
optimization to improve GPU memory performance. 

% Hao added
% argument on why this work is meanful
In this paper, we propose a new GPU caching
model.  It aims for three goals:
\emph{efficiency}, low cost in model construction;
\emph{generality}, the ability of generalizing for different contexts;
and most importantly, \emph{applicability}, application on optimizing performance.
The new model, by our design, aims to satisfy
these three criteria.

First, our model enables \emph{composable} locality analysis.
Being composable means for a set of data objects (e.g. a set of arrays in the program), 
the analysis can easily derive the combined locality from the individual locality.
A formal definition of composability is given in Section~\ref{sec:composable}.
Composable analysis is efficient, since the composition analysis
reuses rather than repeats individual analysis.  
%Composable analysis wins at avoiding many unnecessary operations. 
For example, the locality of two arrays cached together can be derived 
from the cache locality of individual arrays.
%For example, composable model is able to derive the locality of 
%two arrays in the program by composing from the localities of 
%individual arrays. 
The composition avoids the overhead of profiling a program more than
once.

Second, our model is \emph{general}.  The composable analysis
handles hardware details on GPUs including the set-associative cache
and the texture cache of all sizes and associativity.  In comparison,
the past techniques are either not precise or not flexible enough.

Third, the \emph{applicability} of this model is demonstrated in
data placement optimization. We report our experience of
using this model in a state-of-the-art data placement optimizer
in Section~\ref{sec:opt}.
The key that enables this application is composability and
generality makes this optimization portable across platforms. 
Studies have shown that several times of performance improvement 
could be brought by selectively letting arrays go through L1 
cache, texture cache, constant cache, or shared memory on
GPU~\cite{Chen+:Micro14,Jang+:TPDS11}. There are exponentially many 
possibilities for array placement. Finding the best placement
requires assessment of most if not all of these possibilities. 
Lacking composability, existing cache performance
models~\cite{SenW:SIGMETRICS13, Nugteren+:HPCA14} would need to 
process the access traces for each of $2^K-1$ subset of $K$ arrays. 

With a composable model that is general enough for different caches on GPU,
only $K$ traces (one for each array) need to be processed, from which,
the performance of accesses to any subset of the arrays can be quickly
composed. The exponential-to-linear reduction of the complexity is
significant: It makes cache performance analysis orders of magnitude
faster, and opening the possibility for accurate cache models to be
used even during execution time. 

%from cache performance on each of a set of data
%objects, the model can easily derive the cache performance on accesses
%to all of the objects. 

The composability is essential for program
optimizations. An example is data placement optimizations.  
Studies have shown that several times of performance could be brought by
different decisions on letting which arrays go through L1 cache,
texture cache, constant cache, or shared memory on
GPU~\cite{Chen+:Micro14,Jang+:TPDS11}. There are exponentially 
many possibilities for array placement. Finding the best of them
requires assessment of most if not all of these
possibilities. Lacking composability, existing cache performance
models~\cite{SenW:SIGMETRICS13} would need to process the access traces for
each of $2^K-1$ subsets for $K$ arrays. With our composable model,
only $K$ traces (one for each array) need to be processed, from which,
the performance of accesses to any subset of the arrays can be quickly
derived. The exponential-to-linear reduction of the complexity is
significant: It makes cache performance analysis orders of magnitude
faster, and opening the possibility for accurate cache models to be
used even during execution time.

Second, the new model handles not only traditional cache, but also
texture cache on GPU. Texture cache is an important part of GPU memory
systems; its unique support of 2D/3D data locality is essential to
many applications. However, the way it works differs from traditional
cache by featuring a sector-based management. The complexity has put
texture cache beyond the scope of prior GPU cache models. Our
new model overcomes that limitation.

% Hao : the importance of this work, benefits of the community
A cache model is a necessary component in a GPU performance model.  A
correct model makes accurate prediction given a memory access trace.
On GPUs, however, the non-deterministic memory access interleaving
complicates the trace collection.  Instrumentation may change how
warps and thread blocks are scheduled, as observed in prior
work~\cite{Baghsorkhi+:PPOPP10}.  This study focuses on cache modeling
and evaluates using both simulation and real executions.  We
will show 98\% accuracy when the access trace is given.  The accurate
model can then be used with different trace collection tools or serve
as an ``accelerator'' for evaluating the locality of different
scheduling policies~\cite{Huang+:Micro14}.  GPU architecture models
such as those of~\cite{HongK:ISCA09, Sim+:PPOPP12, ZhangO:HPCA11} can
take our technique as a component to evaluate the effect of all cache
sizes and associativity.  
\fi

% The new model is also evaluated against 13 benchmarks and integrated into
% a data placement optimizer to demonstrate its value. (Section~\ref{sec:eval}) 
%Experiments on a set of GPU benchmarks show that the
%model produces accurate cache performance estimation (on average 98\% for L1 and
%99.8\% for the texture cache 
%compared to cache simulation results)
%with orders of magnitude lower overhead than existing solutions. When
%applied to guide runtime data placement on GPU, it helps produce 1.24X
%speedups compared to the original benchmarks. 


%Many GPU performance models have been developed in recent years.
%Some of them require multiple runs of micro-benchmarks
%\cite{HongK:ISCA09, ZhangO:HPCA11}, and
%some are costly due to its detailed
%micro-architecture model~\cite{Baghsorkhi+:PPOPP12}.
%These models are not composable and require repeated testing
%when the data layout changes.  By focusing on the cache performance,
%we can develop a composable analysis that requires just a single
%execution of the target program.

%\begin{figure*}[ht]
%\centering
%\includegraphics[width=0.85\textwidth,height=0.29\textwidth, trim=4 4 4 4,clip]{figures/composition_illustration}
%\caption{Overview of the composable analysis: it profiles the
%  interleaved access trace once and constructs the footprint profile
%  for each array.  The footprint of individual arrays are
%  composed to compute the miss ratio for all array groups. The optimal placement is
%  found by finding the array grouping with the lowest miss rartio. }
%\label{fig:overview}
%\end{figure*}

% A complete solution requires accurate trace collection before modeling
% and complete knowledge of the hardware cache design.  We evaluate
% using both simulation and real executions.  The two types of
% measurements allow us to separate different sources of errors and
% separately evaluate the accuracy of composable analysis.

This paper makes the following contributions: 

\begin{itemize}
\item Two techniques called time-preserving trace decomposition 
  to enable composable
  analysis of parallel code, a new model of parallel footprints,
  its measurement and composition (Section~\ref{sec:mr}),
%  locality 
%  composition, footprint measurement, and
%  miss-ratio prediction for fully associative cache.

\item a model called mapped footprint for set-associative cache that can handle caches of not
  just different associativity but also different number of cache
  sets (Section~\ref{sec:conflict}),

%composable analysis, which infers the miss ratio for all array
%  combinations and all cache sizes.

\item a model called dual granularity footprint to model GPU texture cache to compute performance for all
  possible data placements and different cache size and associativity (Section~\ref{sec:tex_mr}),

\item the evaluation of the individual and combined analysis on 13 GPU benchmarks and
  comparison with two previous solutions (Section~\ref{sec:eval}), 

\item improving the performance of 6 programs using a new, state-of-the-art data placement
optimizer called PORPLE~\cite{Chen+:Micro14} (Section~\ref{sec:opt}).
\end{itemize}

In this work, we model the performance by counting misses, not
execution time (cycles).  A machine-independent model lets a
programmer to optimize a program across machines.  It directly
correlates between program events, e.g. arrays, and hardware
utilization, e.g. cache occupancy.  While no study can include all
factors of performance, the success of this work would increase our
means (and confidence) that the increasingly complex problem of
parallel performance can be decomposed into interacting parts and
optimized using computational and mathematical models, in addition to
algorithm improvement, compiler optimization, and testing and simulation.


% Other types of GPU cache optimization such as kernel
% restructuring~\cite{Unkule+:CC12} can also benefit from this work.

% A CUDA programmer has to choose which arrays to store in
% which cache and for the L1 cache, how large its size should be.

% Using composable analysis, it can quickly evaluate all placement
% choices for a given architecture and select the best solution.  The
% support is specialized for a given program, adaptive across inputs,
 % and portable across architectures.


\input{model}
\input{evaluation}
\input{cases}

%\section{Adaptive Online Profiling through Active Learning}

%\section{Fast Placement Search through Greedy Algorithm}

%\section{Evaluation}

\section{Related Work}

\dpar{GPU Performance Analysis and Improvement}
Many studies have found that memory performance is critical for GPU
programs~\cite{Che+:SC11,Baskaran+:ICS08,Sung+:PACT10,Jia+:ICS12}. 
It has been a focus of program optimization.
Zhang~\cite{Zhang+:ASPLOS11} designed a data reorganization method to reduce
memory transactions for irregular accesses. Wu~\cite{Wu+:PPOPP13} provided several 
efficient reorganization algorithms to solve the problem of data reorganization.
Yang~\cite{Yang+:PLDI10} used a source-to-source compiler to enhance memory coalescing.
Bell and Garland utilized the texture cache to improved the
locality of sparse matrix vector multiplication~\cite{BellG:SC09}. 
Composable analysis addresses the problem of array placement in
different types of GPU memory.

Many GPU performance models have been developed.  \linebreak Hong
gave a sophisticated analytical model, which is based on a set of  
parameters including those of memory properties such as overlapping
among different memory transactions~\cite{HongK:ISCA09}.  Baghsorkhi~\cite{Baghsorkhi+:PPOPP10} designed 
a static model in a compiler based on careful considerations of the
architecture details.  Composable analysis focuses on locality
and cache performance, especially the problem of data placement.

GPU data placement has been carefully studied. 
Jang~\cite{Jang+:TPDS11} developed a rule based system, where rules were
learned empirically.  However, such rules are not portable to different architectures
and even program inputs.  Ma~\cite{MaA:PACT10} developed a solution,
but only for data placement in the
shared memory. 
The PORPLE system~\cite{Chen+:Micro14} includes an automatic data placement tool which exploits 
importance of architectures and data access patterns when determining the best data 
placement.  As reported in Section~\ref{sec:opt}, composable analysis
provides better higher accuracy when evaluating data placement and as 
a result has further improved the performance for two GPU programs.

\dpar{Trace Filtering}
Shen et al.~\cite{shen+:ASPLOS04} converted a trace into a sequence of reuse distances
and then filtered the number sequence based on a threshold.
Kessler~\cite{KesslerHW:TOC94} trimmed a full trace into a 
filtered trace by keeping
only the accesses to a segment of memory.  The filtering is spatial in
\cite{KesslerHW:TOC94} and frequency-based in \cite{shen+:ASPLOS04}.  The latter is
analogous to extracting a sub-signal from a full signal using a
frequency filter.

\dpar{Composable Models of Locality}
Previous work uses the HOTL theory to minimize program
interference in shared cache~\cite{Xiang+:CCGrid12}.  
The HOTL theory cannot be used on GPU programs because of four
limitations.  First, HOTL applies to independent sequential programs
and assumes uniform interleaving.  Second, HOTL composition is for
, not data.  Third, GPU programs tend to cause more cache conflicts
because of the relatively small cache size, but HOTL models fully 
associative LRU cache.  Finally, HOTL does not model
the texture cache.  In this paper, we have developed time-preserving decomposition,
enabled composable analysis of parallel code, and invented 
the first composable models of
set-associative cache and texture cache.

\dpar{Cache Modeling}
Nugteren et al. presented a GPU cache model
based on reuse distance. The proposed model also is able to identify
the different types of misses (compulsory miss, capacity miss and conflict miss)
and takes many hardware details such as MSHR and access latency into account. 
But it did not model texture cache~\cite{Nugteren+:HPCA14} and 
did not focus on explore the performances of different data placements.
Smith developed the statistical model to estimate the effect of
set-associative cache using the reuse distance~\cite{Smith:ICSE76}. 
Our model adapts the Smith formula to use the footprint instead of
the reuse distance.   Since it is based on the footprint, it is the
first composable model of cache associativity.

\section{Summary}

In this paper, we have presented the first composable analysis to
model the GPU cache.  The techniques include time-preserving trace
decomposition and parallel footprints to model data placement,
mapped footprint to model set associativity, and dual granularity
footprint to model cache heterogeneity.  

The evaluation on 13 GPU benchmarks shows that the
composable model takes a fraction of the time of a single cache
simulation to predict the cache performance for all data placements, and the prediction is 99.2\% accurate.  Its average
error is less than 1/4 of PORPLE, the previous composable technique.  
When it is applied to guide data placement optimizations, the
new model boosts the average speedup from 9\% to 19\%. It demonstrates
a solution for
data-centric, combinatorial optimization of parallel code.

% As a future work, we will focus on identifying the scenarios where our
% model has high prediction inaccuracy and try to tackle the limitation
% of this work.

\bibliographystyle{plain}
\vspace{-3mm}
\small{
\bibliography{../bib/all}
%\bibliography{all}
}
\end{document}

% LocalWords:  GPU
