\documentclass[nocopyrightspace,preprint,9pt]{sigplanconf}

\usepackage{amsmath}
\usepackage{bbding}
\usepackage[normalem]{ulem}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{array}
\usepackage{float}
\usepackage{hyperref}
\usepackage{algorithm} %ctan.org\pkg\algorithms
\usepackage{algorithmic}
\usepackage{listings}
\usepackage{multirow}
%\usepackage{algpseudocode}
\usepackage{comment}

%\floatsetup[table]{capposition=top}

%\algsetup{linenosize=\small}
\renewcommand{\algorithmicrequire}{\textbf{Input:}}
\renewcommand{\algorithmicensure}{\textbf{Output:}}
\usepackage{xcolor}
\newcommand{\TODO}[1]{{\it \color{blue}\{TODO: #1\}}}

\definecolor{Brown}{cmyk}{0,0.81,1,0.60}
\definecolor{OliveGreen}{cmyk}{0.64,0,0.95,0.40}
\definecolor{CadetBlue}{cmyk}{0.62,0.57,0.23,0}
\definecolor{lightlightgray}{gray}{0.9}

\newcommand{\dpar}[1]{\medskip\noindent {\bf #1 }}

\clubpenalty = 10000
\widowpenalty = 10000

\setcounter{page}{1}
\pagenumbering{arabic}

\begin{document}

\title{Composable Modeling of GPU Cache Performance}

\authorinfo{Hao Luo \and Pengcheng Li \and Chen Ding}{Department of Computer Science \\University of Rochester}{}
\authorinfo{Guoyang Chen \and Xipeng Shen}{Department of Computer Science \\North Carolina State University}{}
%\authorinfo{Pengcheng Li}{Department of Computer Science \\University of Rochester}{}
%\authorinfo{Xipeng Shen}{Department of Computer Science \\North Carolina State University}{}
%\authorinfo{Chen Ding}{Department of Computer Science \\University of Rochester}{}


\maketitle

\begin{abstract}

Modern GPUs have a heterogeneous memory hierarchy with explicitly 
managed memory and multiple types of cache memory.  The GPU 
performance may vary significantly depending on which memory 
the program data is placed in.  In order to find the optimal
placement, relying on costly detailed cache simulation to 
exhaust all possible layouts is inefficient and even 
impractical.

This paper presents a composable, profile-based solution to 
efficiently and accurately model GPU cache performance. It profiles an execution
once and then predicts the miss ratio of all possible array 
placements on varying cache configurations.  This approach applies on both 
set-associative L1 cache as well as texture cache.  Evaluations on 
13 GPU benchmark tests show that the model is 98\% accurate for 
both caches against trace-based simulation.  
In addition, it achieves 80x speedup over
the state-of-the-art cache performance model. 
A case study of using this model to optimize 
GPU program's data placement is also reported.

\end{abstract}

\section{Introduction}

For a massively parallel architecture like Graphic Processing Units
(GPU), it is important to narrow the gap between the memory throughput
that its hundreds of cores demand and what memory systems can provide.
The issue has prompted the adoption of sophisticated designs of GPU
memory systems. They rely on various on-chip memory resources (L1/L2, shared memory,
texture cache and constant cache) for a high throughput.

To better utilize different on-chip memories, 
many performance models have been proposed in recent years to 
help understand software's memory access patterns
and their implication on memory performance~\cite{Baghsorkhi+:PPOPP10, 
Jang+:TPDS11, Sung+:PACT10, ZhangO:HPCA11, Sim+:PPOPP12, 
Nugteren+:HPCA14, Chen+:Micro14, Li+:CGO15}.
An important class of them is trace-based cache model, 
which estimates GPU cache's performance based on the 
memory access stream of a program~\cite{Baghsorkhi+:PPOPP10, 
Nugteren+:HPCA14}. Having a good GPU cache performance model is 
essential for guiding GPU cache architecture design and GPU program 
transformations to enhance memory performance. 

% Hao added
% argument on why this work is meanful
In this paper, we propose another cache performance
model for GPU systems based on the work of locality theory~\cite{Xiang+:ASPLOS13}.
We identify three desirable criteria of 
a good model: 
\emph{efficiency}, low cost in model construction;
\emph{generality}, the ability of generalizing for different contexts;
and most importantly, \emph{applicability}, application on optimizating performance.
The new model, by our design, aims to satisfy
these three criteria.

First, we enable our cache model to support \emph{composable} locality analysis.
Being composable means for a set of data objects (e.g. a set of arrays in the program), 
the analysis can easily derive its locality quantity from those of each individual object.
A formal definition of composability is given in Section~\ref{sec:composable}.
Composability brings \emph{efficiency} on modeling, since it simplifies 
the modeling procedure of compound objects (set of data objects) by reusing 
the computed results of many single objects. 
%Composable analysis wins at avoiding many unnecessary operations. 
For example, the locality of two colocated arrays can be derived 
from the localities of individual arrays using composable model.
%For example, composable model is able to derive the locality of 
%two arrays in the program by composing from the localities of 
%individual arrays. 
The derivation avoids the overhead of scanning through the memory 
trace multiple times.

Second, our model is designed to be \emph{general} enough to provide 
insights on the performances of both the 
traditional LRU L1 cache and the texture cache but
within the constraint of passing through the trace \emph{only once}. The 
management of texture cache differs from the traditional cache by 
featuring a sector-based approach. Its complexity has put modeling 
texture cache beyond the scope of prior GPU cache models. In this work, 
we managed to extend the LRU L1 cache model with little effort to handle 
texture cache. In addition to handling 
different types of cache management, our model is also
featured with \emph{hardware independence}, i.e. the ability of predicting 
miss ratios for differnt cache capacities as well as set associativities
from one pass of the trace.
In locality theory, hardware independence is a desirable feature,
because it often reveals the essential locality-related property of
the program by decoupling the software characteristics from the 
hardware context.

Third, the \emph{applicability} of this model is exemplified by applying 
on data placement optimization. We report our experience of
using this model in a state-of-the-art data placement optimizer
in Section~\ref{sec:opt}.
The key that enables this application is composability and
generality makes this optimization portable across platforms. 
Studies have shown that several times of performance improvement 
could be brought by selectively letting arrays go through L1 
cache, texture cache, constant cache, or shared memory on
GPU~\cite{Chen+:Micro14,Jang+:TPDS11}. There are exponentially many 
possibilities for array placement. Finding the best placement
requires assessment of most if not all of these possibilities. 
Lacking composability, existing cache performance
models~\cite{Sen+:Sigmetrics13, Nugteren+:HPCA14} would need to 
process the access traces for each of $2^K-1$ subset of $K$ arrays. 
With a composable model that is general enough for different caches on GPU,
only $K$ traces (one for each array) need to be processed, from which,
the performance of accesses to any subset of the arrays can be quickly
composed. The exponential-to-linear reduction of the complexity is
significant: It makes cache performance analysis orders of magnitude
faster, and opening the possibility for accurate cache models to be
used even during execution time. 

%from cache performance on each of a set of data
%objects, the model can easily derive the cache performance on accesses
%to all of the objects. 
\iffalse
The composability is essential for program
optimizations. An example is data placement optimizations.  
Studies have shown that several times of performance could be brought by
different decisions on letting which arrays go through L1 cache,
texture cache, constant cache, or shared memory on
GPU~\cite{Chen+:Micro14,Jang+:TPDS11}. There are exponentially 
many possibilities for array placement. Finding the best of them
requires assessment of most if not all of these
possibilities. Lacking composability, existing cache performance
models~\cite{Sen+:Sigmetrics13} would need to process the access traces for
each of $2^K-1$ subsets for $K$ arrays. With our composable model,
only $K$ traces (one for each array) need to be processed, from which,
the performance of accesses to any subset of the arrays can be quickly
derived. The exponential-to-linear reduction of the complexity is
significant: It makes cache performance analysis orders of magnitude
faster, and opening the possibility for accurate cache models to be
used even during execution time.

Second, the new model handles not only traditional cache, but also
texture cache on GPU. Texture cache is an important part of GPU memory
systems; its unique support of 2D/3D data locality is essential to
many applications. However, the way it works differs from traditional
cache by featuring a sector-based management. The complexity has put
texture cache beyond the scope of prior GPU cache models. Our
new model overcomes that limitation.
\fi

% Hao : difference with prior work
The new model is a significant advancement upon footprint
theory~\cite{Xiang+:ASPLOS13}. Due to the use of a new technique
``time-preserving trace decomposition'' (Section~\ref{sec:tp_decomp}),
our model is able to directly add the footprints of multiple disjoint memory objects
without aligning the trace beforehand. This is the key to composability.
In the context of GPU memory systems where different on-chip memory resources
are present, we also model the texture cache, which has not been done
in previous works as far as we are aware of. (Section~\ref{sec:tex_mr})
The new model is also evaluated against 13 benchmarks and integrated into
a data placement optimizer to demonstrate its value. (Section~\ref{sec:eval}) 
%Experiments on a set of GPU benchmarks show that the
%model produces accurate cache performance estimation (on average 98\% for L1 and
%99.8\% for the texture cache 
%compared to cache simulation results)
%with orders of magnitude lower overhead than existing solutions. When
%applied to guide runtime data placement on GPU, it helps produce 1.24X
%speedups compared to the original benchmarks. 

%In this work, we model the cache performance, not the execution time.

% Hao : the importance of this work, benefits of the community
The work in this paper can benefit other relavant research efforts
on general GPU performance modeling. Prior work on modeling GPU
performance for architectural research can take our model
as a component on analyzing cache effect~\cite{HongK:ISCA09, Sim+:PPOPP12, ZhangO:HPCA11}.
Using footprint to predict cache miss ratio also provides a
viable alternative for the costly detailed cache simulation
because of its efficiency and generality to different hardware
configurations. On program optimization,
in Section~\ref{sec:opt} we report our experience of
improving a state-of-the-art data placement optimizer
\cite{Chen+:Micro14} by using this model. Other 
GPU program optimizations targeting on memory usage, 
such as kernel restructuring~\cite{Unkule+:CC12}, can
also benefit from this work. 

It is important to note that the use of trace-based 
cache performance models (including ours) is not limited to cache 
miss ratio prediction. 
The non-deterministic memory access interleaving
can undermine the validity of the predicted miss ratio
of a trace-based cache model when the memory trace
is distorted by instrumentation as observed by us and
prior works~\cite{Baghsorkhi+:PPOPP10}. In particular, 
how warps and thread blocks are scheduled can change the access stream significantly.
However, due to the fact that our model carries 
98\% accuracy when \emph{compared against trace-based 
cache simulation}, we claim the model can reliably quantify the 
locality of the memory access stream, which can be generated from
different scheduling policies underneath. From 
this perspective, another possible use of our model is to serve as 
an ``accelarator'' for evaluating the locality of different 
scheduling policies~\cite{Huang+:Micro14}.

%Many GPU performance models have been developed in recent years.
%Some of them require multiple runs of micro-benchmarks
%\cite{HongK:ISCA09, ZhangO:HPCA11}, and
%some are costly due to its detailed
%micro-architecture model~\cite{Baghsorkhi+:PPOPP12}.
%These models are not composable and require repeated testing
%when the data layout changes.  By focusing on the cache performance,
%we can develop a composable analysis that requires just a single
%execution of the target program.

\begin{figure*}[ht]
\centering
\includegraphics[width=0.85\textwidth,height=0.29\textwidth, trim=4 4 4 4,clip]{figures/composition_illustration}
\caption{Overview of footprint based composable analysis: Footprint
model scans the interleaved memory trace once and constructs the 
footprint profile for each array in the program. Each array's footprint can
be reused multiple time to compose the miss ratio of an array
group. According to the miss ratio of array groups in different
caches, the optimal placement of the arrays is searched.}
\label{fig:overview}
\end{figure*}

Overall, this work makes the following contributions: 

\begin{itemize}
\item composable analysis for different GPU caches and different data, 
  which is enabled by a new technique, time-preserving trace decomposition. 
%  locality 
%  composition, footprint measurement, and
%  miss-ratio prediction for fully associative cache.

\item a novel approach of accurately modeling the conflict miss 
  for set-associative caches while retaining the hardware independence.

%composable analysis, which infers the miss ratio for all array
%  combinations and all cache sizes.

\item a model of the texture cache, which enables similar
  composable analysis as in the normal cache.

\item evaluation of the composable analysis on 13 benchmarks and
  comparison with cache simulation and hardware counter results.
\end{itemize}

In the rest of this paper, we first present the background on
footprint and introduce some terminology (Section~\ref{sec:fp}). We then
describe the composible model and how it works for traditional cache
on GPU. (Section~\ref{sec:mr} and Section~\ref{sec:conflict}). 
We next explain how it handles texture cache
(Section~\ref{sec:tex_mr}). After reporting the experimental results
(Section~\ref{sec:eval}), the paper concludes with discussions on related work
and a short summary. 

% A CUDA programmer has to choose which arrays to store in
% which cache and for the L1 cache, how large its size should be.

% Using composable analysis, it can quickly evaluate all placement
% choices for a given architecture and select the best solution.  The
% support is specialized for a given program, adaptive across inputs,
 % and portable across architectures.


\input{model}
\input{evaluation}
\input{cases}

%\section{Adaptive Online Profiling through Active Learning}

%\section{Fast Placement Search through Greedy Algorithm}

%\section{Evaluation}

\section{Related Work}

\dpar{GPU Performance Analysis and Improvement}
Many studies have found that memory performance is critical for GPU
programs~\cite{Che+:SC11,Baskaran+:ICS08,Sung+:PACT10,Jia+:ICS12}. 
It has been a focus of program optimization.
Zhang~\cite{Zhang+:ASPLOS11} designed a data reorganization method to reduce
memory transactions for irregular accesses. Wu~\cite{Wu+:PPOPP13} provided several 
efficient reorganization algorithms to solve the problem of data reorganization.
Yang~\cite{Yang+:PLDI10} used a source-to-source compiler to enhance memory coalescing.
Composable analysis addresses the problem of array placement in
different types of GPU memory.

Many GPU performance models have been developed.  \linebreak Hong
gave a sophisticated analytical model, which is based on a set of  
parameters including those of memory properties such as overlapping
among different memory transactions~\cite{HongK:ISCA09}.  Baghsorkhi~\cite{Baghsorkhi+:PPOPP10} designed 
a static model in a compiler based on careful considerations of the
architecture details.  Composable analysis focuses on locality
and cache performance, especially the problem of data placement.

GPU data placement has been carefully studied. 
Jang~\cite{Jang+:TPDS11} developed a rule based system, where rules were
learned empirically.  However, such rules are not portable to different architectures
and even program inputs.  Ma~\cite{MaA:PACT10} developed a solution,
but only for data placement in the
shared memory. 
The PORPLE system~\cite{Chen+:Micro14} includes an automatic data placement tool which exploits 
importance of architectures and data access patterns when determining the best data 
placement.  As reported in Section~\ref{sec:opt}, composable analysis
provides better higher accuracy when evaluating data placement and as 
a result has further improved the performance for two GPU programs.

\dpar{Trace Filtering}
Shen et al.~\cite{shen+:ASPLOS04} converted a trace into a sequence of reuse distances
and then filtered the number sequence based on a threshold.
Kessler~\cite{Kessler+:TOC94} trimmed a full trace into a 
filtered trace by keeping
only the accesses to a segment of memory.  The filtering is spatial in
\cite{Kessler+:TOC94} and frequency-based in \cite{shen+:ASPLOS04}.  The latter is
analogous to extracting a sub-signal from a full signal using a
frequency filter.

\dpar{Composable Models of Locality}
Previous work uses the HOTL theory to minimize program
interference in shared cache~\cite{Xiang+:CCGrid12}.  Locality
was analyzed on complete programs.  In this work, locality is analyzed
for different arrays used in a single GPU program.  Time-preserving
filtering is required for locality optimization within a program.  As
an extension, it complements previous work in the sense that locality
can now be optimized both within and between programs.

This paper makes two extensions to HOTL theory: to measure footprint
and compute miss ratio for time-preserving traces.  With these
extensions, the HOTL theory can be applied to traces that use any kind
of time.

\dpar{Cache Modeling}
Nugteren et al. presented a GPU cache model
based on reuse distance, but it did not model texture cache~\cite{Nugteren+:HPCA14}.
Smith developed the statistical model to estimate the effect of
set-associative cache using the reuse distance~\cite{Smith:ICSE76}. 
Our model adapts the Smith formula to use the footprint instead of
the reuse distance.   Since it is based on the footprint, it is the
first composable model of cache associativity.

\section{Conclusion}

In this paper, we have presented the first composable analysis to
model the GPU cache.  The analysis includes time-preserving trace
decomposition, new algorithms for measuring the footprint and
computing the miss ratio for time-preserving traces, a new model of
set-associative cache, and a new technique to model the texture cache.
The evaluation of 13 GPU benchmark tests shows that the analysis is
98\% accurate for L1 cache and texture cache.  The
analysis has been used in the PORPLE programming framework and
made automatic GPU program optimization more effective.

% As a future work, we will focus on identifying the scenarios where our
% model has high prediction inaccuracy and try to tackle the limitation
% of this work.

\bibliographystyle{plain}
\vspace{-3mm}
\small{
\bibliography{all}
}
\end{document}

% LocalWords:  GPU
