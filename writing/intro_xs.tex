
\begin{abstract}
Modern GPUs have a heterogenous memory hierarchy with explicitly 
managed memory and multiple types of cache memory.  The GPU 
performance may vary significantly depending on which program 
data is placed in which type of memory.  It is inefficient and 
even impractical to test all possible layouts.  This paper 
presents a composable model to reduce this time cost.  The 
composable model measures a metric called footprint for each 
array and uses it to model the performance when arrays are 
grouped differently or placed in different types of cache.  
The paper shows that a programming system can use the model 
to efficiently derive the best array layout, evaluates the 
cost and accuracy of the composable model and compares it 
with related techniques.
\end{abstract}

\section{Introduction}

For a massively parallel architecture like Graphic Processing Units
(GPU), it is important to narrow the gap between the memory throughput
that its hundreds of cores demand and what memory systems can provide.
The issue has prompted the adoption of sophisticated designs of GPU
memory systems. They rely on various hardware caches (e.g., L1/L2,
texture cache, constant cache) for a high memory throughput.

A cache performance model is a model that can estimate the performance
of a cache given its configuration and the data accesses of a
program. Having a good GPU cache performance model is essential for
guiding GPU cache architecture design and GPU program transformations
to enhance memory performance.

This work develops a new model of GPU cache performance. It addresses
two major limitations of existing models.

First, it supports composable analysis of GPU cache performance.
Being composable means from cache performance on each of a set of data
objects, the model can easily derive the cache performance on accesses
to all of the objects. The composability is essential for program
optimizations. An example is data placement optimizations.  Studies
have shown that several times of performance could be brought by
different decisions on letting which arrays go through L1 cache,
texture cache, constant cache, or shared memory on
GPU~\cite{Chen+:MICRO14,Jang+:TPDS11}. Given a program having $K$
arrays, there are $4^K$ possibilities. Finding the best of them
requires assessment of most if not all of these
possibillities. Lacking composibility, existing cache performance
models~\cite{MISSINGREF} would need to process the access traces for
each of the $2^K$ subset of the $K$ arrays. With our composable model,
only $K$ traces (one for each array) need to be processed, from which,
the performance of accesses to any subset of the arrays can be quickly
derived. The exponential-to-linear reduction of the complexity is
significant: It makes cache performance analysis orders of magnitude
faster, and opening the possibility for accurate cache models to be
used even during execution time (as we show in Section~\ref{}).

Second, the new model handles not only traditional cache, but also
texture cache on GPU. Texture cache is an important part of GPU memory
systems; its unique support of 2D/3D data locality is essential to
many applications. However, the way it works differs from traditional
cache by featuring a sector-based management. The complexity has put
texture cache beyond the scope of prior GPU cache models. Our
new model overcomes that limitation.

The new model is a significant advancement upon footprint
theory~\cite{}. Experiments on a set of GPU benchmarks show that the
model produces accurate cache performance estimation (??\% on average
compared to cache simulation results)
with orders of magnitude lower overhead than existing solutions. When
applied to guide runtime data placement on GPU, it helps produce ??X
speedups compared to the original benchmarks. 

In the rest of this paper, we first present some background on
footprint and introduce some terminology (Section~\ref{}). We then
describe the composible model and how it works for traditional cache
on GPU (Section~\ref{}). We next explain how it handles texture cache
(Section~\ref{}). After reporting the experimental results
(Section~\ref{}), the paper concludes with discussions on related work
and a short summary. 
